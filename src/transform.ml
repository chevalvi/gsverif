open Ptree
open Pitptree
open Parsing_helper

(* Module Set and Map for string *)

module CompString =
  struct
    type t = string
    let compare  = Pervasives.compare
  end

module StringMap = Map.Make(CompString)

(* Special names *)

let stamp_str = "st"
let nat_str = "i"
let priv_channel_str = "d"
let query_var_str = "x"

(* Options *)

let precise_str = "precise"
let unique_action_str = "uniqueAction"
let unique_comm_str = "uniqueComm"
let cell_str = "cell"
let counter_str = "counter"
let locked_round_str = "lockedRound"
let locked_insert_str = "lockedInsert"
let locked_value_str = "value"

let ignore_type = ref false
let replace_types_by_bitstring = ref false

(***********************************
          Initialisation
************************************)

(* Initialise environment *)

let any_type_id = { id = "_any_type"; counter = 0 }
let bitstring_id = { id = "bitstring"; counter = 0 }
let channel_id = { id = "channel"; counter = 0 }
let bool_id = { id = "bool"; counter = 0 }
let nat_id = { id = "nat"; counter = 0 }
let stamp_id = { id = "stamp"; counter = 0 }

let true_id = { id = "true"; counter = 0 }
let false_id = { id = "false"; counter = 0 }

(* Operators *)

let and_id = { id = "&&"; counter = 0 }
let or_id = { id = "||"; counter = 0 }
let eq_id = { id = "="; counter = 0 }
let neq_id = { id = "<>"; counter = 0 }
let inf_id = { id = "<"; counter = 0 }
let infeq_id = { id = "<="; counter = 0 }
let not_id = { id = "not"; counter = 0 }

let and_symb = { id_f = and_id; arg_ty = [bool_id; bool_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let or_symb = { id_f = or_id; arg_ty = [bool_id; bool_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let eq_symb = { id_f = eq_id; arg_ty = [any_type_id; any_type_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let neq_symb = { id_f = neq_id; arg_ty = [any_type_id; any_type_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let inf_symb = { id_f = inf_id; arg_ty = [nat_id; nat_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let infeq_symb = { id_f = infeq_id; arg_ty = [nat_id; nat_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let not_symb = { id_f = not_id; arg_ty = [bool_id]; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let true_symb = { id_f = true_id; arg_ty = []; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }
let false_symb = { id_f = false_id; arg_ty = []; return_ty = bool_id; data = false; priv = false; type_conv = false; pure_priv = false }

let initial_environment =
  let env1 = StringMap.add "bitstring" (EType bitstring_id) StringMap.empty in
  let env2 = StringMap.add "channel" (EType channel_id) env1 in
  let env3 = StringMap.add "bool" (EType bool_id) env2 in
  let env4 = StringMap.add "nat" (EType nat_id) env3 in
  let env5 = StringMap.add "true" (EFun true_symb) env4 in
  let env6 = StringMap.add "false" (EFun false_symb) env5 in
  let env7 = StringMap.add "&&" (EFun and_symb) env6 in
  let env8 = StringMap.add "||" (EFun or_symb) env7 in
  let env9 = StringMap.add "=" (EFun eq_symb) env8 in
  let env10 = StringMap.add "<>" (EFun neq_symb) env9 in
  let env11 = StringMap.add "<" (EFun inf_symb) env10 in
  let env12 = StringMap.add "<=" (EFun infeq_symb) env11 in
  let env13 = StringMap.add "not" (EFun not_symb) env12 in
  env13

(* Freshness *)

let fresh_id_from_ident, fresh_id_from_string =
  let acc = ref 1 in
  let fresh_ident (str,_) =
    let r = { id = str; counter = !acc } in
    incr acc;
    r
  in
  let fresh_string str =
    let r = { id = str; counter = !acc } in
    incr acc;
    r
  in
  fresh_ident, fresh_string

let fresh_var_nat () =
  let n = fresh_id_from_string nat_str in
  { id_v = n; typ_v = nat_id; status_v = SCNone; options_v = [] }

let fresh_var_stamp () =
  let n = fresh_id_from_string stamp_str in
  { id_v = n; typ_v = stamp_id; status_v = SCNone; options_v = [] }

let fresh_name_stamp () =
  let n = fresh_id_from_string stamp_str in
  { id_n = n; typ_n = stamp_id; options_n = []; ext_n = dummy_ext; status_n = SCNone }

let fresh_name_channel () =
  let n = fresh_id_from_string priv_channel_str in
  { id_n = n; typ_n = channel_id; options_n = []; ext_n = dummy_ext; status_n = SCNone }

let fresh_var typ =
  if typ = nat_id
  then fresh_var_nat ()
  else if typ = stamp_id
  then fresh_var_stamp ()
  else if typ = channel_id
  then
    let n = fresh_id_from_string priv_channel_str in
    { id_v = n; typ_v = typ; status_v = SCNone; options_v = [] }
  else
    let n = fresh_id_from_string query_var_str in
    { id_v = n; typ_v = typ; status_v = SCNone; options_v = [] }

(* Retreive from environement *)
let get_variable_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | EVar var -> var
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a variable." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

let get_type_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | EType id -> id
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a type." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

let get_process_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | EProcess (id,ty_l,gen_proc) -> (id,ty_l,gen_proc)
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a process." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

let get_function_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | EFun f -> f
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a function." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

let get_event_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | EEvent ev -> ev
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be an event." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

let get_table_from_id env (str,ext) =
  try
    match StringMap.find str env with
      | ETable tbl -> tbl
      | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a table." str) ext
  with
  | Not_found -> input_error (Printf.sprintf "The identifier %s is not defined." str) ext

(* Errors *)

let display_list f_elt sep = function
  | [] -> ""
  | [t] -> f_elt t
  | t::q -> List.fold_left (fun acc t' -> Printf.sprintf "%s%s%s" acc sep (f_elt t')) (f_elt t) q

(* Type comparaison *)

let is_equal_type typ1 typ2 =
  (typ1 == any_type_id) ||
  (typ2 == any_type_id) ||
  (typ1 == nat_id && typ2 == nat_id) ||
  (not !ignore_type && typ1 == typ2) ||
  (!ignore_type && typ1 != nat_id && typ2 != nat_id)

let is_equal_type_list typl1 typl2 = List.for_all2 is_equal_type typl1 typl2

let rec get_type = function
  | Var v -> v.typ_v
  | Name n -> n.typ_n
  | Sum _ -> nat_id
  | Nat _ -> nat_id
  | Fun(f,_) -> f.return_ty
  | Tuple _ -> bitstring_id
  | Restr(_,_,t)
  | Test(_,t,_)
  | Let(_,_,t,_)
  | LetFilter(_,_,t,_) -> get_type t

(****************************************
***          Expand protocol          ***
*****************************************)

let restricted_name = ref StringMap.empty

let rec expand_term_e env (t,ext) = match t with
  | PIdent (str,ext) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                if f.arg_ty <> []
                then input_error (Printf.sprintf "The identifier %s is a function that expect %d arguments." str (List.length f.arg_ty)) ext;

                (Fun(f,[]),f.return_ty)
            | EVar v -> (Var v, v.typ_v)
            | EName n -> (Name n, n.typ_n)
            | EArg (t,ty) -> (t,ty)
            | EProcess _ -> input_error (Printf.sprintf "The process %s cannot be called in a term." str) ext
            | ETable _ -> input_error (Printf.sprintf "The table %s cannot appear in a term." str) ext
            | EEvent _ -> input_error (Printf.sprintf "The event %s cannot appear in a term." str) ext
            | _ -> input_error (Printf.sprintf "The type %s cannot appear in a term." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PNat n -> (Nat n, nat_id)
  | PSum ((str,ext),n) ->
      let v = get_variable_from_id env (str,ext) in

      if not (is_equal_type v.typ_v nat_id)
      then input_error (Printf.sprintf "The function + expects arguments of type nat but received an argument of type %s." v.typ_v.id) ext;

      (Sum(v,n), nat_id)
  | PFail -> input_error "The front end does not handle function declaration with the Fail constant." ext
  | PFunApp((str,ext),args) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                let (new_args,new_types) =
                  List.fold_right (fun t (acc_t,acc_ty) ->
                    let (new_t,new_ty) = expand_term_e env t in
                    (new_t::acc_t,new_ty::acc_ty)
                  ) args ([],[])
                in

                if not (is_equal_type_list new_types f.arg_ty)
                then input_error (Printf.sprintf "The function %s is expenting arguments of types %s but received arguments of type %s" f.id_f.id (display_list (fun id -> id.id) ", " f.arg_ty) (display_list (fun id -> id.id) ", " new_types)) ext;

                (Fun (f,new_args),f.return_ty)
            | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a function symbol." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PTuple(args) ->
      let new_args =
        List.fold_right (fun t acc_t ->
          let (new_t,_) = expand_term_e env t in
          new_t::acc_t
        ) args []
      in
      Tuple(new_args),bitstring_id

let rec expand_and_check_pure_priv_term_e env (t,ext) = match t with
  | PIdent (str,ext) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                if f.arg_ty <> []
                then input_error (Printf.sprintf "The identifier %s is a function that expect %d arguments." str (List.length f.arg_ty)) ext;

                (Fun(f,[]),f.return_ty)
            | EVar v -> (Var v, v.typ_v)
            | EName n -> (Name n, n.typ_n)
            | EArg (t,ty) -> (t,ty)
            | EProcess _ -> input_error (Printf.sprintf "The process %s cannot be called in a term." str) ext
            | ETable _ -> input_error (Printf.sprintf "The table %s cannot appear in a term." str) ext
            | EEvent _ -> input_error (Printf.sprintf "The event %s cannot appear in a term." str) ext
            | _ -> input_error (Printf.sprintf "The type %s cannot appear in a term." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PNat n -> (Nat n, nat_id)
  | PSum ((str,ext),n) ->
      let v = get_variable_from_id env (str,ext) in

      if not (is_equal_type v.typ_v nat_id)
      then input_error (Printf.sprintf "The function + expects arguments of type nat but received an argument of type %s." v.typ_v.id) ext;

      (Sum(v,n), nat_id)
  | PFail -> input_error "The front end does not handle function declaration with the Fail constant." ext
  | PFunApp((str,ext),args) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                if f.priv then f.pure_priv <- false;

                let (new_args,new_types) =
                  List.fold_right (fun t (acc_t,acc_ty) ->
                    let (new_t,new_ty) = expand_and_check_pure_priv_term_e env t in
                    (new_t::acc_t,new_ty::acc_ty)
                  ) args ([],[])
                in

                if not (is_equal_type_list new_types f.arg_ty)
                then input_error (Printf.sprintf "The function %s is expenting arguments of types %s but received arguments of type %s" f.id_f.id (display_list (fun id -> id.id) ", " f.arg_ty) (display_list (fun id -> id.id) ", " new_types)) ext;

                (Fun (f,new_args),f.return_ty)
            | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a function symbol." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PTuple(args) ->
      let new_args =
        List.fold_right (fun t acc_t ->
          let (new_t,_) = expand_and_check_pure_priv_term_e env t in
          new_t::acc_t
        ) args []
      in
      Tuple(new_args),bitstring_id

let rec expand_pterm env main_ext = function
  | PPIdent (str,ext) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                if f.arg_ty <> []
                then input_error (Printf.sprintf "The identifier %s is a function that expect %d arguments." str (List.length f.arg_ty)) ext;

                (Fun(f,[]),f.return_ty)
            | EVar v -> (Var v, v.typ_v)
            | EName n -> (Name n, n.typ_n)
            | EArg (t,ty) -> (t,ty)
            | EProcess _ -> input_error (Printf.sprintf "The process %s cannot be called in a term." str) ext
            | ETable _ -> input_error (Printf.sprintf "The table %s cannot appear in a term." str) ext
            | EEvent _ -> input_error (Printf.sprintf "The event %s cannot appear in a term." str) ext
            | _ -> input_error (Printf.sprintf "The type %s cannot appear in a term." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PPNat n -> (Nat n, nat_id)
  | PPSum ((str,ext),n) ->
      let v = get_variable_from_id env (str,ext) in

      if not (is_equal_type v.typ_v nat_id)
      then input_error (Printf.sprintf "The function + expects arguments of type nat but received an argument of type %s." v.typ_v.id) ext;

      (Sum(v,n), nat_id)
  | PPFunApp((str,ext),args) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                let (new_args,new_types) =
                  List.fold_right (fun t (acc_t,acc_ty) ->
                    let (new_t,new_ty) = expand_pterm_e env t in
                    (new_t::acc_t,new_ty::acc_ty)
                  ) args ([],[])
                in

                if not (is_equal_type_list new_types f.arg_ty)
                then input_error (Printf.sprintf "The function %s is expenting arguments of types %s but received arguments of type %s" f.id_f.id (display_list (fun id -> id.id) ", " f.arg_ty) (display_list (fun id -> id.id) ", " new_types)) ext;

                (Fun (f,new_args),f.return_ty)
            | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a function symbol." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PPTuple(args) ->
      let new_args =
        List.fold_right (fun t acc_t ->
          let (new_t,_) = expand_pterm_e env t in
          new_t::acc_t
        ) args []
      in
      Tuple(new_args),bitstring_id
  | PPRestr((str,ext),vars_op,ty, term) ->
      let new_id = fresh_id_from_ident (str,ext) in
      let new_vars_op = match vars_op with
        | None -> None
        | Some vars -> Some (List.map (get_variable_from_id env) vars)
      in
      let new_type = get_type_from_id env ty in
      let new_name = { id_n = new_id; typ_n = new_type; options_n = []; status_n = SCNone; ext_n = ext } in
      let new_env = StringMap.add new_id.id (EName new_name) env in
      let new_term,return_type = expand_pterm_e new_env term in

      Restr(new_name,new_vars_op,new_term), return_type
  | PPTest(cond,t_then,t_op) ->
      let new_cond,cond_type = expand_pterm_e env cond in

      if not (is_equal_type cond_type bool_id)
      then input_error (Printf.sprintf "The condition of the test is expected to be of type bool but is of type %s" cond_type.id) main_ext;

      let new_t_then,t_then_type = expand_pterm_e env t_then in
      let new_t_op = match t_op with
        | None -> None
        | Some t ->
            let new_t, t_type = expand_pterm_e env t in
            if not (is_equal_type t_type t_then_type)
            then input_error (Printf.sprintf "The type of the then branch %s differs from the type of the else branch %s." t_then_type.id t_type.id) main_ext;
            Some new_t
      in
      Test(new_cond,new_t_then,new_t_op),t_then_type
  | PPLet(pat,t,t_then,t_op) ->
      let new_t,_ = expand_pterm_e env t in
      let new_pat, new_env = expand_tpattern env (Some new_t) pat in

      let new_t_then, new_ty_then = expand_pterm_e new_env t_then in
      begin match t_op with
        | None -> Let(new_pat,new_t,new_t_then, None), new_ty_then
        | Some t_else ->
            let new_t_else,new_ty_else = expand_pterm_e env t_else in
            if not (is_equal_type new_ty_then new_ty_else)
            then input_error (Printf.sprintf "The type of the then branch %s differs from the type of the else branch %s." new_ty_then.id new_ty_else.id) main_ext;
            Let(new_pat,new_t,new_t_then, Some new_t_else), new_ty_then
      end
  | PPLetFilter(var_list,cond,t_then,t_else_op) ->
      let new_var_list,new_env =
        List.fold_left (fun (acc_vars,acc_env) (id,ty) ->
          let new_ty = get_type_from_id env ty in
          let new_id = fresh_id_from_ident id in
          let new_var = { id_v = new_id; typ_v = new_ty; status_v = SCNone; options_v = [] } in
          (acc_vars @ [new_var], StringMap.add new_id.id (EVar new_var) acc_env)
        ) ([],env) var_list
      in
      let new_cond, cond_type = expand_pterm_e new_env cond in

      if not (is_equal_type cond_type bool_id)
      then input_error (Printf.sprintf "The predicate of the Let is expected to be of type bool but is of type %s" cond_type.id) main_ext;

      let new_t_then, then_type = expand_pterm_e new_env t_then in
      let new_t_else_op = match t_else_op with
        | None -> None
        | Some t_else ->
            let new_t_else, else_type = expand_pterm_e env t_else in
            if not (is_equal_type then_type else_type)
            then input_error (Printf.sprintf "The type of the then branch (%s) differs from the type of the else branch (%s)." then_type.id else_type.id) main_ext;
            Some new_t_else
      in

      LetFilter(new_var_list,new_cond,new_t_then,new_t_else_op), then_type

and expand_pterm_e env (pterm,ext) = expand_pterm env ext pterm

and expand_tpattern env t_match_op = function
  | PPatVar((str,ext),ty_op) ->
      let new_type =
        match t_match_op,ty_op with
          | None, None -> input_error (Printf.sprintf "The variable %s does not have a type." str) ext
          | Some t_match, None -> get_type t_match
          | Some t_match, Some ty ->
              let t_type = get_type t_match in
              let new_ty_op = get_type_from_id env ty in
              if not (is_equal_type new_ty_op t_type)
              then input_error (Printf.sprintf "The variable %s is declared with type %s but is assigned a term of type %s" str new_ty_op.id t_type.id) ext;
              t_type
          | None, Some ty -> get_type_from_id env ty
      in
      let new_var_id = fresh_id_from_ident (str,ext) in
      let new_var = { id_v = new_var_id; typ_v = new_type; status_v = SCNone; options_v = [] } in
      let new_env = StringMap.add str (EVar new_var) env in
      (PatVar new_var, new_env)
  | PPatTuple(pat_args) ->
      begin match t_match_op with
        | Some (Tuple (args)) when List.length pat_args = List.length args ->
            let (new_pat_args,new_env) =
              List.fold_left2 (fun (acc_pat,acc_env) pat t ->
                let new_pat,new_env = expand_tpattern acc_env (Some t) pat in
                (acc_pat @ [new_pat],new_env)
              ) ([],env) pat_args args
            in
            PatTuple(new_pat_args), new_env
        | _ ->
          let (new_pat_args,new_env) =
            List.fold_left (fun (acc_pat,acc_env) pat ->
              let new_pat,new_env = expand_tpattern acc_env None pat in
              (acc_pat @ [new_pat],new_env)
            ) ([],env) pat_args
          in
          PatTuple(new_pat_args), new_env
      end
  | PPatFunApp(id,pat_args) ->
      let f = get_function_from_id env id in
      begin match t_match_op with
        | Some (Fun (f',args)) when f == f' ->
            let (new_pat_args,new_env) =
              List.fold_left2 (fun (acc_pat,acc_env) pat t ->
                let new_pat,new_env = expand_tpattern acc_env (Some t) pat in
                (acc_pat @ [new_pat],new_env)
              ) ([],env) pat_args args
            in
            PatData(f,new_pat_args), new_env
        | _ ->
          let (new_pat_args,new_env) =
            List.fold_left (fun (acc_pat,acc_env) pat ->
              let new_pat,new_env = expand_tpattern acc_env None pat in
              (acc_pat @ [new_pat],new_env)
            ) ([],env) pat_args
          in
          PatData(f,new_pat_args), new_env
      end
  | PPatEqual term ->
      let (new_term,_) = expand_pterm_e env term in
      PatEq new_term, env

let rec expand_tprocess env = function
  | PNil -> { proc = CNil; comment = None }
  | PPar(p1,p2) -> { proc = CPar (expand_tprocess env p1,expand_tprocess env p2); comment = None }
  | PRepl(p) -> { proc = CRepl (expand_tprocess env p); comment = None }
  | PRestr((str,ext_n),vars,id_ty,op_l,p) ->
      let new_id = fresh_id_from_ident (str,ext_n) in
      let new_vars =
        match vars with
          | None -> None
          | Some vars_l -> Some (List.map (get_variable_from_id env) vars_l)
      in
      let new_type = get_type_from_id env id_ty in

      let is_precise = ref false in
      let special_options = ref false in
      let new_op_l =
        List.map (fun (str,ext) ->
          match str with
            | str when str = unique_action_str-> special_options := true; NDUAction
            | str when str = unique_comm_str -> special_options := true; NDUComm
            | str when str = cell_str -> special_options := true; NDCell
            | str when str = counter_str -> special_options := true; NDCounter
            | str when str = precise_str -> is_precise := true; NDPrecise
            | _ -> input_error "Unexpected option for a name." ext
        ) op_l
      in

      if List.mem NDUComm new_op_l && List.exists (fun op -> op = NDCell || op = NDCounter) new_op_l
      then input_error (Printf.sprintf "A name cannot be declared with the options %s and either %s or %s." unique_comm_str cell_str counter_str) ext_n;

      if new_op_l <> [] && not (is_equal_type new_type channel_id)
      then input_error (Printf.sprintf "Only name of type channel can be declared with the options %s, %s, %s, %s or %s." unique_comm_str cell_str counter_str precise_str unique_action_str) ext_n;

      if !special_options && !is_precise
      then input_error (Printf.sprintf "A channel declared with the option %s cannot also be declared with the options %s, %s, %s or %s."
          precise_str unique_comm_str unique_action_str cell_str counter_str) ext_n;

      let status_channel =
        if !is_precise || List.exists (fun op -> op = NDCell || op = NDCounter) new_op_l
        then SCCell None
        else if List.mem NDUComm new_op_l
        then SCPrivate
        else if List.mem NDUAction new_op_l
        then SCPublic
        else SCNone
      in

      let new_name = { id_n = new_id; typ_n = new_type; options_n = new_op_l; status_n = status_channel; ext_n = ext_n } in
      let new_env = StringMap.add str (EName new_name) env in
      let new_p = expand_tprocess new_env p in

      { proc = CRestr(new_name,new_vars,new_p); comment = None }
  | PLetDef((str,ext),args) ->
      let (_,ty_l,gen_proc) = get_process_from_id env (str,ext) in

      if List.length ty_l <> List.length args
      then input_error (Printf.sprintf "The function %s is expecting %d arguments but is given %d." str (List.length ty_l) (List.length args)) ext;

      let new_args =
        List.fold_left2 (fun acc_arg (t,ext_t) ty ->
          let new_t, new_type = expand_pterm_e env (t,ext_t) in
          if not (is_equal_type new_type ty)
          then input_error (Printf.sprintf "The argument is expected to be of type %s but is of type %s" ty.id new_type.id) ext_t;

          acc_arg @ [ new_t ]
        ) [] args ty_l
      in

      let new_proc = gen_proc new_args in
      { new_proc with comment = Some (Printf.sprintf "Application of the process %s" str) }
  | PTest((cond,ext_cond),p_then,p_else) ->
      let new_cond, cond_type = expand_pterm_e env (cond,ext_cond) in
      if not (is_equal_type cond_type bool_id)
      then input_error (Printf.sprintf "The condition of the test is expected to be of type bool but is of type %s" cond_type.id) ext_cond;

      { proc = CTest(new_cond,expand_tprocess env p_then,expand_tprocess env p_else); comment = None }
  | PInput((c,ext_c),pat,p) ->
      let new_c, c_type = expand_pterm_e env (c,ext_c) in
      if not (is_equal_type c_type channel_id)
      then input_error (Printf.sprintf "The term is expected to be of type channel but is of type %s" c_type.id) ext_c;
      let new_pat,new_env = expand_tpattern env None pat in
      let new_p = expand_tprocess new_env p in

      { proc = CInput(new_c,new_pat,new_p); comment = None }
  | POutput((c,ext_c),t,p) ->
      let new_c, c_type = expand_pterm_e env (c,ext_c) in
      if not (is_equal_type c_type channel_id)
      then input_error (Printf.sprintf "The term is expected to be of type channel but is of type %s" c_type.id) ext_c;
      let new_t,_ = expand_pterm_e env t in
      let new_p = expand_tprocess env p in

      { proc = COutput(new_c,new_t,new_p); comment = None }
  | PLet(pat,t,p_then,p_else) ->
      let new_t,_ = expand_pterm_e env t in
      let new_pat, new_env = expand_tpattern env (Some new_t) pat in
      let new_p_then = expand_tprocess new_env p_then in
      let new_p_else = expand_tprocess env p_else in

      { proc = CLet(new_pat,new_t,new_p_then,new_p_else); comment = None }
  | PLetFilter(var_list,(cond,ext_cond),p_then,p_else) ->
      let new_var_list,new_env =
        List.fold_left (fun (acc_vars,acc_env) (id,ty) ->
          let new_ty = get_type_from_id env ty in
          let new_id = fresh_id_from_ident id in
          let new_var = { id_v = new_id; typ_v = new_ty; status_v = SCNone; options_v = [] } in
          (acc_vars @ [new_var], StringMap.add new_id.id (EVar new_var) acc_env)
        ) ([],env) var_list
      in

      let new_cond, cond_type = expand_pterm_e new_env (cond,ext_cond) in

      if not (is_equal_type cond_type bool_id)
      then input_error (Printf.sprintf "The predicate is expected to be of type bool but is of type %s" cond_type.id) ext_cond;

      let new_p_then = expand_tprocess new_env p_then in
      let new_p_else = expand_tprocess env p_else in

      { proc = CLetFilter(new_var_list,new_cond,new_p_then,new_p_else); comment = None }
  | PEvent((str,ext),args,vars_op,p) ->
      let ev = get_event_from_id env (str,ext) in

      if List.length ev.arg_ty_e <> List.length args
      then input_error (Printf.sprintf "The event %s is expecting %d arguments but is given %d." str (List.length ev.arg_ty_e) (List.length args)) ext;

      let new_args =
        List.fold_left2 (fun acc_arg (t,ext_t) ty ->
          let new_t, new_type = expand_pterm_e env (t,ext_t) in
          if not (is_equal_type new_type ty)
          then input_error (Printf.sprintf "The argument is expected to be of type %s but is of type %s" ty.id new_type.id) ext_t;

          acc_arg @ [ new_t ]
        ) [] args ev.arg_ty_e
      in

      let new_vars_op = match vars_op with
        | None -> None
        | Some vars -> Some (List.map (get_variable_from_id env) vars)
      in
      let new_p = expand_tprocess env p in

      { proc = CEvent(ev,new_args,new_vars_op,new_p); comment = None }
  | PPhase _ -> input_error "The front-end does not handle phases yet." dummy_ext
  | PBarrier _ -> input_error "The front-end does not handle synchronisation yet." dummy_ext
  | PInsert ((str,ext),args,p) ->
      let tbl = get_table_from_id env (str,ext) in

      if List.length tbl.arg_ty_t <> List.length args
      then input_error (Printf.sprintf "The table %s is expecting %d arguments but is given %d." str (List.length tbl.arg_ty_t) (List.length args)) ext;

      let new_args =
        List.fold_left2 (fun acc_arg (t,ext_t) ty ->
          let new_t, new_type = expand_pterm_e env (t,ext_t) in
          if not (is_equal_type new_type ty)
          then input_error (Printf.sprintf "The argument is expected to be of type %s but is of type %s" ty.id new_type.id) ext_t;

          acc_arg @ [ new_t ]
        ) [] args tbl.arg_ty_t
      in

      let new_p = expand_tprocess env p in

      { proc = CInsert(tbl,new_args,new_p); comment = None }
  | PGet((str,ext),pat_l,cond,p_then,p_else) ->
      let tbl = get_table_from_id env (str,ext) in

      if List.length tbl.arg_ty_t <> List.length pat_l
      then input_error (Printf.sprintf "The table %s is expecting %d arguments but is given %d." str (List.length tbl.arg_ty_t) (List.length pat_l)) ext;

      let new_pat_l, new_env =
        List.fold_left2 (fun (acc_pat,acc_env) pat ty ->
          let v = fresh_var ty in
          let new_pat,new_env = expand_tpattern acc_env (Some (Var v)) pat in
          (acc_pat @ [new_pat], new_env)
        ) ([],env) pat_l tbl.arg_ty_t
      in

      let new_cond = match cond with
        | None -> None
        | Some ((cond,ext_cond)) ->
            let new_cond,cond_type = expand_pterm_e new_env (cond,ext_cond) in
            if not (is_equal_type cond_type bool_id)
            then input_error (Printf.sprintf "The predicate is expected to be of type bool but is of type %s" cond_type.id) ext_cond;
            Some new_cond
      in

      let new_p_then = expand_tprocess new_env p_then in
      let new_p_else = expand_tprocess env p_else in

      { proc = CGet(tbl,new_pat_l,new_cond,new_p_then,new_p_else); comment = None }

let rec gather_restriction env = function
  | PNil -> ()
  | PPar(p1,p2)
  | PTest(_,p1,p2)
  | PLet(_,_,p1,p2)
  | PLetFilter(_,_,p1,p2)
  | PGet(_,_,_,p1,p2) -> gather_restriction env p1; gather_restriction env p2
  | PRepl p
  | PInput(_,_,p)
  | POutput(_,_,p)
  | PEvent(_,_,_,p)
  | PInsert(_,_,p) -> gather_restriction env p
  | PPhase _ -> input_error "The front-end does not handle phases yet." dummy_ext
  | PBarrier _ -> input_error "The front-end does not handle synchronisation yet." dummy_ext
  | PRestr((str,_),_,id_ty,_,p) ->
      let new_type = get_type_from_id env id_ty in

      restricted_name := StringMap.add str new_type !restricted_name;
      gather_restriction env p;
  | PLetDef _ -> ()

(* Queries treatment *)

let rec expand_inside_gterm_e env (t,ext) = match t with
  | PGIdent (str,ext) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                if f.arg_ty <> []
                then input_error (Printf.sprintf "The identifier %s is a function that expect %d arguments." str (List.length f.arg_ty)) ext;

                (Fun(f,[]),f.return_ty)
            | EVar v -> (Var v, v.typ_v)
            | EName n -> (Name n, n.typ_n)
            | EArg _ -> internal_error "There should not be EArg in queries"
            | EProcess _ -> input_error (Printf.sprintf "The process %s cannot be called in a term." str) ext
            | ETable _ -> input_error (Printf.sprintf "The table %s cannot appear in a term." str) ext
            | EEvent _ -> input_error (Printf.sprintf "The event %s cannot appear in a term." str) ext
            | _ -> input_error (Printf.sprintf "The type %s cannot appear in a term." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PGNat n -> (Nat n, nat_id)
  | PGSum ((str,ext),n) ->
      let v = get_variable_from_id env (str,ext) in

      if not (is_equal_type v.typ_v nat_id)
      then input_error (Printf.sprintf "The function + expects arguments of type nat but received an argument of type %s." v.typ_v.id) ext;

      (Sum(v,n), nat_id)
  | PGFunApp((str,ext),args) ->
      begin
        try
          match StringMap.find str env with
            | EFun f ->
                let (new_args,new_types) =
                  List.fold_right (fun t (acc_t,acc_ty) ->
                    let (new_t,new_ty) = expand_inside_gterm_e env t in
                    (new_t::acc_t,new_ty::acc_ty)
                  ) args ([],[])
                in

                if not (is_equal_type_list new_types f.arg_ty)
                then input_error (Printf.sprintf "The function %s is expenting arguments of types %s but received arguments of type %s" f.id_f.id (display_list (fun id -> id.id) ", " f.arg_ty) (display_list (fun id -> id.id) ", " new_types)) ext;

                (Fun (f,new_args),f.return_ty)
            | _ -> input_error (Printf.sprintf "The identifier %s is expected to be a function symbol." str) ext
        with Not_found -> input_error (Printf.sprintf "Identifier %s not defined." str) ext
      end
  | PGTuple(args) ->
      let new_args =
        List.fold_right (fun t acc_t ->
          let (new_t,_) = expand_inside_gterm_e env t in
          new_t::acc_t
        ) args []
      in
      Tuple(new_args),bitstring_id
  | _ -> input_error "The front-end does not allow phases, name restriction or let operator inside queries yet." ext

let is_fact (query,_) = match query with
  | PGIdent _
  | PGPhase _
  | PGName _
  | PGLet _
  | PGFunApp(("==>",_),_)
  | PGFunApp(("not",_),_)
  | PGFunApp(("||",_),_)
  | PGFunApp(("&&",_),_) -> false
  | _ -> true

let rec expand_fact env (query,ext) = match query with
  | PGFunApp(("attacker",_),[t]) ->
      let new_t,_ = expand_inside_gterm_e env t in
      FAttacker(new_t)
  | PGFunApp(("mess",ext'),[c;t]) ->
      let new_c,typ_c = expand_inside_gterm_e env c in

      if not (is_equal_type typ_c channel_id)
      then input_error "The first argument of a predicate mess should be of type channel." ext';

      let new_t,_ = expand_inside_gterm_e env t in
      FMessage(new_c,new_t)
  | PGFunApp(("table",ext'),[PGFunApp(id,args),_]) ->
      let tbl = get_table_from_id env id in

      if List.length args <> List.length tbl.arg_ty_t
      then input_error (Printf.sprintf "The table %s expects %d arguments but is given %d arguments." tbl.id_t.id (List.length tbl.arg_ty_t) (List.length args)) ext';

      let new_args =
        List.fold_left2 (fun acc (t,ext') typ ->
          let new_t,new_typ = expand_inside_gterm_e env (t,ext') in

          if not (is_equal_type typ new_typ)
          then input_error (Printf.sprintf "An argument of type %s is expected but a term of type %s is given." typ.id new_typ.id) ext';

          acc @ [new_t]
        ) [] args tbl.arg_ty_t
      in

      FTable(tbl,new_args)
  | PGFunApp((op,ext_op),[t1;t2]) when List.exists (fun op' -> op = op') ["="; "<>"; "<"; "<="] ->
      let new_t1,typ1 = expand_inside_gterm_e env t1 in
      let new_t2,typ2 = expand_inside_gterm_e env t2 in

      if not (is_equal_type typ1 typ2)
      then input_error "The two arguments should have the same type." ext;

      if (op = "<" || op = "<=") && not (is_equal_type typ1 nat_id)
      then input_error "The two arguments should have the type nat." ext;

      let op_symb = get_function_from_id env (op,ext_op) in

      FPred(op_symb,[new_t1;new_t2])
  | PGFunApp(("event",ext'),[PGFunApp(id,args),_]) ->
      let ev = get_event_from_id env id in

      if List.length args <> List.length ev.arg_ty_e
      then input_error (Printf.sprintf "The event %s expects %d arguments but is given %d arguments." ev.id_e.id (List.length ev.arg_ty_e) (List.length args)) ext';

      let new_args =
        List.fold_left2 (fun acc (t,ext') typ ->
          let new_t,new_typ = expand_inside_gterm_e env (t,ext') in

          if not (is_equal_type typ new_typ)
          then input_error (Printf.sprintf "An argument of type %s is expected but a term of type %s is given." typ.id new_typ.id) ext';

          acc @ [new_t]
        ) [] args ev.arg_ty_e
      in

      FEvent(ev,new_args,false)
  | PGFunApp(("event",ext'),[PGIdent id,_]) ->
      let ev = get_event_from_id env id in

      if ev.arg_ty_e <> []
      then input_error (Printf.sprintf "The event %s expects %d argument but is given no argument." ev.id_e.id (List.length ev.arg_ty_e)) ext';

      FEvent(ev,[],false)
  | PGFunApp(("inj-event",ext'),[PGFunApp(id,args),_]) ->
      let ev = get_event_from_id env id in

      if List.length args <> List.length ev.arg_ty_e
      then input_error (Printf.sprintf "The event %s expects %d arguments but is given %d arguments." ev.id_e.id (List.length ev.arg_ty_e) (List.length args)) ext';

      let new_args =
        List.fold_left2 (fun acc (t,ext') typ ->
          let new_t,new_typ = expand_inside_gterm_e env (t,ext') in

          if not (is_equal_type typ new_typ)
          then input_error (Printf.sprintf "An argument of type %s is expected but a term of type %s is given." typ.id new_typ.id) ext';

          acc @ [new_t]
        ) [] args ev.arg_ty_e
      in

      FEvent(ev,new_args,true)
  | PGFunApp(("inj-event",ext'),[PGIdent id,_]) ->
      let ev = get_event_from_id env id in

      if ev.arg_ty_e <> []
      then input_error (Printf.sprintf "The event %s expects %d argumentd but is given no arguments." ev.id_e.id (List.length ev.arg_ty_e)) ext';

      FEvent(ev,[],true)
  | PGFunApp(("table",ext'),_)
  | PGFunApp(("event",ext'),_)
  | PGFunApp(("inj-event",ext'),_)
  | PGFunApp(("mess",ext'),_)
  | PGFunApp(("attacker",ext'),_) -> input_error "Wrong number of arguments." ext'
  | PGFunApp((op,ext'),_) when List.exists (fun op' -> op = op') ["="; "<>"; "<"; "<="] -> input_error "Wrong number of arguments." ext'
  | PGFunApp(("==>",_),_) -> internal_error "[transform.ml >> expand_fact] Unexpected case"
  | PGFunApp((str,ext'),args) ->
      let f = get_function_from_id env (str,ext') in

      if not (is_equal_type f.return_ty bool_id)
      then input_error (Printf.sprintf "The identifier %s should be predicate." str) ext';

      if List.length args <> List.length f.arg_ty
      then input_error (Printf.sprintf "The predicate %s expects %d arguments but is given %d arguments." f.id_f.id (List.length f.arg_ty) (List.length args)) ext';

      let new_args =
        List.fold_left2 (fun acc (t,ext') typ ->
          let new_t,new_typ = expand_inside_gterm_e env (t,ext') in

          if not (is_equal_type typ new_typ)
          then input_error (Printf.sprintf "An argument of type %s is expected but a term of type %s is given." typ.id new_typ.id) ext';

          acc @ [new_t]
        ) [] args f.arg_ty
      in

      FPred(f,new_args)
  | _ -> internal_error "[transform.ml >> expand_fact] Unexpected case (2)"

and expand_query env (query,ext) = match query with
  | PGIdent _
  | PGPhase _
  | PGName _
  | PGLet _ -> input_error "The front-end does not allow phases, name restriction or let operator yet." ext
  | PGFunApp(("==>",ext'),[q1;q2]) ->
      if is_fact q1
      then QQuery (expand_fact env q1, expand_query env q2)
      else input_error "The left hand side of ==> should be a fact." ext'
  | PGFunApp(("not",_),[q]) -> QNot (expand_query env q)
  | PGFunApp(("&&",_),[q1;q2]) -> QAnd (expand_query env q1, expand_query env q2)
  | PGFunApp(("||",_),[q1;q2]) -> QOr (expand_query env q1, expand_query env q2)
  | _ -> QFact(expand_fact env (query,ext))

let rec expand_full_query env (query,ext) = match query with
  | PGLet((str,ext_s),(PGName((str_n,_),[]),_),q) ->
      let new_id = fresh_id_from_ident (str,ext_s) in
      let typ_id =
        try
          StringMap.find str_n !restricted_name
        with
        | Not_found -> input_error "In a query, the new operator should always be applied a name defined somewhere before (not necessarily in the scope of the query.)" ext
      in
      let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
      let env' = StringMap.add str (EVar v) env in
      FLetNew(v,str_n,expand_full_query env' q)
  | PGLet _ -> input_error "The front-end only allow for simple \"let x = new k in ...\" in front of the query." dummy_ext
  | _ -> FQuery(expand_query env (query,ext))

let expand_tquery env = function
  | PPutBegin _ -> input_error "The front-end does not allow PutBegin queries." dummy_ext
  | PRealQuery gt -> expand_full_query env gt

let expand_tclause env = function
  | PClause(t1,t2) ->
      let t1',_ = expand_term_e env t1 in
      let t2',_ = expand_term_e env t2 in
      CClause(t1',t2')
  | PFact(t) ->
      let t',_ = expand_term_e env t in
      CFact(t')
  | PEquiv(t1,t2,b) ->
      let t1',_ = expand_term_e env t1 in
      let t2',_ = expand_term_e env t2 in
      CEquiv(t1',t2',b)

let rec expand_tdecl env proc = function
  | [] -> [DProcess (expand_tprocess env proc)]
  | decl :: q_decl ->
      match decl with
        | TTypeDecl id ->
            let str,_ = id in
            let new_id = fresh_id_from_ident id in
            (DType new_id)::expand_tdecl (StringMap.add str (EType new_id) env) proc q_decl
        | TFunDecl(id,typ_args,typ,options) ->
            let (str,_) = id in
            let typ_args_id = List.map (get_type_from_id env) typ_args in
            let typ_id = get_type_from_id env typ in
            let is_data = ref false
            and is_priv = ref false
            and is_type_conv = ref false in
            List.iter (function
              | "private",_ -> is_priv := true
              | "data",_ -> is_data := true
              | "typeConverter",_ -> is_type_conv := true
              | s,ext -> input_error (Printf.sprintf "A function cannot be declared with the option %s." s) ext
            ) options;
            let f =
              { id_f = fresh_id_from_ident id;
                arg_ty = typ_args_id;
                return_ty = typ_id;
                priv = !is_priv;
                pure_priv = !is_priv;
                data = !is_data;
                type_conv = !is_type_conv
              }
            in
            (DFun f)::(expand_tdecl (StringMap.add str (EFun f) env) proc q_decl)
        | TEventDecl(id,typ_args) ->
            let (str,_) = id in
            let typ_args_id = List.map (get_type_from_id env) typ_args in
            let new_id = fresh_id_from_ident id in
            let new_ev = { id_e = new_id; arg_ty_e = typ_args_id } in

            (DEvent new_ev)::(expand_tdecl (StringMap.add str (EEvent new_ev) env) proc q_decl)
        | TConstDecl(id,typ,options) ->
            let str,_ = id in
            let typ_id = get_type_from_id env typ in
            let is_data = ref false
            and is_priv = ref false
            and is_type_conv = ref false in
            List.iter (function
              | "private",_ -> is_priv := true
              | "data",_ -> is_data := true
              | "typeConverter",_ -> is_type_conv := true
              | s,ext -> input_error (Printf.sprintf "A constant cannot be declared with the option %s." s) ext
            ) options;
            let f =
              { id_f = fresh_id_from_ident id;
                arg_ty = [];
                return_ty = typ_id;
                pure_priv = !is_priv;
                priv = !is_priv;
                data = !is_data;
                type_conv = !is_type_conv
              }
            in
            (DConst f)::(expand_tdecl (StringMap.add str (EFun f) env) proc q_decl)
        | TReduc(reduc,options) ->
            let typ_lhs = ref [] in
            let typ_rhs = ref None in
            let f_str = ref None in
            let symb = ref None in

            let new_reduc =
              List.map (fun (vars,(lhs,ext),rhs) ->
                let new_env, new_vars =
                  List.fold_left (fun (acc_env,acc_var) ((str,ext),typ) ->
                    let new_id = fresh_id_from_ident (str,ext) in
                    let typ_id = get_type_from_id acc_env typ in
                    let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                    (StringMap.add str (EVar v) acc_env, acc_var @ [v])
                  ) (env,[]) vars
                in
                match lhs with
                  | PFunApp((str,f_ext),args) ->
                      begin match !f_str with
                        | None -> f_str := Some str
                        | Some str' when str = str' -> ()
                        | _ -> input_error "The head symbol is not the same in the declaration." f_ext
                      end;
                      let (new_args,typ_args) =
                        List.fold_left (fun (acc_args,acc_typ) t ->
                          let (new_t,new_typ) = expand_and_check_pure_priv_term_e new_env t in
                          (acc_args @ [new_t], acc_typ @ [new_typ])
                        ) ([],[]) args
                      and (new_rhs,new_typ_rhs) = expand_and_check_pure_priv_term_e new_env rhs in

                      if !typ_lhs = [] then typ_lhs := typ_args;
                      if not (is_equal_type_list !typ_lhs typ_args) then input_error "The arguments of the left hand sides of the reduction do not have the same type." f_ext;

                      begin match !typ_rhs with
                        | None -> typ_rhs := Some new_typ_rhs
                        | Some typ when is_equal_type typ new_typ_rhs -> ()
                        | _ -> input_error "The right hand sides of the reduction do not have the same type." f_ext
                      end;

                      let f = match !symb with
                        | None ->
                            let f =
                              { id_f = fresh_id_from_ident (str,f_ext);
                                arg_ty = !typ_lhs;
                                return_ty = (match !typ_rhs with Some t -> t | None -> internal_error "[transform.ml >> expand_tdecl] Unexpected case 1");
                                priv = (match options with ["private",_] -> true | _ -> false);
                                pure_priv = false;
                                data = false;
                                type_conv = false
                              }
                            in
                            symb := Some f;
                            f
                        | Some f -> f
                      in
                      (new_vars,Fun(f,new_args),new_rhs)
                  | _ -> input_error "The term should be an application of a function." ext
              ) reduc
            in

            begin match !symb with
              | None -> internal_error "[transform.ml >> expand_tdecl] Unexpected case 2"
              | Some f -> (DReduc (f,new_reduc))::(expand_tdecl (StringMap.add f.id_f.id (EFun f) env) proc q_decl)
            end
        | TReducFail(f,typ_args,typ_r,reduc,options) ->
            let new_f = fresh_id_from_ident f in
            let new_typ_args = List.map (get_type_from_id env) typ_args in
            let new_typ_r = get_type_from_id env typ_r in

            let f =
              {
                id_f = new_f;
                arg_ty = new_typ_args;
                return_ty = new_typ_r;

                priv = (match options with ["private",_] -> true | _ -> false);
                data = false;
                type_conv = false;
                pure_priv = false
              }
            in

            let new_reduc =
              List.map (fun (may_vars,(lhs,ext),rhs) ->
                let new_env, new_may_vars =
                  List.fold_left (fun (acc_env,acc_may_var) ((str,ext),typ,b) ->
                    let new_id = fresh_id_from_ident (str,ext) in
                    let typ_id = get_type_from_id acc_env typ in
                    let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                    (StringMap.add str (EVar v) acc_env, acc_may_var @ [v,b])
                  ) (env,[]) may_vars
                in
                match lhs with
                  | PFunApp((str,f_ext),args) ->
                      if f.id_f.id <> str then input_error "The head symbol is not the same in the declaration." f_ext;

                      let (new_args,new_typ_args') =
                        List.fold_left (fun (acc_args,acc_typ) t ->
                          let (new_t,new_typ) = expand_and_check_pure_priv_term_e new_env t in
                          (acc_args @ [new_t], acc_typ @ [new_typ])
                        ) ([],[]) args
                      and (new_rhs,new_typ_rhs) = expand_and_check_pure_priv_term_e new_env rhs in

                      if not (is_equal_type_list new_typ_args new_typ_args') then input_error "The arguments of the left hand sides of the reduction do not have the same type." f_ext;
                      if not (is_equal_type new_typ_r new_typ_rhs) then input_error "The right hand sides of the reduction do not have the same type." f_ext;

                      (new_may_vars,Fun(f,new_args),new_rhs)
                  | _ -> input_error "The term should be an application of a function." ext
              ) reduc
            in

            (DReducFail(f,new_reduc))::(expand_tdecl (StringMap.add new_f.id (EFun f) env) proc q_decl)
        | TEquation(eq_list,options) ->
            let new_eq_list =
              List.map (fun (vars,lhs,rhs) ->
                let new_env, new_vars =
                  List.fold_left (fun (acc_env,acc_var) ((str,ext),typ) ->
                    let new_id = fresh_id_from_ident (str,ext) in
                    let typ_id = get_type_from_id acc_env typ in
                    let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                    (StringMap.add str (EVar v) acc_env, acc_var @ [v])
                  ) (env,[]) vars
                in

                let new_lhs,_ = expand_and_check_pure_priv_term_e new_env lhs in
                let new_rhs,_ = expand_and_check_pure_priv_term_e new_env rhs in
                (new_vars,new_lhs,new_rhs)
              ) eq_list
            in

            (DEquation(new_eq_list,options))::(expand_tdecl env proc q_decl)
        | TPredDecl(id,typ_args,options) ->
            let new_id = fresh_id_from_ident id in
            let new_typ_args = List.map (get_type_from_id env) typ_args in
            let f =
              {
                id_f = new_id;
                arg_ty = new_typ_args;
                return_ty = bool_id;
                pure_priv = false;
                priv = false;
                data = false;
                type_conv = false
              }
            in

            (DPred(f,options))::(expand_tdecl (StringMap.add new_id.id (EFun f) env) proc q_decl)
        | TTableDecl(id,typ_args,options) ->
            let new_id = fresh_id_from_ident id in
            let new_typ_args = List.map (get_type_from_id env) typ_args in
            let precise_options = ref TblNone in
            let uaction = ref false in
            List.iter (fun (str,ext) -> match str with
              | str when str = precise_str ->
                  if !precise_options <> TblNone || !uaction
                  then input_error (Printf.sprintf "The table option %s cannot appear at the same time at the options %s, %s, %s and %s." precise_str unique_action_str locked_round_str locked_insert_str locked_value_str) ext;

                  input_warning (Printf.sprintf "The table %s has been declared with the option %s. Note that the front-end will try to find cells that lock this table. If no such cell exists, the front-end consider the table as unlocked. Thus it is imperative to declare the channels locking the table as cells (using the options precise, cell or counter)."
                    new_id.id precise_str) ext;
                  precise_options := TblPrecise;
                  uaction := true
              | str when str = locked_round_str ->
                  if !precise_options <> TblNone
                  then input_error (Printf.sprintf "The table option %s cannot appear at the same time at the options %s, %s and %s." locked_round_str precise_str locked_insert_str locked_value_str) ext;

                  input_warning (Printf.sprintf "The table %s has been declared with the option %s. Note that the front-end will try to find cells that lock this table. If no such cell exists, the front-end consider the table as unlocked. Thus it is imperative to declare the channels locking the table as cells (using the options precise, cell or counter)."
                    new_id.id locked_round_str) ext;

                  precise_options := TblLockedPerRound
              | str when str = locked_insert_str ->
                  if !precise_options <> TblNone
                  then input_error (Printf.sprintf "The table option %s cannot appear at the same time at the options %s, %s and %s." locked_insert_str precise_str locked_round_str locked_value_str) ext;

                  input_warning (Printf.sprintf "The table %s has been declared with the option %s. Note that the front-end will try to find cells that lock this table. If no such cell exists, the front-end consider the table as unlocked. Thus it is imperative to declare the channels locking the table as cells (using the options precise, cell or counter)."
                    new_id.id locked_insert_str) ext;

                  precise_options := TblLockedPerInsert
              | str when str = locked_value_str ->
                  if !precise_options <> TblNone
                  then input_error (Printf.sprintf "The table option %s cannot appear at the same time at the options %s, %s and %s." locked_value_str precise_str locked_round_str locked_insert_str) ext;

                  precise_options := TblValue
              | str when str = unique_action_str ->
                  if !uaction
                  then input_error (Printf.sprintf "The table option %s cannot appear at the same time at the option %s." unique_action_str precise_str) ext;
                  uaction := true
              | _ -> input_error (Printf.sprintf "Wrong option: A table can have options from %s, %s, %s and %s." precise_str unique_action_str locked_round_str locked_insert_str) ext
            ) options;

            let status = match !precise_options with
              | TblNone
              | TblValue -> STNone
              | _ -> STUnknown
            in

            let tbl =
              {
                id_t = new_id;
                arg_ty_t = new_typ_args;
                options_t = !precise_options;
                uaction_t = !uaction;
                status_t = status
              }
            in

            (DTable tbl)::(expand_tdecl (StringMap.add new_id.id (ETable tbl) env) proc q_decl)
        | TSet (id,value) -> (DSet (id,value))::(expand_tdecl env proc q_decl)
        | TPDef(id,may_vars,p) ->
            let new_id = fresh_id_from_ident id in

            let new_typ_args = List.map (fun (_,typ,_) -> get_type_from_id env typ) may_vars in

            let generate_process args_list =
              let rec gen_env env typ_l vars args = match typ_l,vars,args with
                | [],[],[] -> env
                | [],_,_ | _,[],_ | _,_,[] -> internal_error "[transform.ml >> expand_tdecl] Uncorrect list sizes."
                | ty::q_t, ((str_v,_),_,_)::q_v, arg::q_a -> gen_env (StringMap.add str_v (EArg (arg,ty)) env) q_t q_v q_a
              in
              let new_env = gen_env env new_typ_args may_vars args_list in
              expand_tprocess new_env p
            in

            gather_restriction env p;


            expand_tdecl (StringMap.add new_id.id (EProcess(new_id,new_typ_args,generate_process)) env) proc q_decl
        | TQuery(vars,queries) ->
            gather_restriction env proc;
            let new_env, new_vars =
              List.fold_left (fun (acc_env,acc_var) ((str,ext),typ) ->
                let new_id = fresh_id_from_ident (str,ext) in
                let typ_id = get_type_from_id acc_env typ in
                let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                (StringMap.add str (EVar v) acc_env, acc_var @ [v])
              ) (env,[]) vars
            in
            let new_queries = List.map (expand_tquery new_env) queries in
            (DQuery(new_vars,new_queries))::(expand_tdecl env proc q_decl)
        | TNot(vars,(t,ext)) ->
            let new_env, new_vars =
              List.fold_left (fun (acc_env,acc_var) ((str,ext),typ) ->
                let new_id = fresh_id_from_ident (str,ext) in
                let typ_id = get_type_from_id acc_env typ in
                let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                (StringMap.add str (EVar v) acc_env, acc_var @ [v])
              ) (env,[]) vars
            in
            if is_fact (t,ext)
            then
              let fact = expand_fact new_env (t,ext) in
              (DNot(new_vars,fact))::(expand_tdecl env proc q_decl)
            else input_error "A \"not\" declaration should only contain facts." ext
        | TNoninterf _
        | TWeaksecret _
        | TNoUnif _
        | TDefine _
        | TExpand _ -> internal_error "[transform.ml >> expand_tdecl] Wrong declaration"
        | TElimtrue(may_vars,t) ->
            let new_env, new_may_vars =
              List.fold_left (fun (acc_env,acc_may_var) ((str,ext),typ,b) ->
                let new_id = fresh_id_from_ident (str,ext) in
                let typ_id = get_type_from_id acc_env typ in
                let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                (StringMap.add str (EVar v) acc_env, acc_may_var @ [v,b])
              ) (env,[]) may_vars
            in
            let t',_ = expand_term_e new_env t in
            (DElimtrue(new_may_vars,t'))::(expand_tdecl env proc q_decl)
        | TClauses clauses_list ->
            let clauses_list' =
              List.map (fun (may_vars,clause) ->
                let new_env, new_may_vars =
                  List.fold_left (fun (acc_env,acc_may_var) ((str,ext),typ,b) ->
                    let new_id = fresh_id_from_ident (str,ext) in
                    let typ_id = get_type_from_id acc_env typ in
                    let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                    (StringMap.add str (EVar v) acc_env, acc_may_var @ [v,b])
                  ) (env,[]) may_vars
                in

                new_may_vars, expand_tclause new_env clause
              ) clauses_list
            in
            (DClauses clauses_list')::(expand_tdecl env proc q_decl)
        | TFree((str,ext),typ,options) ->
            let new_id = fresh_id_from_ident (str,ext) in
            let new_typ = get_type_from_id env typ in
            let is_private = ref false in
            let special_options = ref false in
            let is_precise = ref false in
            let is_uaction = ref false in
            let new_options =
              List.map (function
                | "private",_ -> is_private := true; NDPrivate
                | str,_ when str = precise_str && new_typ = channel_id -> is_precise := true; NDPrecise
                | str,_ when str = unique_action_str && new_typ = channel_id -> is_uaction := true; NDUAction
                | str,_ when str = unique_comm_str && new_typ = channel_id -> special_options := true; NDUComm
                | str,_ when str = cell_str && new_typ = channel_id -> special_options := true; NDCell
                | str,_ when str = counter_str && new_typ = channel_id -> special_options := true; NDCounter
                | _,ext ->
                  input_error
                    (Printf.sprintf "Wrong option for a name: A name can have the option private. Moreover, a private name with channel type can take the options %s, %s, %s, %s and %s."
                      precise_str unique_comm_str unique_action_str cell_str counter_str
                    ) ext
              ) options
            in

            if !special_options && not !is_private
            then input_error (Printf.sprintf "Only channels declared private can have the options %s, %s and %s."
                unique_comm_str cell_str counter_str) ext;

            if (!special_options || !is_uaction) && !is_precise
            then input_error (Printf.sprintf "A channel declared with the option %s cannot also be declared with the options %s, %s, %s or %s."
                precise_str unique_comm_str unique_action_str cell_str counter_str) ext;

            if List.mem NDUComm new_options && List.exists (fun op -> op = NDCell || op = NDCounter) new_options
            then input_error (Printf.sprintf "A channel cannot be declared with the options %s and either %s or %s." unique_comm_str cell_str counter_str) ext;

            if (!is_precise || !is_uaction || !special_options) && not (is_equal_type new_typ channel_id)
            then input_error (Printf.sprintf "Only name of type channel can be declared with the options %s, %s, %s, %s or %s." unique_comm_str cell_str counter_str precise_str unique_action_str) ext;

            let status_n =
              if (!is_private && !is_precise) || List.exists (fun op -> op = NDCell || op = NDCounter) new_options
              then SCCell None
              else if List.mem NDUComm new_options
              then SCPrivate
              else if !is_uaction || (not !is_private && !is_precise)
              then SCPublic
              else SCNone
            in

            let new_n =
              {
                id_n = new_id;
                typ_n = new_typ;
                options_n = new_options;
                status_n = status_n;
                ext_n = ext
              }
            in

            (DFree new_n)::(expand_tdecl (StringMap.add new_id.id (EName new_n) env) proc q_decl)
        | TLetFun(id,may_vars,t) ->
            let new_id = fresh_id_from_ident id in
            let new_env, new_may_vars =
              List.fold_left (fun (acc_env,acc_may_var) ((str,ext),typ,b) ->
                let new_id = fresh_id_from_ident (str,ext) in
                let typ_id = get_type_from_id acc_env typ in
                let v = { id_v = new_id; typ_v = typ_id; status_v = SCNone; options_v = [] } in
                (StringMap.add str (EVar v) acc_env, acc_may_var @ [v,b])
              ) (env,[]) may_vars
            in

            let new_typ_args = List.map (fun (v,_) -> v.typ_v) new_may_vars in

            let new_term,return_typ = expand_pterm_e new_env t in

            let f =
              {
                id_f = new_id;
                arg_ty = new_typ_args;
                return_ty = return_typ;
                priv = false;
                data = false;
                type_conv = false;
                pure_priv = false
              }
            in

            let new_env' = StringMap.add new_id.id (EFun f) env in

            (DLetFun (f,new_may_vars,new_term))::(expand_tdecl new_env' proc q_decl)

let rec set_ignore_type = function
  | [] -> ()
  | DSet(id,value)::q ->
      begin match id,value with
        | ("ignoreTypes",_), S ("true",_)
        | ("ignoreTypes",_), S ("all",_) -> ignore_type := true
        | ("ignoreTypes",_), S ("false",_)
        | ("ignoreTypes",_), S ("none",_)
        | ("ignoreTypes",_), S ("attacker",_) -> ignore_type := false
        | _,_ -> ()
      end;
      set_ignore_type q
  | _::q -> set_ignore_type q

(****************************************
***         Analyse channels          ***
*****************************************)

(* Analyse the process to determine the status of channels and tables *)

module CompId =
  struct
    type t = id
    let compare  = Pervasives.compare
  end

module IdSet = Set.Make(CompId)

module CompName =
  struct
    type t = name
    let compare n1 n2 = Pervasives.compare n1.id_n n2.id_n
  end

module NameSet = Set.Make(CompName)

module NameMap = Map.Make(CompName)

module CompVar =
  struct
    type t = var
    let compare n1 n2 = Pervasives.compare n1.id_v n2.id_v
  end

module VarMap = Map.Make(CompVar)

module VarSet = Set.Make(CompVar)

module CompTable =
  struct
    type t = table
    let compare n1 n2 = Pervasives.compare n1.id_t n2.id_t
  end

module TableMap = Map.Make(CompTable)

module CompInt =
  struct
    type t = int
    let compare n1 n2 = Pervasives.compare n1 n2
  end

module IntSet = Set.Make(CompInt)

(* Update functions *)

let downgrade_channel c new_status reason =
  if new_status = SCPublic && List.mem NDUComm c.options_n
  then input_error (Printf.sprintf "The channel %s was declared with as a strong private channel (option %s) but it was detected that this channel does not satisfy the condition to be a strong private channel: the channel does not only occur as the first argument of inputs, outputs or in associative channel table."
        c.id_n.id unique_comm_str) c.ext_n;

  if new_status = SCPublic && List.exists (fun d -> d = NDCell || d = NDCounter) c.options_n
  then input_error (Printf.sprintf "The channel %s was declared with as cell (option %s or %s) but it was detected that this channel does not satisfy the conditions to be a cell: the channel does not only occur as the first argument of inputs, outputs or in associative channel table."
        c.id_n.id cell_str counter_str) c.ext_n;

  if new_status = SCPrivate && List.exists (fun d -> d = NDCell || d = NDCounter) c.options_n
  then input_error (Printf.sprintf "The channel %s was declared with as cell (option %s or %s) but it was detected that this channel does not satisfy the conditions to be a cell: %s"
        c.id_n.id cell_str counter_str reason) c.ext_n;

  match c.status_n with
    | SCNone
    | SCPublic
    | SCPrivate -> ()
    | SCCell None -> c.status_n <- new_status
    | SCCell (Some status) ->
        let explore_table tbl = match tbl.status_t with
          | STUnknown
          | STNone -> internal_error "[transform.ml >> downgrade_cell] A table cannot have at the same time unknown or none status and appearing in channel status."
          | STUnlocked -> ()
          | STLocked n_l ->
              let new_name_list = List.filter (fun n -> n.id_n <> c.id_n) n_l in
              if new_name_list = []
              then tbl.status_t <- STUnlocked
              else tbl.status_t <- STLocked new_name_list
        in

        let explore_status_nat = function
          | TableRound (tbl,true)
          | TableInsert (tbl,true) -> explore_table tbl
          | _ -> ()
        in

        let rec explore_status_pattern = function
          | SOpen _
          | SBlackBox _ -> ()
          | SNat(snat_l,_) -> List.iter explore_status_nat snat_l
          | STuple spat_l | SData(_,spat_l) -> List.iter explore_status_pattern spat_l
        in

        explore_status_pattern status;
        c.status_n <- new_status

let downgrade_var v new_status =
  begin match new_status with
    | SCCell _ -> internal_error "[transform.ml >> downgrade_var] Unexpected case."
    | _ -> ()

  end;
  match v.status_v with
    | SCCell None -> v.status_v <- new_status
    | _-> ()

(* Strong private channel verification *)

let rec verify_channel_in_term = function
  | Var v when v.status_v <> SCNone -> v.status_v <- SCPublic
  | Var _
  | Sum _
  | Nat _ -> ()
  | Name n ->
      if n.typ_n = channel_id
      then downgrade_channel n SCPublic ""
  | Fun(f,_) when f.pure_priv -> ()
  | Fun(_,args) | Tuple args -> List.iter verify_channel_in_term args
  | Restr(_,_,t) -> verify_channel_in_term t
  | Test(cond,t_then,t_else_op) ->
      verify_channel_in_term cond;
      verify_channel_in_term t_then;
      verify_channel_in_term_option t_else_op
  | Let(pat,cond,t_then,t_else_op) ->
      verify_channel_in_pattern pat;
      verify_channel_in_term cond;
      verify_channel_in_term t_then;
      verify_channel_in_term_option t_else_op
  | LetFilter(_,cond,t_then,t_else_op) ->
      verify_channel_in_term cond;
      verify_channel_in_term t_then;
      verify_channel_in_term_option t_else_op

and verify_channel_in_term_option = function
  | None -> ()
  | Some t -> verify_channel_in_term t

and verify_channel_in_pattern = function
  | PatVar _ -> ()
  | PatEq t -> verify_channel_in_term t
  | PatTuple pat_args | PatData(_,pat_args) -> List.iter verify_channel_in_pattern pat_args

(* Utility functions *)

let is_name = function
  | Name _ -> true
  | _ -> false

let name_of_term = function
  | Name n -> n
  | _ -> internal_error "[transform.ml >> name_of_term] Unexpected case"

let is_var = function
  | Var _ -> true
  | _ -> false

let var_of_term = function
  | Var v -> v
  | _ -> internal_error "[transform.ml >> var_of_term] Unexpected case"

let rec term_of_pattern = function
  | PatVar v -> Var v
  | PatEq t -> t
  | PatTuple args -> Tuple (List.map term_of_pattern args)
  | PatData(f,args) -> Fun(f,List.map term_of_pattern args)

let is_cell c = match c.status_n with
  | SCCell _ -> true
  | _ -> false

let is_unused_cell c = match c.status_n with
  | SCCell None -> true
  | _ -> false

let is_var_cell c = match c.status_v with
  | SCCell _ -> true
  | _ -> false

let exists_input_on_channel scope_channel c =
  try
    match NameMap.find c scope_channel with
      | None -> false
      | _ -> true
  with Not_found -> internal_error "[transform.ml >> exists_input_on_channel] The channel should be in the scope."

let exists_input_on_channel_var scope_variable v =
  try
    match VarMap.find v scope_variable with
      | None -> false
      | _ -> true
  with Not_found -> internal_error "[transform.ml >> exists_input_on_channel_var] The channel should be in the scope."

(* Analyse test in if-the-else conditional *)

(* Modification pattern *)

let rec is_equal_term t1 t2 = match t1,t2 with
  | Var v1, Var v2 -> v1 == v2
  | Name n1, Name n2 -> n1 == n2
  | Sum(v1,n1), Sum(v2,n2) -> v1 == v2 && n1 = n2
  | Nat n1, Nat n2 -> n1 = n2
  | Fun(f1,args1), Fun(f2,args2) -> f1 == f2 && List.for_all2 is_equal_term args1 args2
  | Tuple args1, Tuple args2 -> List.for_all2 is_equal_term args1 args2
  | Test(t1,t1',t1_op), Test(t2,t2',t2_op) ->
      let test_op = match t1_op, t2_op with
        | None,None -> true
        | Some t1, Some t2 -> is_equal_term t1 t2
        | _ -> false
      in
      is_equal_term t1 t2 && is_equal_term t1' t2' && test_op
  | _,_ -> false

let rec find_inequalities = function
  | Fun(f,args) when f.id_f.id = "&&" ->
      List.fold_left (fun acc t -> (find_inequalities t)@acc) [] args
  | Fun(f,[t1;t2]) when f.id_f.id = "<"  ->
      begin match t1,t2 with
        | Var _, _
        | _, Var _ -> [(t1,t2)]
        | Sum(v,i), Sum(v',i') ->
            let i_min = min i i' in

            if i = i_min && i' = i_min
            then [(Var v,Var v')]
            else if i = i_min
            then [Var v,Sum(v',i' - i_min)]
            else [Sum(v,i-i_min),Var v']
        | _, _ -> []
      end
  | Fun(f,[t1;t2]) when f.id_f.id = "<="  ->
      begin match t1,t2 with
        | Sum(v,i), Var _ when i > 0 -> [Sum(v,i-1),t2]
        | Sum(v,i), Sum(v',i') ->
            let i_min = min i i' in

            if i = i_min && i' = i_min
            then []
            else if i = i_min
            then []
            else if i-i_min = 1
            then [Var v, Var v']
            else [Sum(v,i-i_min-1),Var v']
        | _, _ -> []
      end
  | _ -> []

let is_less ineq_list t1 t2 =
  let ineq = match t1, t2 with
    | Var _, _
    | _, Var _ -> Some (t1,t2)
    | Sum(v,i), Sum(v',i') ->
        let i_min = min i i' in

        if i = i_min && i' = i_min
        then Some (Var v,Var v')
        else if i = i_min
        then Some (Var v,Sum(v',i' - i_min))
        else Some (Sum(v,i-i_min),Var v')
    | _, _ -> None
  in

  let within_ineq = match ineq with
    | None -> false
    | Some(t1',t2') ->
        List.exists (fun (t1'',t2'') -> is_equal_term t1'' t1' && is_equal_term t2'' t2') ineq_list
  in

  if within_ineq
  then true
  else
    match t1,t2 with
      | Var v, Sum(v',n) when v == v' && n > 0 -> true
      | Sum(v,n), Sum(v',n') when v == v' && n < n' -> true
      | _ -> false

(* Status patterns *)

let rec status_pattern_of_initialisation = function
  | Var v when v.typ_v == nat_id -> SNat([],None)
  | Nat _
  | Sum _ -> SNat ([],None)
  | Fun(f,args) when f.data -> SData(f,List.map status_pattern_of_initialisation args)
  | Tuple(args) -> STuple(List.map status_pattern_of_initialisation args)
  | t ->
      let typ = get_type t in
      SBlackBox typ

let rec status_pattern_of_pattern = function
  | PatVar v when v.typ_v == nat_id -> SNat([],None)
  | PatVar v -> SOpen v.typ_v
  | PatEq t -> status_pattern_of_initialisation t
  | PatTuple args -> STuple(List.map status_pattern_of_pattern args)
  | PatData(f,args) -> SData(f,List.map status_pattern_of_pattern args)

exception Incoherent_type

let get_type_status_pattern = function
  | SOpen typ
  | SBlackBox typ -> typ
  | SNat _ -> nat_id
  | STuple _ -> bitstring_id
  | SData(f,_) -> f.return_ty

let rec status_pattern_of_input_output ineq_list t_in t_out = match t_in, t_out with
  | t_in, t_out when (get_type t_in) == nat_id && (get_type t_out) == nat_id ->
      if is_less ineq_list t_in t_out || is_equal_term t_in t_out
      then SNat([], None)
      else SBlackBox nat_id
  | Var v, Var v' when v == v' -> SOpen v.typ_v
  | Fun(f,args), Fun(f',args') when f == f' && f.data ->
      begin try
        SData(f,List.map2 (status_pattern_of_input_output ineq_list) args args')
      with Incoherent_type -> SBlackBox f.return_ty
      end
  | Tuple(args), Tuple(args') when List.length args = List.length args' ->
      begin try
        STuple(List.map2 (status_pattern_of_input_output ineq_list) args args')
      with Incoherent_type -> SBlackBox bitstring_id
      end
  | _, _ ->
      let typ_in = get_type t_in in
      let typ_out = get_type t_out in

      if is_equal_type typ_in typ_out
      then SBlackBox typ_in
      else raise Incoherent_type

let rec merge_status_pattern pat1 pat2 = match pat1, pat2 with
  | SOpen typ_id, STuple _ when is_equal_type typ_id bitstring_id -> pat2
  | STuple _, SOpen typ_id when is_equal_type typ_id bitstring_id -> pat1
  | SOpen type_id, SData(f,_) when is_equal_type type_id f.return_ty -> pat2
  | SData(f,_), SOpen type_id when is_equal_type type_id f.return_ty -> pat1
  | SOpen typ1, SOpen typ2 when is_equal_type typ1 typ2 -> pat1
  | SNat _, SNat _ -> pat1
  | STuple(args1), STuple(args2) when List.length args1 = List.length args2 ->
      begin try
        STuple(List.map2 merge_status_pattern args1 args2)
      with Incoherent_type -> SBlackBox bitstring_id
      end
  | SData(f1,args1), SData(f2,args2) when f1 == f2 ->
      begin try
        SData(f1,List.map2 merge_status_pattern args1 args2)
      with Incoherent_type -> SBlackBox f1.return_ty
      end
  | _, _ ->
      let typ1 = get_type_status_pattern pat1 in
      let typ2 = get_type_status_pattern pat2 in

      if is_equal_type typ1 typ2
      then SBlackBox typ1
      else raise Incoherent_type

let update_status c pat = match c.status_n with
  | SCCell None -> c.status_n <- SCCell (Some pat)
  | SCCell (Some pat') ->
      begin try
        c.status_n <- SCCell (Some (merge_status_pattern pat pat'))
      with Incoherent_type -> downgrade_channel c SCPrivate "The content of a cell should be of same type."
      end
  | _ -> internal_error "[transform.ml >> update_status] Update status should only be used on a cell"

let update_var_status v pat = match v.status_v with
  | SCCell None -> v.status_v <- SCCell (Some pat)
  | SCCell (Some pat') ->
      begin try
        v.status_v <- SCCell (Some (merge_status_pattern pat pat'))
      with Incoherent_type -> downgrade_var v SCPrivate
      end
  | _ -> internal_error "[transform.ml >> update_var_status] Update status should only be used on a cell"

(* Table arguments *)

type table_argument_link =
  | TALink of NameSet.t * VarSet.t
  | TAImpossible
  | TAUnknown

let linked_table_channel = ref TableMap.empty

let downgrade_channel_in_link str = function
  | TAImpossible
  | TAUnknown -> ()
  | TALink(c_set,_) -> NameSet.iter (fun c -> downgrade_channel c SCPublic str) c_set

let check_unique_channel tbl link_ref c =
  let alone = ref true in
  TableMap.iter (fun tbl' arg_links ->
    List.iter (fun link_ref' ->
      if tbl' == tbl && link_ref == link_ref'
      then ()
      else
        match !link_ref' with
          | TALink(c_set,_) when NameSet.exists (fun c' -> c == c') c_set ->
              NameSet.iter (fun c' -> downgrade_channel c' SCPublic "A cell can only appear in a unique table and unique argument of the table.") c_set;
              link_ref' := TAImpossible;
              alone := false
          | _ -> ()
    ) arg_links
  ) !linked_table_channel;
  !alone

let rec detect_table_arguments_eprocess_c scope proc_c = match proc_c.proc with
  | CNil -> ()
  | CPar(p1,p2)
  | CTest(_,p1,p2)
  | CLet(_,_,p1,p2)
  | CLetFilter(_,_,p1,p2) ->
      detect_table_arguments_eprocess_c scope p1;
      detect_table_arguments_eprocess_c scope p2
  | CRepl p
  | CInput(_,_,p)
  | COutput(_,_,p)
  | CEvent(_,_,_,p) -> detect_table_arguments_eprocess_c scope p
  | CRestr(n,_,p) ->
      if n.typ_n = channel_id && n.status_n <> SCNone
      then detect_table_arguments_eprocess_c (NameSet.add n scope) p
      else detect_table_arguments_eprocess_c scope p
  | CInsert(tbl,args,p) ->
      let arg_links = TableMap.find tbl !linked_table_channel in

      List.iter2 (fun t link_ref ->
        if is_name t
        then
          let c = name_of_term t in
          if c.typ_n = channel_id && c.status_n <> SCNone
          then
            match !link_ref with
              | TAImpossible -> downgrade_channel c SCPublic ""
              | TAUnknown ->
                  if check_unique_channel tbl link_ref c
                  then link_ref := TALink (NameSet.singleton c,VarSet.empty)
                  else link_ref := TAImpossible
              | TALink (c_set,v_set) ->
                  if check_unique_channel tbl link_ref c
                  then link_ref := TALink (NameSet.add c c_set, v_set)
                  else
                    begin
                      NameSet.iter (fun c -> downgrade_channel c SCPublic "A cell can only appear in a unique table and unique argument of the table.") c_set;
                      link_ref := TAImpossible
                    end
          else
            begin
              downgrade_channel_in_link "When a cell is inserted in a table as ith argument, any other other on that table should have a cell as ith argument." !link_ref;
              link_ref := TAImpossible
            end
        else
          begin
            downgrade_channel_in_link "When a cell is inserted in a table as ith argument, any other other on that table should have a cell as ith argument." !link_ref;
            link_ref := TAImpossible
          end
      ) args arg_links;

      detect_table_arguments_eprocess_c scope p
  | CGet(tbl,pats,None,p1,p2) ->
      let arg_links = TableMap.find tbl !linked_table_channel in

      List.iter2 (fun pat link_ref -> match pat, !link_ref with
        | PatVar v, TAUnknown when v.typ_v == channel_id -> link_ref := TALink(NameSet.empty,VarSet.singleton v)
        | PatVar v, TALink(c_set,v_set) when v.typ_v == channel_id -> link_ref := TALink(c_set,VarSet.add v v_set)
        | _, TALink(c_set,_) ->
            NameSet.iter (fun c -> downgrade_channel c SCPublic "When cells are used as ith arguments of a table, the ith pattern of any Get on this table should necessarily be a fresh variable.") c_set;
            link_ref := TAImpossible
        | _, _ -> link_ref := TAImpossible
      ) pats arg_links;

      detect_table_arguments_eprocess_c scope p1;
      detect_table_arguments_eprocess_c scope p2
  | CGet(tbl,_,_,p1,p2) ->
      let arg_links = TableMap.find tbl !linked_table_channel in
      List.iter (fun link_ref ->
        downgrade_channel_in_link "When cells are used in a table, we do not allow GET operations on this table with predicate declared by 'suchthat'." !link_ref;
        link_ref := TAImpossible
      ) arg_links;
      detect_table_arguments_eprocess_c scope p1;
      detect_table_arguments_eprocess_c scope p2

let rec detect_table_arguments_decl scope = function
  | [] -> ()
  | (DFree n)::q_decl when n.status_n <> SCNone && n.typ_n == channel_id -> detect_table_arguments_decl (NameSet.add n scope) q_decl
  | [DProcess p] -> detect_table_arguments_eprocess_c scope p
  | (DProcess _)::_ -> internal_error "[transform.ml >> detect_table_arguments_decl] There should be a unique DProcess at the end of the list."
  | (DTable tbl)::q_decl ->
      let links =
        List.fold_right (fun typ acc ->
          if typ == channel_id
          then (ref TAUnknown)::acc
          else (ref TAImpossible)::acc
        ) tbl.arg_ty_t []
      in
      linked_table_channel := TableMap.add tbl links !linked_table_channel;
      detect_table_arguments_decl scope q_decl
  | _::q_decl -> detect_table_arguments_decl scope q_decl

let merge_name_decl list1 list2 =
  List.fold_left (fun acc d ->
    if List.exists (fun d' -> d = d') acc
    then acc
    else d::acc
  ) list1 list2

let assign_linked_variables () =
  TableMap.iter (fun _ links ->
    List.iter (fun link_ref -> match !link_ref with
      | TALink(c_set,v_set) ->
          if NameSet.is_empty c_set
          then link_ref := TAImpossible
          else
            let decl = NameSet.fold (fun c acc -> merge_name_decl c.options_n acc) c_set [] in
            VarSet.iter (fun v -> v.status_v <- SCCell None; v.options_v <- decl) v_set
      | _ -> ()
    ) links
  ) !linked_table_channel

let merge_linked_status_pattern () =
  let merge_status st1 st2 = match st1,st2 with
    | SCNone, _
    | _, SCNone -> internal_error "[transform.ml >> merge_linked_status_pattern] Unexpected case."
    | SCPublic, _
    | _, SCPublic -> SCPublic
    | SCPrivate, _
    | _, SCPrivate -> SCPrivate
    | SCCell None, st
    | st, SCCell None -> st
    | SCCell (Some pat1), SCCell (Some pat2) ->
        begin try
          SCCell (Some (merge_status_pattern pat1 pat2))
        with Incoherent_type -> SCPrivate
        end
  in

  TableMap.iter (fun _ links ->
    List.iter (fun link_ref -> match !link_ref with
      | TAImpossible
      | TAUnknown -> ()
      | TALink(c_set,v_set) ->
          let st1 = NameSet.fold (fun c acc -> merge_status c.status_n acc) c_set (SCCell None) in
          let st2 = VarSet.fold (fun v acc -> merge_status v.status_v acc) v_set st1 in
          NameSet.iter (fun c -> downgrade_channel c st2 "All cells used in the same argument of a table shoudl have the same type.") c_set;
          VarSet.iter (fun v -> v.status_v <- st2) v_set
    ) links
  ) !linked_table_channel

(* Function generate the status pattern. Modification pattern and status table are not generated by this function. *)
let rec analyse_channels_eprocess_c scope_channel scope_variable ineq_list cproc = match cproc.proc with
  | CNil -> NameSet.empty, VarSet.empty
  | CPar(p1,p2) ->
      let c_set1, v_set1 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p1 in
      let c_set2, v_set2 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p2 in

      let inter_c = NameSet.inter c_set1 c_set2 in
      let inter_v = VarSet.inter v_set1 v_set2 in

      NameSet.iter (fun c -> downgrade_channel c SCPrivate "Two processes in parallel cannot directly write on the same cell.") inter_c;
      VarSet.iter (fun v -> downgrade_var v SCPrivate) inter_v;
      NameSet.union c_set1 c_set2, VarSet.union v_set1 v_set2
  | CRepl p ->
      let c_set, v_set = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p in
      NameSet.iter (fun c -> downgrade_channel c SCPrivate "A replicated process cannot directly write on a cell.") c_set;
      VarSet.iter (fun v-> downgrade_var v SCPrivate) v_set;
      c_set, v_set
  | CRestr(n,_,p) ->
      if n.typ_n = channel_id && n.status_n <> SCNone
      then
        let c_set, v_set = analyse_channels_eprocess_c (NameMap.add n None scope_channel) scope_variable ineq_list p in
        NameSet.remove n c_set, v_set
      else analyse_channels_eprocess_c scope_channel scope_variable ineq_list p
  | CTest(t,p_then,p_else) ->
      verify_channel_in_term t;

      let c_set1, v_set1 = analyse_channels_eprocess_c scope_channel scope_variable ((find_inequalities t)@ineq_list) p_then in
      let c_set2, v_set2 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_else in

      NameSet.union c_set1 c_set2, VarSet.union v_set1 v_set2
  | CInput(ch,pat,p) when is_name ch && (name_of_term ch).status_n <> SCNone ->
      verify_channel_in_pattern pat;

      let c = name_of_term ch in
      let tpat = term_of_pattern pat in

      if exists_input_on_channel scope_channel c
      then downgrade_channel c SCPrivate "The process following an input on a cell cannot directly read on the same cell.";

      if is_cell c
      then
        begin try
          update_status c (status_pattern_of_pattern pat)
        with Incoherent_type -> downgrade_channel c SCPrivate "The content of a cell should be of same type."
        end;

      let new_scope_channel = NameMap.add c (Some tpat) scope_channel in

      let c_set, v_set = analyse_channels_eprocess_c new_scope_channel scope_variable ineq_list p in
      NameSet.remove c c_set, v_set
  | CInput(ch,pat,p) when is_var ch && (var_of_term ch).status_v <> SCNone ->
      verify_channel_in_pattern pat;

      let v = var_of_term ch in
      let tpat = term_of_pattern pat in

      if exists_input_on_channel_var scope_variable v
      then downgrade_var v SCPrivate;

      if is_var_cell v
      then
        begin try
          update_var_status v (status_pattern_of_pattern pat)
        with Incoherent_type -> downgrade_var v SCPrivate
        end;

      let new_scope_variable = VarMap.add v (Some tpat) scope_variable in

      let c_set, v_set = analyse_channels_eprocess_c scope_channel new_scope_variable ineq_list p in
      c_set, VarSet.remove v v_set
  | CInput(ch,pat,p) ->
      verify_channel_in_term ch;
      verify_channel_in_pattern pat;
      analyse_channels_eprocess_c scope_channel scope_variable ineq_list p
  | COutput(ch,t_out,p) when is_name ch && (name_of_term ch).status_n <> SCNone ->
      verify_channel_in_term t_out;

      let c = name_of_term ch in

      if is_cell c
      then
        begin
          try
            match NameMap.find c scope_channel with
              | None -> update_status c (status_pattern_of_initialisation t_out)
              | Some t_in ->
                  begin try
                    update_status c (status_pattern_of_input_output ineq_list t_in t_out)
                  with Incoherent_type -> downgrade_channel c SCPrivate "The content of a cell should be of same type."
                  end
          with Not_found -> internal_error "[transform.ml >> analyse_channels_eprocess_c] The channel should be in the scope."
        end;

      let scope_channel' = NameMap.add c None scope_channel in
      let c_set,v_set = analyse_channels_eprocess_c scope_channel' scope_variable ineq_list p in

      if NameSet.mem c c_set
      then downgrade_channel c SCPrivate "The process following an output on a cell cannot directly write on the same cell.";

      NameSet.add c c_set,v_set
  | COutput(ch,t_out,p) when is_var ch && (var_of_term ch).status_v <> SCNone ->
      verify_channel_in_term t_out;

      let v = var_of_term ch in

      if is_var_cell v
      then
        begin
          try
            match VarMap.find v scope_variable with
              | None -> update_var_status v (status_pattern_of_initialisation t_out)
              | Some t_in ->
                  begin try
                    update_var_status v (status_pattern_of_input_output ineq_list t_in t_out)
                  with Incoherent_type -> downgrade_var v SCPrivate
                  end
          with Not_found -> internal_error "[transform.ml >> analyse_channels_eprocess_c] The channel should be in the scope."
        end;

      let scope_variable' = VarMap.add v None scope_variable in
      let c_set,v_set = analyse_channels_eprocess_c scope_channel scope_variable' ineq_list p in

      if VarSet.mem v v_set
      then downgrade_var v SCPrivate ;

      c_set,VarSet.add v v_set
  | COutput(ch,t,p) ->
      verify_channel_in_term ch;
      verify_channel_in_term t;
      analyse_channels_eprocess_c scope_channel scope_variable ineq_list p
  | CLet(pat,cond,p_then,p_else) ->
      verify_channel_in_term cond;
      verify_channel_in_pattern pat;

      let c_set1, v_set1 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_then in
      let c_set2, v_set2 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_else in

      NameSet.union c_set1 c_set2, VarSet.union v_set1 v_set2
  | CLetFilter(_,cond,p_then,p_else) ->
      verify_channel_in_term cond;

      let c_set1, v_set1 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_then in
      let c_set2, v_set2 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_else in

      NameSet.union c_set1 c_set2, VarSet.union v_set1 v_set2
  | CEvent(_,_,_,p) -> analyse_channels_eprocess_c scope_channel scope_variable ineq_list p
  | CInsert(tbl,args,p) ->
      let arg_link = TableMap.find tbl !linked_table_channel in
      List.iter2 (fun t link_ref -> match !link_ref with
        | TALink _ -> ()
        | _ -> verify_channel_in_term t
      ) args arg_link;
      analyse_channels_eprocess_c scope_channel scope_variable ineq_list p
  | CGet(tbl,pat_l,t_op,p_then,p_else) ->
      let arg_link = TableMap.find tbl !linked_table_channel in
      let vars = ref [] in
      List.iter2 (fun pat link_ref -> match !link_ref, pat with
        | TALink _, PatVar v -> vars := v::!vars
        | TALink _, _ -> internal_error "[transform.ml >> analyse_channels_eprocess_c] Unexpected case in the GET operator"
        | _,_ -> verify_channel_in_pattern pat
      ) pat_l arg_link;
      verify_channel_in_term_option t_op;

      let scope_variable' = List.fold_left (fun acc v -> VarMap.add v None acc) scope_variable !vars in
      let c_set1, v_set1 = analyse_channels_eprocess_c scope_channel scope_variable' ineq_list p_then in
      let c_set2, v_set2 = analyse_channels_eprocess_c scope_channel scope_variable ineq_list p_else in

      let v_set1' =
        List.fold_left (fun acc v ->
          if VarSet.mem v v_set1
          then downgrade_var v SCPrivate;

          VarSet.remove v acc
        ) v_set1 !vars
      in

      NameSet.union c_set1 c_set2, VarSet.union v_set1' v_set2

let rec analyse_channels_decl scope_channel = function
  | [] -> ()
  | (DFree n)::q_decl when n.status_n <> SCNone && n.typ_n == channel_id -> analyse_channels_decl (NameMap.add n None scope_channel) q_decl
  | [DProcess p] ->
      let _ = analyse_channels_eprocess_c scope_channel VarMap.empty [] p in
      merge_linked_status_pattern ()
  | (DProcess _)::_ -> internal_error "[transform.ml >> analyse_channels_decl] There should be a unique DProcess at the end of the list."
  | _::q_decl -> analyse_channels_decl scope_channel q_decl

(****************************************
***     Expand Open status pattern    ***
*****************************************)

let get_status c = match c.status_n with
  | SCCell (Some st) -> st
  | _ -> internal_error "[transform.ml >> get_status] Unexpected case."

let get_var_status v = match v.status_v with
  | SCCell (Some st) -> st
  | _ -> internal_error "[transform.ml >> get_var_status] Unexpected case."

let rec get_vars = function
  | Var v
  | Sum (v,_) -> VarSet.singleton v
  | Name _
  | Nat _ -> VarSet.empty
  | Fun(_,args)
  | Tuple args -> List.fold_left (fun acc t -> VarSet.union acc (get_vars t)) VarSet.empty args
  | Restr(_,_,t) -> get_vars t
  | Test(t1,t2,t_op)
  | Let(_,t1,t2,t_op)
  | LetFilter(_,t1,t2,t_op) ->
      let v1 = get_vars t1 in
      let v2 = VarSet.union v1 (get_vars t2) in
      begin match t_op with
        | None -> v2
        | Some t -> VarSet.union v2 (get_vars t)
      end

let rec generate_pattern_from_status_pattern = function
  | SOpen typ
  | SBlackBox typ -> PatVar (fresh_var typ)
  | SNat _ -> PatVar (fresh_var nat_id)
  | STuple args -> PatTuple(List.map generate_pattern_from_status_pattern args)
  | SData(f,args) -> PatData(f,List.map generate_pattern_from_status_pattern args)

let rec compare_status_pattern_and_pattern env status pat = match status, pat with
  | STuple st_args, PatTuple args ->
      let new_args,new_env =
        List.fold_left2 (fun (acc_args,acc_env) st pat ->
          let (pat',env') = compare_status_pattern_and_pattern acc_env st pat in
          (acc_args @ [pat'], env')
        ) ([],env) st_args args
      in
      PatTuple new_args, new_env
  | SData(f,st_args), PatData(f',args) when f == f' ->
      let new_args,new_env =
        List.fold_left2 (fun (acc_args,acc_env) st pat ->
          let (pat',env') = compare_status_pattern_and_pattern acc_env st pat in
          (acc_args @ [pat'], env')
        ) ([],env) st_args args
      in
      PatData(f,new_args), new_env
  | SData _, PatData _ -> internal_error "[transform.ml >> compare_status_pattern_and_pattern] The function symbols should be the same."
  | STuple _, PatVar v
  | SData _, PatVar v ->
      let new_pat = generate_pattern_from_status_pattern status in
      let t_new_pat = term_of_pattern new_pat in
      let new_env = VarMap.add v t_new_pat env in
      new_pat, new_env
  | _, _ -> expand_wrt_status_pattern env pat, env

and expand_wrt_status_eterm env = function
  | Var v ->
      if VarMap.mem v env
      then VarMap.find v env
      else Var v
  | Fun(f,args) -> Fun(f,List.map (expand_wrt_status_eterm env) args)
  | Tuple args -> Tuple (List.map (expand_wrt_status_eterm env) args)
  | Restr(n,vars,t) -> Restr(n,vars,expand_wrt_status_eterm env t)
  | Test(t1,t2,t_op) ->
      let t1' = expand_wrt_status_eterm env t1 in
      let t2' = expand_wrt_status_eterm env t2 in
      let t_op' = match t_op with
        | None -> None
        | Some t -> Some (expand_wrt_status_eterm env t)
      in
      Test(t1',t2',t_op')
  | Let(pat,t1,t2,t_op) ->
      let pat' = expand_wrt_status_pattern env pat in
      let t1' = expand_wrt_status_eterm env t1 in
      let t2' = expand_wrt_status_eterm env t2 in
      let t_op' = match t_op with
        | None -> None
        | Some t -> Some (expand_wrt_status_eterm env t)
      in
      Let(pat',t1',t2',t_op')
  | LetFilter(vars,t1,t2,t_op) ->
      let t1' = expand_wrt_status_eterm env t1 in
      let t2' = expand_wrt_status_eterm env t2 in
      let t_op' = match t_op with
        | None -> None
        | Some t -> Some (expand_wrt_status_eterm env t)
      in
      LetFilter(vars,t1',t2',t_op')
  | t -> t

and expand_wrt_status_pattern env = function
  | PatEq t -> PatEq (expand_wrt_status_eterm env t)
  | PatTuple args -> PatTuple (List.map (expand_wrt_status_pattern env) args)
  | PatData(f,args) -> PatData(f,List.map (expand_wrt_status_pattern env) args)
  | pat -> pat

let rec expand_wrt_status_eprocess env = function
  | CNil -> CNil
  | CPar(p1,p2) -> CPar(expand_wrt_status_eprocess_c env p1, expand_wrt_status_eprocess_c env p2)
  | CRepl p -> CRepl(expand_wrt_status_eprocess_c env p)
  | CRestr(n,vars,p) ->
      let vars' = match vars with
        | None -> None
        | Some v_list ->
            let vset =
              List.fold_left (fun acc v ->
                if VarMap.mem v env
                then VarSet.union (get_vars (VarMap.find v env)) acc
                else VarSet.add v acc
              ) VarSet.empty v_list
            in
            Some (VarSet.elements vset)
      in
      CRestr(n,vars',expand_wrt_status_eprocess_c env p)
  | CTest(t,p1,p2) ->
      let t' = expand_wrt_status_eterm env t in
      let p1' = expand_wrt_status_eprocess_c env p1 in
      let p2' = expand_wrt_status_eprocess_c env p2 in
      CTest(t',p1',p2')
  | CInput(ch,pat,p) when is_name ch && is_cell (name_of_term ch) ->
      let c = name_of_term ch in
      let status = get_status c in

      let pat',env' = compare_status_pattern_and_pattern env status pat in
      let p' = expand_wrt_status_eprocess_c env' p in
      CInput(ch,pat',p')
  | CInput(ch,pat,p) when is_var ch && is_var_cell (var_of_term ch) ->
      let v = var_of_term ch in
      let status = get_var_status v in

      let pat',env' = compare_status_pattern_and_pattern env status pat in
      let p' = expand_wrt_status_eprocess_c env' p in
      CInput(ch,pat',p')
  | CInput(ch,pat,p) ->
      let ch' = expand_wrt_status_eterm env ch in
      let pat' = expand_wrt_status_pattern env pat in
      let p' = expand_wrt_status_eprocess_c env p in
      CInput(ch',pat',p')
  | COutput(ch,t,p) ->
      let ch' = expand_wrt_status_eterm env ch in
      let t' = expand_wrt_status_eterm env t in
      let p' = expand_wrt_status_eprocess_c env p in
      COutput(ch',t',p')
  | CLet(pat,t,p1,p2) ->
      let pat' = expand_wrt_status_pattern env pat in
      let t' = expand_wrt_status_eterm env t in
      let p1' = expand_wrt_status_eprocess_c env p1 in
      let p2' = expand_wrt_status_eprocess_c env p2 in
      CLet(pat',t',p1',p2')
  | CLetFilter(vars,t,p1,p2) ->
      let t' = expand_wrt_status_eterm env t in
      let p1' = expand_wrt_status_eprocess_c env p1 in
      let p2' = expand_wrt_status_eprocess_c env p2 in
      CLetFilter(vars,t',p1',p2')
  | CEvent(ev,args,vars,p) ->
      let vars' = match vars with
        | None -> None
        | Some v_list ->
            let vset =
              List.fold_left (fun acc v ->
                if VarMap.mem v env
                then VarSet.union (get_vars (VarMap.find v env)) acc
                else VarSet.add v acc
              ) VarSet.empty v_list
            in
            Some (VarSet.elements vset)
      in
      let args' = List.map (expand_wrt_status_eterm env) args in
      let p' = expand_wrt_status_eprocess_c env p in
      CEvent(ev,args',vars',p')
  | CInsert(tbl,args,p) ->
      let args' = List.map (expand_wrt_status_eterm env) args in
      let p' = expand_wrt_status_eprocess_c env p in
      CInsert(tbl,args',p')
  | CGet(tbl,args,t_op,p1,p2) ->
      let args' = List.map (expand_wrt_status_pattern env) args in
      let t_op' = match t_op with
        | None -> None
        | Some t -> Some (expand_wrt_status_eterm env t)
      in
      let p1' = expand_wrt_status_eprocess_c env p1 in
      let p2' = expand_wrt_status_eprocess_c env p2 in
      CGet(tbl,args',t_op',p1',p2')

and expand_wrt_status_eprocess_c env proc_c = { proc_c with proc = expand_wrt_status_eprocess env proc_c.proc }

let rec expand_wrt_status_decl = function
  | [] -> []
  | [DProcess p] -> [DProcess (expand_wrt_status_eprocess_c VarMap.empty p)]
  | t::q -> t::(expand_wrt_status_decl q)

(****************************************
***         Transform tables          ***
*****************************************)

let merge_comment com1 com2 = match com1,com2 with
  | None, None -> None
  | Some com, None
  | None, Some com -> Some com
  | Some c1, Some c2 -> Some (c1^" and "^c2)



let rec modif_pattern_of_terms status t_in t_out = match status, t_in, t_out with
  | SData(_,args_pat), Fun(_,args_in), Fun(_,args_out)
  | STuple args_pat, Tuple args_in, Tuple args_out -> MFun(modif_pattern_of_terms_list args_pat args_in args_out)
  | SData _ , _, _
  | STuple _, _, _ -> internal_error "[transform.ml >> modif_pattern_of_terms] Wrong matching with status and terms."
  | _, _, _ -> if is_equal_term t_in t_out then MModif else MBlackBox

and modif_pattern_of_terms_list status_l t_in_l t_out_l = match status_l, t_in_l, t_out_l with
  | [], [], [] -> []
  | [],_,_ | _,[],_ | _,_,[] -> internal_error "[transform.ml >> modif_pattern_of_terms_list] Wrong matching with status and terms."
  | t::q, t_in::q_in, t_out::q_out -> (modif_pattern_of_terms t t_in t_out) :: (modif_pattern_of_terms_list q q_in q_out)

let rec modif_pattern_of_status_pattern = function
  | SBlackBox _
  | SOpen _
  | SNat _ -> MModif
  | STuple args
  | SData(_,args) -> MFun(List.map modif_pattern_of_status_pattern args)

let rec merge_modif_pattern mpat1 mpat2 = match mpat1, mpat2 with
  | MFun(args1), MFun(args2) -> MFun(List.map2 merge_modif_pattern args1 args2)
  | MFun _, _
  | _, MFun _ -> internal_error "[transform.ml >> merge_modif_pattern] Unexpected case"
  | MModif, MModif -> MModif
  | _, _ -> MBlackBox

let modif_pattern_of_round c t_in t_out = match c.status_n with
  | SCCell (Some stat) -> modif_pattern_of_terms stat t_in t_out
  | _ -> internal_error "[transform.ml >> modif_pattern_of_round] Unexpected case."

let modif_pattern_of_round_var v t_in t_out = match v.status_v with
  | SCCell (Some stat) -> modif_pattern_of_terms stat t_in t_out
  | _ -> internal_error "[transform.ml >> modif_pattern_of_round_var] Unexpected case."

let update_modif_pattern c m_pat =

  let rec explore s_pat' m_pat' = match s_pat', m_pat' with
    | STuple(s_args), MFun(m_args) -> STuple(List.map2 explore s_args m_args)
    | SData(f,s_args), MFun(m_args) -> SData(f,List.map2 explore s_args m_args)
    | STuple _, _
    | SData _, _ -> internal_error "[transform.ml >> update_modif_pattern] Unexpected case."
    | SNat(tbl_l, None), MModif -> SNat(tbl_l, Some m_pat)
    | SNat(tbl_l, Some m_pat_old), MModif -> SNat(tbl_l,Some (merge_modif_pattern m_pat_old m_pat))
    | _, _ -> s_pat'
  in

  match c.status_n with
    | SCCell (Some s_pat) -> c.status_n <- SCCell (Some (explore s_pat m_pat))
    | _ -> internal_error "[transform.ml >> update_modif_pattern] Unexpected case (2)."

let update_modif_pattern_var v m_pat =

  let rec explore s_pat' m_pat' = match s_pat', m_pat' with
    | STuple(s_args), MFun(m_args) -> STuple(List.map2 explore s_args m_args)
    | SData(f,s_args), MFun(m_args) -> SData(f,List.map2 explore s_args m_args)
    | STuple _, _
    | SData _, _ -> internal_error "[transform.ml >> update_modif_pattern_var] Unexpected case."
    | SNat(tbl_l, None), MModif -> SNat(tbl_l, Some m_pat)
    | SNat(tbl_l, Some m_pat_old), MModif -> SNat(tbl_l,Some (merge_modif_pattern m_pat_old m_pat))
    | _, _ -> s_pat'
  in

  match v.status_v with
    | SCCell (Some s_pat) -> v.status_v <- SCCell (Some (explore s_pat m_pat))
    | _ -> internal_error "[transform.ml >> update_modif_pattern_var] Unexpected case (2)."

(* Tables utilities*)

let remove_table c tbl =
  let rec explore = function
    | SNat (s_tbl_l,m_pat) ->
        let s_tbl_l' =
          List.filter (function
            | TableRound(t,_) when tbl == t -> false
            | TableInsert(t,_) when tbl == t -> false
            | _ -> true
          ) s_tbl_l
        in
        SNat(s_tbl_l',m_pat)
    | STuple(args) -> STuple(List.map explore args)
    | SData(f,args) -> SData(f,List.map explore args)
    | s_pat -> s_pat
  in

  match c.status_n with
    | SCCell (Some s_pat) -> c.status_n <- SCCell (Some (explore s_pat))
    | _ -> internal_error "[transform.ml >> remove_table] Unexpected case."

let verify_locked_table cell_channel tbl = match tbl.status_t with
  | STNone -> internal_error "[transform.ml >> verify_locked_table] Unexpected case"
  | STUnlocked -> ()
  | STUnknown ->
      if NameMap.is_empty cell_channel
      then tbl.status_t <- STUnlocked
      else tbl.status_t <- STLocked(NameMap.fold (fun c _ acc -> c::acc) cell_channel [])
  | STLocked c_l ->
      let new_c_l =
        List.fold_left (fun acc c ->
          if NameMap.mem c cell_channel
          then c::acc
          else
            begin
              remove_table c tbl;
              acc
            end
        ) [] c_l
      in
      if new_c_l = []
      then tbl.status_t <- STUnlocked
      else tbl.status_t <- STLocked new_c_l

let rec add_status_nat tbl is_round l = match l with
  | [] ->
      if is_round
      then [TableRound (tbl,true)]
      else [TableInsert (tbl,true)]
  | (TableRound(tbl',_))::_ when tbl == tbl' && is_round -> l
  | (TableInsert(tbl',_))::_ when tbl == tbl' && not is_round -> l
  | t::q -> t::(add_status_nat tbl is_round q)

let rec remove_status_nat tbl is_round = function
  | [] ->
      if is_round
      then [TableRound (tbl,false)]
      else [TableInsert (tbl,false)]
  | TableRound(tbl',_)::q when tbl == tbl' && is_round -> TableRound(tbl',false)::q
  | TableInsert(tbl',_)::q when tbl == tbl' && not is_round -> TableInsert(tbl',false)::q
  | t::q -> t::(remove_status_nat tbl is_round q)

let analyse_locked_table tbl is_round c t_in t_out =
  let s_pat = get_status c in
  let m_pat = modif_pattern_of_round c t_in t_out in

  let rec explore_m_pat s_pat m_pat = match s_pat, m_pat with
    | SNat(s_nat_l,m_pat'), MModif -> SNat(remove_status_nat tbl is_round s_nat_l, m_pat')
    | SNat(s_nat_l,m_pat'), MBlackBox -> SNat(add_status_nat tbl is_round s_nat_l, m_pat')
    | STuple(args), MFun(m_args) -> STuple(List.map2 explore_m_pat args m_args)
    | SData(f,args), MFun(m_args) -> SData(f,List.map2 explore_m_pat args m_args)
    | STuple _, _
    | SData _, _
    | SNat _, _ -> internal_error "[transform.ml >> analyse_locked_table] Unexpected case."
    | _, _ -> s_pat
  in

  c.status_n <- SCCell (Some (explore_m_pat s_pat m_pat))

(* Analyse *)

let union_cellMap = NameMap.union (fun _ l1 l2 -> Some (l1 @ l2))

let union_varMap = VarMap.union (fun _ l1 l2 -> Some (l1 @ l2))

let rec analyse_tables_eprocess_c cell_channel cproc = match cproc.proc with
  | CNil -> NameMap.empty
  | CPar(p1,p2) ->
      let res1 = analyse_tables_eprocess_c cell_channel p1 in
      let res2 = analyse_tables_eprocess_c cell_channel p2 in
      union_cellMap res1 res2
  | CRepl p -> analyse_tables_eprocess_c cell_channel p
  | CRestr (_,_,p) -> analyse_tables_eprocess_c cell_channel p
  | CInput(ch,pat,p) when is_name ch ->
      let c = name_of_term ch in

      if is_cell c &&
        TableMap.for_all (fun _ links ->
          List.for_all (fun link_ref -> match !link_ref with
            | TALink(c_set,_) -> NameSet.for_all (fun c' -> not (c == c')) c_set
            | _ -> true
          ) links
        ) !linked_table_channel
      then
        let tpat = term_of_pattern pat in
        let new_cell_channel = NameMap.add c tpat cell_channel in
        let res = analyse_tables_eprocess_c new_cell_channel p in
        NameMap.remove c res
      else analyse_tables_eprocess_c cell_channel p
  | CInput(_,_,p) -> analyse_tables_eprocess_c cell_channel p
  | COutput(ch,t_out,p) when is_name ch ->
      let c = name_of_term ch in

      if is_cell c &&
        TableMap.for_all (fun _ links ->
          List.for_all (fun link_ref -> match !link_ref with
            | TALink(c_set,_) -> NameSet.for_all (fun c' -> not (c == c')) c_set
            | _ -> true
          ) links
        ) !linked_table_channel
      then
        try
          let cell_channel' = NameMap.remove c cell_channel in
          let res = analyse_tables_eprocess_c cell_channel' p in
          NameMap.add c [t_out] res
        with Not_found -> analyse_tables_eprocess_c cell_channel p
      else analyse_tables_eprocess_c cell_channel p
  | COutput(_,_,p) -> analyse_tables_eprocess_c cell_channel p
  | CTest(_,p_then,p_else)
  | CLet(_,_,p_then,p_else)
  | CLetFilter(_,_,p_then,p_else) ->
      let res1 = analyse_tables_eprocess_c cell_channel p_then in
      let res2 = analyse_tables_eprocess_c cell_channel p_else in
      union_cellMap res1 res2
  | CEvent(_,_,_,p) -> analyse_tables_eprocess_c cell_channel p
  | CInsert(tbl,_,p) when tbl.status_t <> STNone ->
      verify_locked_table cell_channel tbl;

      let res = analyse_tables_eprocess_c cell_channel p in

      begin match tbl.status_t with
        | STUnlocked -> ()
        | STLocked c_list ->
            List.iter (fun c ->
              let t_out_l = NameMap.find c res in
              let t_in = NameMap.find c cell_channel in
              List.iter (fun t_out ->
                analyse_locked_table tbl true c t_in t_out;
                analyse_locked_table tbl false c t_in t_out
              ) t_out_l
            ) c_list
        | _ -> internal_error "[transform.ml >> analyse_tables_eprocess_c] Unexpected case."
      end;

      res
  | CInsert(_,_,p) -> analyse_tables_eprocess_c cell_channel p
  | CGet(tbl,_,_,p_then,p_else) when tbl.status_t <> STNone ->
      verify_locked_table cell_channel tbl;
      let res1 = analyse_tables_eprocess_c cell_channel p_then in
      let res2 = analyse_tables_eprocess_c cell_channel p_else in
      let res = union_cellMap res1 res2 in
      begin match tbl.status_t with
        | STUnlocked -> ()
        | STLocked c_list ->
            List.iter (fun c ->
              let t_out_l = NameMap.find c res in
              let t_in = NameMap.find c cell_channel in
              List.iter (fun t_out -> analyse_locked_table tbl true c t_in t_out) t_out_l
            ) c_list
        | _ -> internal_error "[transform.ml >> analyse_tables_eprocess_c] Unexpected case."
      end;
      res
  | CGet(_,_,_,p_then,p_else) ->
      let res1 = analyse_tables_eprocess_c cell_channel p_then in
      let res2 = analyse_tables_eprocess_c cell_channel p_else in
      union_cellMap res1 res2

let rec analyse_tables_decl = function
  | [] -> ()
  | [DProcess p] -> let _ = analyse_tables_eprocess_c NameMap.empty p in ()
  | (DProcess _)::_ -> internal_error "[transform.ml >> analyse_channels_decl] There should be a unique DProcess at the end of the list."
  | _::q -> analyse_tables_decl q

(* Transform *)

type table_transformation_request =
  | TAddCell of table * name * bool (* true when perRound, false when perInsert *)
  | TAddNat of table * name * bool (* true when perRound, false when perInsert *) * var option * bool (* true if + 1 required *)

let is_cell_locking_table tbl c tbl_decl =
  let rec explore = function
    | SOpen _
    | SBlackBox _ -> false
    | SNat (s_nat_l,_) ->
        begin match tbl_decl with
          | TblPrecise ->
              List.exists (function
                | TableRound (tbl',true) when tbl == tbl' -> true
                | TableInsert (tbl',true) when tbl == tbl' -> true
                | _ -> false
              ) s_nat_l
          | TblLockedPerRound ->
              List.exists (function
                | TableRound (tbl',true) when tbl == tbl' -> true
                | _ -> false
              ) s_nat_l
          | _ ->
              List.exists (function
                | TableInsert (tbl',true) when tbl == tbl' -> true
                | _ -> false
              ) s_nat_l
        end
    | STuple(args)
    | SData(_,args) -> List.exists explore args
  in
  match c.status_n with
    | SCCell (Some st) -> explore st
    | _ -> internal_error "[transform.ml >> is_cell_locking_table] Unexpected case."

let apply_table_trans_request_input c tbl_rq pat =

  let new_tbl_rq,vars =
    List.fold_right (fun rq (acc_rq,acc_vars) -> match rq with
      | TAddNat(tbl,c',perRound,None,false) when c == c' ->
          let new_var = fresh_var nat_id in
          (TAddNat(tbl,c',perRound,Some new_var,false))::acc_rq, new_var::acc_vars
      | TAddNat(_,c',_,_,_) when c == c' -> internal_error "[transform.ml >> apply_table_trans_request_input] Unexpected case."
      | _ -> (rq::acc_rq,acc_vars)
    ) tbl_rq ([],[])
  in

  if vars = []
  then tbl_rq, pat, None
  else
    let stat = get_status c in

    let new_pat = match stat, pat with
      | STuple _, PatTuple pat_l ->
          let new_pat_l = (List.map (fun v -> PatVar v) vars) @ pat_l in
          PatTuple new_pat_l
      | STuple _, _ -> internal_error "[transform.ml >> apply_table_trans_request_input] Unexpected case (2)."
      | _,_ ->
          let new_pat_l = List.map (fun v -> PatVar v) vars in
          PatTuple(new_pat_l @ [pat])
    in

    new_tbl_rq,new_pat, Some (Printf.sprintf "%d variables have been added in the pattern for precise tables." (List.length vars))

let apply_table_trans_request_output c tbl_rq t =

  let new_tbl_rq,nat_l =
    List.fold_right (fun rq (acc_rq,acc_nat) -> match rq with
      | TAddNat(tbl,c',perRound,Some v,true) when c == c' ->
          (TAddNat(tbl,c',perRound,None,false))::acc_rq, (Sum (v,1))::acc_nat
      | TAddNat(tbl,c',perRound,Some v,false) when c == c' ->
          (TAddNat(tbl,c',perRound,None,false))::acc_rq, (Var v)::acc_nat
      | TAddNat(tbl,c',perRound,None,false) when c == c' -> TAddNat(tbl,c',perRound,None,false)::acc_rq, (Nat 0)::acc_nat
      | TAddNat(_,c',_,_,_) when c == c' -> internal_error "[transform.ml >> apply_table_trans_request_output] Unexpected case."
      | _ -> (rq::acc_rq,acc_nat)
    ) tbl_rq ([],[])
  in

  if nat_l = []
  then tbl_rq, t, None
  else
    let stat = get_status c in

    let new_t = match stat, t with
      | STuple _, Tuple args -> Tuple (nat_l@args)
      | STuple _, _ -> internal_error "[transform.ml >> apply_table_trans_request_output] Unexpected case (2)."
      | _,_ -> Tuple(nat_l @ [t])
    in

    new_tbl_rq,new_t, Some (Printf.sprintf "%d variables have been added in the term for precise tables." (List.length nat_l))

let rec require_increased_nat tbl is_get = function
  | [] -> []
  | TAddNat(tbl',c,is_round,v,_)::q when tbl == tbl' && (not is_get || is_round) -> TAddNat(tbl',c,is_round,v,true)::(require_increased_nat tbl is_get q)
  | t::q -> t::(require_increased_nat tbl is_get q)

let rec retrieve_name_of_add_cell tbl = function
  | [] -> None
  | TAddCell(tbl',c,b)::_ when tbl == tbl' -> Some(c,b)
  | _::q -> retrieve_name_of_add_cell tbl q

let rec transform_tables_eprocess_c tbl_rq proc_c = match proc_c.proc with
  | CNil -> proc_c
  | CPar(p1,p2) ->
      let p1' = transform_tables_eprocess_c tbl_rq p1 in
      let p2' = transform_tables_eprocess_c tbl_rq p2 in
      { proc_c with proc = CPar(p1',p2') }
  | CRepl p ->
      let p' = transform_tables_eprocess_c tbl_rq p in
      { proc_c with proc = CRepl p' }
  | CRestr(n,v,p) ->
      let p' = transform_tables_eprocess_c tbl_rq p in
      { proc_c with proc = CRestr(n,v,p') }
  | CTest(t,p1,p2) ->
      let p1' = transform_tables_eprocess_c tbl_rq p1 in
      let p2' = transform_tables_eprocess_c tbl_rq p2 in
      { proc_c with proc = CTest(t,p1',p2')}
  | CInput(c,pat,p) ->
      if is_name c && is_cell (name_of_term c)
      then
        let c_name = name_of_term c in
        let new_tbl_rq, new_pat, comment = apply_table_trans_request_input c_name tbl_rq pat in
        let new_p = transform_tables_eprocess_c new_tbl_rq p in
        { proc = CInput(c,new_pat,new_p); comment = merge_comment comment proc_c.comment }
      else
        let new_p = transform_tables_eprocess_c tbl_rq p in
        { proc_c with proc = CInput(c,pat, new_p) }
  | COutput(c,t,p) ->
      if is_name c && is_cell (name_of_term c)
      then
        let c_name = name_of_term c in
        let new_tbl_rq, new_t, comment = apply_table_trans_request_output c_name tbl_rq t in
        let new_p = transform_tables_eprocess_c new_tbl_rq p in
        { proc = COutput(c,new_t,new_p); comment = merge_comment comment proc_c.comment }
      else
        let new_p = transform_tables_eprocess_c tbl_rq p in
        { proc_c with proc = COutput(c,t,new_p) }
  | CLet(pat,t,p1,p2) ->
      let p1' = transform_tables_eprocess_c tbl_rq p1 in
      let p2' = transform_tables_eprocess_c tbl_rq p2 in
      { proc_c with proc = CLet(pat,t,p1',p2') }
  | CLetFilter(vars,t,p1,p2) ->
      let p1' = transform_tables_eprocess_c tbl_rq p1 in
      let p2' = transform_tables_eprocess_c tbl_rq p2 in
      { proc_c with proc = CLetFilter(vars,t,p1',p2') }
  | CEvent(e,t,op,p) ->
      let p' = transform_tables_eprocess_c tbl_rq p in
      { proc_c with proc = CEvent(e,t,op,p') }
  | CInsert(tbl,args,p) when tbl.status_t <> STNone ->
      begin match retrieve_name_of_add_cell tbl tbl_rq with
        | None ->
            let p' = transform_tables_eprocess_c (require_increased_nat tbl false tbl_rq) p in
            { proc_c with proc = CInsert(tbl,args,p') }
        | Some(c,_) ->
            let p' = transform_tables_eprocess_c tbl_rq p in
            let id = fresh_id_from_ident (nat_str,dummy_ext) in
            let v = { id_v = id ; typ_v = nat_id; status_v = SCNone; options_v = [] } in

            let out_c = { proc = COutput(Name c,Sum(v,1),p'); comment = None } in
            let insert = { proc = CInsert(tbl,args,out_c); comment = None } in
            { proc = CInput(Name c,PatVar v, insert); comment = Some "Lock added around each operator on the table." }
      end
  | CInsert(tbl,args,p) ->
      let p' = transform_tables_eprocess_c tbl_rq p in
      { proc_c with proc = CInsert(tbl,args,p')}
  | CGet(tbl,pat,e_op,p1,p2) when tbl.status_t <> STNone ->
      begin match retrieve_name_of_add_cell tbl tbl_rq with
        | None ->
            let p1' = transform_tables_eprocess_c (require_increased_nat tbl true tbl_rq) p1 in
            let p2' = transform_tables_eprocess_c (require_increased_nat tbl true tbl_rq) p2 in
            { proc_c with proc = CGet(tbl,pat,e_op,p1',p2') }
        | Some(c,b) ->
            let p1' = transform_tables_eprocess_c tbl_rq p1 in
            let p2' = transform_tables_eprocess_c tbl_rq p2 in
            let id = fresh_id_from_ident (nat_str,dummy_ext) in
            let v = { id_v = id ; typ_v = nat_id; status_v = SCNone; options_v = [] } in

            let out_c p =
              if b
              then { proc = COutput(Name c,Sum(v,1),p); comment = None }
              else { proc = COutput(Name c,Var v,p); comment = None }
            in
            let get = { proc = CGet(tbl,pat,e_op,out_c p1',out_c p2'); comment = None } in
            { proc = CInput(Name c,PatVar v, get); comment = Some "Lock added around each operator on the table." }
      end
  | CGet(tbl,pat,e_op,p1,p2) ->
      let p1' = transform_tables_eprocess_c tbl_rq p1 in
      let p2' = transform_tables_eprocess_c tbl_rq p2 in
      { proc_c with proc = CGet(tbl,pat,e_op,p1',p2') }

let update_status_channel_for_tables tbl_rq =
  let rec explore = function
    | [] -> NameMap.empty
    | TAddNat(tbl,c,b,_,_)::q ->
        let map = explore q in
        let l =
          if NameMap.mem c map
          then NameMap.find c map
          else []
        in

        if b
        then NameMap.add c ((SNat([TableRound(tbl,true)],None))::l) map
        else NameMap.add c ((SNat([TableInsert(tbl,true)],None))::l) map
    | _::q -> explore q
  in
  NameMap.iter (fun c l ->
    match c.status_n with
      | SCCell (Some (STuple s_l)) -> c.status_n <- SCCell (Some (STuple (l@s_l)))
      | SCCell (Some st) -> c.status_n <- SCCell (Some (STuple (l@[st])))
      | _ -> internal_error "[transform.ml >> update_status_channel_for_tables] Unexpected case."
  ) (explore tbl_rq)

let rec add_initialisation_table p = function
  | [] -> p
  | TAddCell(tbl,n,_)::q ->
      let nil_p = { proc = CNil; comment = None } in
      let v = fresh_var nat_id in
      let out_init = { proc = COutput(Name n,Nat 0,nil_p); comment = Some (Printf.sprintf "Initialisation of the added cell to lock the table %s." tbl.id_t.id) } in
      let out_gen = { proc = COutput(Name n,Var v,nil_p); comment = None } in
      let in_gen = { proc = CInput(Name n, PatVar v, out_gen); comment = None } in
      let repl_gen = { proc = CRepl in_gen; comment = Some (Printf.sprintf "Access of the added cell to lock the table %s." tbl.id_t.id) } in
      let init_p = { proc = CPar(out_init,repl_gen); comment = None } in
      add_initialisation_table ({ proc = CPar(p,init_p); comment = None }) q
  | _::q -> add_initialisation_table p q

let rec transform_tables_decl trans_request = function
  | [] -> internal_error "[transform.ml >> transform_tables_decl] Should not occur."
  | (DTable tbl)::q ->
      begin match tbl.status_t, tbl.options_t with
        | STUnknown,_ (* No action on the table *)
        | _, TblNone
        | _, TblValue
        | STNone, _ -> (DTable tbl)::(transform_tables_decl trans_request q)
        | STUnlocked,_ ->
            let new_c_id = fresh_id_from_ident (priv_channel_str,dummy_ext) in
            let is_round = tbl.options_t = TblLockedPerRound in
            let new_stat_nat =
              if is_round
              then TableRound(tbl,true)
              else TableInsert(tbl,true)
            in

            let new_c = { id_n = new_c_id; typ_n = channel_id; options_n = [NDPrivate;NDPrecise]; status_n = SCCell (Some (SNat ([new_stat_nat],None))); ext_n = dummy_ext } in
            tbl.status_t <- STLocked [new_c];
            (DFree new_c)::(DTable tbl)::(transform_tables_decl ((TAddCell(tbl,new_c,is_round))::trans_request) q)
        | STLocked name_list,_ ->
            let new_trans_request =
              List.fold_left (fun acc_rq c ->
                if is_cell_locking_table tbl c tbl.options_t
                then acc_rq
                else
                  let is_round = tbl.options_t = TblLockedPerRound in
                  (TAddNat(tbl,c,is_round,None,false))::acc_rq
              ) trans_request name_list
            in
            (DTable tbl)::(transform_tables_decl new_trans_request q)
      end
  | [DProcess p] ->
      let p' = transform_tables_eprocess_c trans_request p in
      update_status_channel_for_tables trans_request;
      [DProcess (add_initialisation_table p' trans_request)]
  | decl::q -> decl::(transform_tables_decl trans_request q)

(****************************************
***          Transform cells          ***
*****************************************)

let merge_linked_modif_pattern () =
  let rec explore s_pat_1 s_pat_2 = match s_pat_1, s_pat_2 with
    | STuple(s_args), STuple(s_args') -> STuple(List.map2 explore s_args s_args')
    | SData(f,s_args), SData(f',s_args') when f == f' -> SData(f,List.map2 explore s_args s_args')
    | SNat(_,None), SNat(_,mpat)
    | SNat(_,mpat), SNat(_,None) -> SNat([],mpat)
    | SNat(_,Some mpat1), SNat(_,Some mpat2) -> SNat([],Some (merge_modif_pattern mpat1 mpat2))
    | SOpen typ, SOpen typ' when typ == typ' -> SOpen typ
    | SBlackBox typ, SBlackBox typ' when typ == typ' -> SBlackBox typ
    | _, _ -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case."
  in

  let explore_status st1 st2 = match st1, st2 with
    | SCNone, _
    | _, SCNone -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case 5."
    | SCPublic, _
    | _, SCPublic -> SCPublic
    | SCPrivate, _
    | _, SCPrivate -> SCPrivate
    | SCCell None, _ -> st2
    | _, SCCell None -> st1
    | SCCell (Some s1), SCCell (Some s2) -> SCCell (Some (explore s1 s2))
  in

  let rec replace_mpat s_pat_rcv spat = match s_pat_rcv, spat with
    | STuple(s_args), STuple(s_args') -> STuple(List.map2 replace_mpat s_args s_args')
    | SData(f,s_args), SData(f',s_args') when f == f' -> SData(f,List.map2 replace_mpat s_args s_args')
    | SNat(tbl,None), SNat(_,mpat) -> SNat(tbl,mpat)
    | SNat(_,_), SNat(_,None) -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case 2."
    | SNat(tbl,Some _), SNat(_,Some mpat2) -> SNat(tbl,Some mpat2)
    | SOpen typ, SOpen typ' when typ == typ' -> s_pat_rcv
    | SBlackBox typ, SBlackBox typ' when typ == typ' -> s_pat_rcv
    | _, _ -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case 3."
  in

  let replace_status st1 st2 = match st1, st2 with
    | SCNone, _
    | _, SCNone -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case 6."
    | SCPublic, _
    | _, SCPublic -> SCPublic
    | SCPrivate, _
    | _, SCPrivate -> SCPrivate
    | SCCell None, _ -> st2
    | _, SCCell None -> internal_error "[transform.ml >> merge_linked_modif_pattern] Unexpected case 4."
    | SCCell (Some s1), SCCell (Some s2) -> SCCell (Some (replace_mpat s1 s2))
  in

  TableMap.iter (fun _ links ->
    List.iter (fun link_ref -> match !link_ref with
      | TAImpossible
      | TAUnknown -> ()
      | TALink(c_set,v_set) ->
          let c = NameSet.choose c_set in
          let st1 =
            NameSet.fold (fun c' acc ->
              explore_status acc c'.status_n
            ) c_set c.status_n
          in
          let st2 =
            VarSet.fold (fun v' acc ->
              explore_status acc v'.status_v
            ) v_set st1
          in
          NameSet.iter (fun c' ->
            c'.status_n <- replace_status c'.status_n st2
          ) c_set;
          VarSet.iter (fun v' ->
            v'.status_v <- replace_status v'.status_v st2
          ) v_set;
    ) links
  ) !linked_table_channel

let rec analyse_cells_eprocess_c cell_channel cell_var cproc = match cproc.proc with
  | CNil -> NameMap.empty, VarMap.empty
  | CPar(p1,p2) ->
      let c_res1,v_res1 = analyse_cells_eprocess_c cell_channel cell_var p1 in
      let c_res2,v_res2 = analyse_cells_eprocess_c cell_channel cell_var p2 in
      union_cellMap c_res1 c_res2, union_varMap v_res1 v_res2
  | CRestr (_,_,p) -> analyse_cells_eprocess_c cell_channel cell_var p
  | CInput(ch,pat,p) when is_name ch ->
      let c = name_of_term ch in

      if is_cell c
      then
        let status = get_status c in
        let mpat = modif_pattern_of_status_pattern status in
        update_modif_pattern c mpat;
        let tpat = term_of_pattern pat in
        let new_cell_channel = NameMap.add c tpat cell_channel in
        let c_res,v_res = analyse_cells_eprocess_c new_cell_channel cell_var p in
        NameMap.remove c c_res, v_res
      else analyse_cells_eprocess_c cell_channel cell_var p
  | CInput(ch,pat,p) when is_var ch ->
      let v = var_of_term ch in

      if is_var_cell v
      then
        let status = get_var_status v in
        let mpat = modif_pattern_of_status_pattern status in
        update_modif_pattern_var v mpat;
        let tpat = term_of_pattern pat in
        let new_cell_var = VarMap.add v tpat cell_var in
        let c_res,v_res = analyse_cells_eprocess_c cell_channel new_cell_var p in
        c_res, VarMap.remove v v_res
      else analyse_cells_eprocess_c cell_channel cell_var p
  | CInput(_,_,p) -> analyse_cells_eprocess_c cell_channel cell_var p
  | COutput(ch,t_out,p) when is_name ch ->
      let c = name_of_term ch in

      if is_cell c
      then
        begin
          try
            let t_in = NameMap.find c cell_channel in
            let modif_pat = modif_pattern_of_round c t_in t_out in
            update_modif_pattern c modif_pat;
            let cell_channel' = NameMap.remove c cell_channel in
            let c_res,v_res = analyse_cells_eprocess_c cell_channel' cell_var p in
            union_cellMap c_res (NameMap.singleton c [t_out]), v_res
          with Not_found -> analyse_cells_eprocess_c cell_channel cell_var p
        end
      else analyse_cells_eprocess_c cell_channel cell_var p
  | COutput(ch,t_out,p) when is_var ch ->
      let v = var_of_term ch in

      if is_var_cell v
      then
        begin
          try
            let t_in = VarMap.find v cell_var in
            let modif_pat = modif_pattern_of_round_var v t_in t_out in
            update_modif_pattern_var v modif_pat;
            let cell_var' = VarMap.remove v cell_var in
            let c_res,v_res = analyse_cells_eprocess_c cell_channel cell_var' p in
            c_res, union_varMap v_res (VarMap.singleton v [t_out])
          with Not_found -> internal_error "[transform.ml >> analyse_cells_eprocess_c] An output on a variable cell should always be preceded by an input."
        end
      else analyse_cells_eprocess_c cell_channel cell_var p
  | COutput(_,_,p)
  | CEvent(_,_,_,p)
  | CRepl p
  | CInsert(_,_,p) -> analyse_cells_eprocess_c cell_channel cell_var p
  | CTest(_,p_then,p_else)
  | CLet(_,_,p_then,p_else)
  | CLetFilter(_,_,p_then,p_else)
  | CGet(_,_,_,p_then,p_else) ->
      let c_res1,v_res1 = analyse_cells_eprocess_c cell_channel cell_var p_then in
      let c_res2,v_res2 = analyse_cells_eprocess_c cell_channel cell_var p_else in
      union_cellMap c_res1 c_res2, union_varMap v_res1 v_res2

let rec analyse_cells_decl = function
  | [] -> ()
  | [DProcess p] ->
      let _ = analyse_cells_eprocess_c NameMap.empty VarMap.empty p in
      merge_linked_modif_pattern ()
  | (DProcess _)::_ -> internal_error "[transform.ml >> analyse_cells_decl] There should be a unique DProcess at the end of the list."
  | _::q -> analyse_cells_decl q

(* Transform *)

let rec generate_formula last_out input status = match last_out,input,status with
  | _,_,SNat _ -> Some (Fun(infeq_symb,[last_out;input]))
  | _,_,SOpen _
  | _,_,SBlackBox _ -> None
  | Tuple(args_out), Tuple(args_in), STuple(args_st)
  | Fun(_,args_out), Fun(_,args_in), SData(_,args_st) -> generate_formula_list args_out args_in args_st
  | _,_,_ -> internal_error "[transform.ml >> generate_formula] Unexpected case."

and generate_formula_list last_out_l input_l status_l = match last_out_l,input_l,status_l with
  | [],[],[] -> None
  | [],_,_ | _,[],_ | _,_,[] -> internal_error "[transform.ml >> generate_formula_list] Unexpected case."
  | t_out::q_out, t_in::q_in, t_st::q_st ->
      let t_op = generate_formula t_out t_in t_st
      and t_op' = generate_formula_list q_out q_in q_st in

      match t_op, t_op' with
        | None, None -> None
        | Some t, None
        | None, Some t -> Some t
        | Some t1, Some t2 -> Some (Fun(and_symb, [t1;t2]))

let rec is_all_modif_pattern = function
  | MModif -> true
  | MBlackBox -> false
  | MFun(args) -> List.for_all is_all_modif_pattern args

let rec exists_covering_nat = function
  | SOpen _
  | SBlackBox _ -> false
  | SNat(_,None) -> internal_error "[transform.ml >> exists_covering_nat_status_pattern] Unexpected case."
  | SNat(_,Some mpat) -> is_all_modif_pattern mpat
  | STuple args
  | SData(_,args) -> List.exists exists_covering_nat args

let add_var_in_pattern v pat status = match pat,status with
  | PatTuple args_pat, STuple _ -> PatTuple((PatVar v)::args_pat)
  | _ , STuple _ -> internal_error "[transform.ml >> add_var_in_pattern] Unexpected case."
  | _,_ -> PatTuple([PatVar v; pat])

let add_nat_in_term i term status = match term,status with
  | Tuple args, STuple _ -> Tuple(i::args)
  | _ , STuple _ -> internal_error "[transform.ml >> add_var_in_pattern] Unexpected case."
  | _,_ -> Tuple([i; term])

type cells_transformation_request =
  {
    vars_to_add : NameSet.t;
    last_input : eterm NameMap.t;
    last_var_input : var NameMap.t;
    last_output : eterm NameMap.t;
    last_var_output : eterm NameMap.t;
  }

type cells_var_transformation_request =
  {
    vars_to_add_v : VarSet.t;
    last_input_v : eterm VarMap.t;
    last_var_input_v : var VarMap.t;
    last_output_v : eterm VarMap.t;
    last_var_output_v : eterm VarMap.t;
  }

let rec transform_cells_eprocess_c ch_rq var_rq proc_c = match proc_c.proc with
  | CNil -> { proc_c with proc = CNil }
  | CPar(p1,p2) ->
      let p1' = transform_cells_eprocess_c ch_rq var_rq p1 in
      let p2' = transform_cells_eprocess_c ch_rq var_rq p2 in
      { proc_c with proc = CPar(p1',p2') }
  | CRepl p ->
      let p' = transform_cells_eprocess_c ch_rq var_rq p in
      { proc_c with proc = CRepl(p') }
  | CRestr(n,vars,p) ->
      let ch_rq' =
        if is_cell n && exists_covering_nat (get_status n)
        then ch_rq
        else { ch_rq with vars_to_add = NameSet.add n ch_rq.vars_to_add }
      in
      let p' = transform_cells_eprocess_c ch_rq' var_rq p in

      { proc_c with proc = CRestr(n,vars,p') }
  | CTest(t,p1,p2) ->
      let p1' = transform_cells_eprocess_c ch_rq var_rq p1 in
      let p2' = transform_cells_eprocess_c ch_rq var_rq p2 in
      { proc_c with proc = CTest(t,p1',p2') }
  | CInput(c,pat,p) ->
      if is_name c && is_cell (name_of_term c)
      then
        let c_name = name_of_term c in
        let status = get_status c_name in

        let pat', ch_rq', form, comment =
          if NameSet.mem c_name ch_rq.vars_to_add
          then
            let v = fresh_var_nat () in
            let pat' = add_var_in_pattern v pat status in
            let ch_rq' =
              { ch_rq with
                last_input = NameMap.add c_name (term_of_pattern pat) ch_rq.last_input;
                last_var_input = NameMap.add c_name v ch_rq.last_var_input
              }
            in
            let form =
              if NameMap.mem c_name ch_rq.last_output
              then
                let last_out = NameMap.find c_name ch_rq.last_output in
                let last_v_out = NameMap.find c_name ch_rq.last_var_output in
                generate_formula_list [last_v_out;last_out] [Var v; term_of_pattern pat] [SNat([],None); status]
              else None
            in
            pat',ch_rq',form, Some (Printf.sprintf "The first nat variable has been added due to the %s parameter" cell_str)
          else
            let tpat = term_of_pattern pat in
            let ch_rq' = { ch_rq with last_input = NameMap.add c_name tpat ch_rq.last_input } in
            let form =
              if NameMap.mem c_name ch_rq.last_output
              then
                let last_out = NameMap.find c_name ch_rq.last_output in
                generate_formula last_out tpat status
              else None
            in
            pat,ch_rq',form, None
        in

        let p' = transform_cells_eprocess_c ch_rq' var_rq p in

        match form with
          | None -> { proc = CInput(c,pat',p'); comment = comment }
          | Some tform ->
              let p_nil = {proc = CNil; comment = None} in
              let p'' = { proc = CTest(tform,p',p_nil); comment = Some "Added test due to sequential round of a cell." } in
              { proc = CInput(c,pat',p''); comment = comment}
      else if is_var c && is_var_cell (var_of_term c)
      then
        let c_var = var_of_term c in
        let status = get_var_status c_var in

        let pat', var_rq', form, comment =
          if VarSet.mem c_var var_rq.vars_to_add_v
          then
            let v = fresh_var_nat () in
            let pat' = add_var_in_pattern v pat status in
            let var_rq' =
              { var_rq with
                last_input_v = VarMap.add c_var (term_of_pattern pat) var_rq.last_input_v;
                last_var_input_v = VarMap.add c_var v var_rq.last_var_input_v
              }
            in
            let form =
              if VarMap.mem c_var var_rq.last_output_v
              then
                let last_out = VarMap.find c_var var_rq.last_output_v in
                let last_v_out = VarMap.find c_var var_rq.last_var_output_v in
                generate_formula_list [last_v_out;last_out] [Var v; term_of_pattern pat] [SNat([],None); status]
              else None
            in
            pat',var_rq',form, Some (Printf.sprintf "The first nat variable has been added due to the %s parameter" cell_str)
          else
            let tpat = term_of_pattern pat in
            let var_rq' = { var_rq with last_input_v = VarMap.add c_var tpat var_rq.last_input_v } in
            let form =
              if VarMap.mem c_var var_rq.last_output_v
              then
                let last_out = VarMap.find c_var var_rq.last_output_v in
                generate_formula last_out tpat status
              else None
            in
            pat,var_rq',form, None
        in

        let p' = transform_cells_eprocess_c ch_rq var_rq' p in

        match form with
          | None -> { proc = CInput(c,pat',p'); comment = comment }
          | Some tform ->
              let p_nil = {proc = CNil; comment = None} in
              let p'' = { proc = CTest(tform,p',p_nil); comment = Some "Added test due to sequential round of a cell." } in
              { proc = CInput(c,pat',p''); comment = comment}
      else
        let p' = transform_cells_eprocess_c ch_rq var_rq p in
        { proc_c with proc = CInput(c,pat,p') }
  | COutput(c,t,p) ->
      if is_name c && is_cell (name_of_term c)
      then
        let c_name = name_of_term c in
        let status = get_status c_name in

        let t', ch_rq' =
          if NameSet.mem c_name ch_rq.vars_to_add
          then
            if NameMap.mem c_name ch_rq.last_input
            then
              let last_in = NameMap.find c_name ch_rq.last_input in
              let last_var_in = NameMap.find c_name ch_rq.last_var_input in
              let last_var_out =
                if is_equal_term t last_in
                then Var last_var_in
                else Sum (last_var_in,1)
              in
              let t' = add_nat_in_term last_var_out t status in
              let ch_rq' =
                { ch_rq with
                  last_output = NameMap.add c_name t ch_rq.last_output;
                  last_var_output = NameMap.add c_name last_var_out ch_rq.last_var_output
                }
              in
              t', ch_rq'
            else
              let t' = add_nat_in_term (Nat 0) t status in
              let ch_rq' =
                { ch_rq with
                  last_output = NameMap.add c_name t ch_rq.last_output;
                  last_var_output = NameMap.add c_name (Nat 0) ch_rq.last_var_output
                }
              in
              t',ch_rq'
          else
            t, { ch_rq with last_output = NameMap.add c_name t ch_rq.last_output }
        in

        let p' = transform_cells_eprocess_c ch_rq' var_rq p in
        { proc_c with proc = COutput(c,t',p') }
      else if is_var c && is_var_cell (var_of_term c)
      then
        let c_var = var_of_term c in
        let status = get_var_status c_var in

        let t', var_rq' =
          if VarSet.mem c_var var_rq.vars_to_add_v
          then
            if VarMap.mem c_var var_rq.last_input_v
            then
              let last_in = VarMap.find c_var var_rq.last_input_v in
              let last_var_in = VarMap.find c_var var_rq.last_var_input_v in
              let last_var_out =
                if is_equal_term t last_in
                then Var last_var_in
                else Sum (last_var_in,1)
              in
              let t' = add_nat_in_term last_var_out t status in
              let var_rq' =
                { var_rq with
                  last_output_v = VarMap.add c_var t var_rq.last_output_v;
                  last_var_output_v = VarMap.add c_var last_var_out var_rq.last_var_output_v
                }
              in
              t', var_rq'
            else
              let t' = add_nat_in_term (Nat 0) t status in
              let var_rq' =
                { var_rq with
                  last_output_v = VarMap.add c_var t var_rq.last_output_v;
                  last_var_output_v = VarMap.add c_var (Nat 0) var_rq.last_var_output_v
                }
              in
              t',var_rq'
          else
            t, { var_rq with last_output_v = VarMap.add c_var t var_rq.last_output_v }
        in

        let p' = transform_cells_eprocess_c ch_rq var_rq' p in
        { proc_c with proc = COutput(c,t',p') }
      else
        let p' = transform_cells_eprocess_c ch_rq var_rq p in
        { proc_c with proc = COutput(c,t,p') }
  | CLet(pat,t,p1,p2) ->
      let p1' = transform_cells_eprocess_c ch_rq var_rq p1 in
      let p2' = transform_cells_eprocess_c ch_rq var_rq p2 in
      { proc_c with proc = CLet(pat,t,p1',p2') }
  | CLetFilter(vars,t,p1,p2) ->
      let p1' = transform_cells_eprocess_c ch_rq var_rq p1 in
      let p2' = transform_cells_eprocess_c ch_rq var_rq p2 in
      { proc_c with proc = CLetFilter(vars,t,p1',p2') }
  | CEvent(id,args,vars,p) ->
      let p' = transform_cells_eprocess_c ch_rq var_rq p in
      { proc_c with proc = CEvent(id,args,vars,p') }
  | CInsert(tbl,args,p) ->
      let p' = transform_cells_eprocess_c ch_rq var_rq p in
      { proc_c with proc = CInsert(tbl,args,p') }
  | CGet(tbl,args,op,p1,p2) ->
      let var_rq' =
        List.fold_left (fun acc_var_rq pat -> match pat with
          | PatVar v when is_var_cell v && exists_covering_nat (get_var_status v) -> acc_var_rq
          | PatVar v -> { acc_var_rq with vars_to_add_v = VarSet.add v acc_var_rq.vars_to_add_v }
          | _ -> acc_var_rq
        ) var_rq args
      in
      let p1' = transform_cells_eprocess_c ch_rq var_rq' p1 in
      let p2' = transform_cells_eprocess_c ch_rq var_rq p2 in
      { proc_c with proc = CGet(tbl,args,op,p1',p2') }

let rec transform_cells_decl ch_rq = function
  | [] -> internal_error "[transform.ml >> transform_cells_decl] Unexpected case."
  | (DFree n)::q ->
      let ch_rq' =
        if is_cell n && (is_unused_cell n || exists_covering_nat (get_status n))
        then ch_rq
        else { ch_rq with vars_to_add = NameSet.add n ch_rq.vars_to_add }
      in
      (DFree n)::(transform_cells_decl ch_rq' q)
  | [DProcess p] ->
      let var_rq =
        {
          vars_to_add_v = VarSet.empty;
          last_input_v = VarMap.empty;
          last_var_input_v = VarMap.empty;
          last_output_v = VarMap.empty;
          last_var_output_v = VarMap.empty
        }
      in
      [DProcess (transform_cells_eprocess_c ch_rq var_rq p)]
  | decl::q -> decl::(transform_cells_decl ch_rq q)

(*******************************
***          Reset           ***
********************************)

let rec reset_eprocess_c proc_c = match proc_c.proc with
  | CNil -> ()
  | CPar(p1,p2)
  | CTest(_,p1,p2)
  | CLet(_,_,p1,p2)
  | CLetFilter(_,_,p1,p2) -> reset_eprocess_c p1; reset_eprocess_c p2
  | CGet(_,pats,_,p1,p2) ->
      List.iter (function
        | PatVar v ->
            begin match v.status_v with
              | SCCell _ -> v.status_v <- SCCell None
              | _ -> ()
            end
        | _ -> ()
      ) pats;
      reset_eprocess_c p1;
      reset_eprocess_c p2
  | CRepl p
  | CInput(_,_,p)
  | COutput(_,_,p)
  | CEvent(_,_,_,p)
  | CInsert(_,_,p) -> reset_eprocess_c p
  | CRestr(n,_,p) ->
      begin match n.status_n with
        | SCCell _ -> n.status_n <- SCCell None
        | _ -> ()
      end;
      reset_eprocess_c p

let rec reset_decl = function
  | [] -> internal_error "[transform.ml >> reset_decl] Unexpected case."
  | (DFree n)::q ->
      begin match n.status_n with
        | SCCell _ -> n.status_n <- SCCell None
        | _ -> ()
      end;
      reset_decl q
  | (DTable tbl)::q -> tbl.status_t <- STUnknown; reset_decl q
  | [DProcess p] -> reset_eprocess_c p
  | _::q -> reset_decl q

(*******************************
***      Add the events      ***
********************************)

(* Generate the events *)

type name_var =
  | VNVar of var
  | VNName of name

module CompNameVar =
  struct
    type t = name_var
    let compare n1 n2 = match n1, n2 with
      | VNVar _, VNName _ -> -1
      | VNName _, VNVar _ -> 1
      | VNVar v1, VNVar v2 -> Pervasives.compare v1.id_v v2.id_v
      | VNName n1, VNName n2 -> Pervasives.compare n1.id_n n2.id_n
  end

module NameVarMap = Map.Make(CompNameVar)

let term_of_name_var = function
  | VNVar v -> Var v
  | VNName n -> Name n

let union_cellNameVarMap = NameVarMap.union (fun _ l1 l2 -> Some (l1 @ l2))

type table_pattern =
  | TPatVar of id
  | TPatEq of id
  | TPatTuple of table_pattern list
  | TPatData of symbol * table_pattern list

let rec table_pattern_of_pattern = function
  | PatVar v -> TPatVar v.typ_v
  | PatEq t -> TPatEq (get_type t)
  | PatTuple args -> TPatTuple (List.map table_pattern_of_pattern args)
  | PatData(f,args) -> TPatData(f,List.map table_pattern_of_pattern args)

let event_uaction_record = Hashtbl.create 5
let event_vcell_record = Hashtbl.create 5
let event_in_table_record = Hashtbl.create 5
let event_not_in_table_record = Hashtbl.create 5
let event_ucomm_record = ref None
let event_counter_record = ref None
let event_blocking_record = ref None
let event_double_record = Hashtbl.create 5
let event_special_double_record = ref None

let identifier_record = Hashtbl.create 5
let table_get_identifier_record = Hashtbl.create 5

let get_in_hashtbl elt f htbl =
  if Hashtbl.mem htbl elt
  then Hashtbl.find htbl elt
  else
    let x = f () in
    Hashtbl.add htbl elt x;
    x

let get_uaction typ =
  get_in_hashtbl typ (fun () ->
    {
      id_e = fresh_id_from_string (Printf.sprintf "UAction_%s" typ.id);
      arg_ty_e = [stamp_id; typ]
    }
  ) event_uaction_record

let rec strip_status = function
  | SOpen typ -> SOpen typ
  | SBlackBox typ -> SBlackBox typ
  | SNat(_,modif) -> SNat([],modif)
  | STuple(args) -> STuple(List.map strip_status args)
  | SData(f,args) -> SData(f,List.map strip_status args)

let get_vcell c typ =

  let st = match c with
    | VNVar v -> strip_status (get_var_status v)
    | VNName n -> strip_status (get_status n)
  in
  get_in_hashtbl (st,typ) (fun () ->
    {
      id_e = fresh_id_from_string (Printf.sprintf "VCell_%s" typ.id);
      arg_ty_e = [channel_id; typ]
    }
  ) event_vcell_record

let get_type_nat_locking_table tbl =

  let rec explore_status_pat acc = function
    | SNat (s_nat_l,_) ->
        if List.exists (function
            | TableInsert(tbl',true)
            | TableRound(tbl',true) -> tbl' == tbl
            | _ -> false
          ) s_nat_l
        then nat_id::acc
        else acc
    | SOpen _ | SBlackBox _ -> acc
    | STuple(args)
    | SData(_,args) -> List.fold_left explore_status_pat acc args
  in

  match tbl.status_t with
    | STLocked n_l ->
        List.fold_left (fun acc n -> match n.status_n with
          | SCCell (Some st) -> explore_status_pat acc st
          | _ -> internal_error "[transform.ml >> get_type_nat_locking_table] Unexpected case."
        ) [] n_l
    | _ -> internal_error "[transform.ml >> get_type_nat_locking_table] Unexpected case 2."

let get_in_table tbl =
  let typ = match tbl.arg_ty_t with
    | [] -> internal_error "[transform.ml >> get_in_table] Unexpected case."
    | [t] -> t
    | _ -> bitstring_id
  in

  let nat_typs = get_type_nat_locking_table tbl in

  get_in_hashtbl tbl.id_t (fun () ->
    {
      id_e = fresh_id_from_string (Printf.sprintf "In_%s" tbl.id_t.id);
      arg_ty_e = nat_typs @ [typ]
    }
  ) event_in_table_record

let get_not_in_table tbl pat_args =
  let tbl_pat = match pat_args with
    | [pat] -> table_pattern_of_pattern pat
    | _ -> table_pattern_of_pattern (PatTuple pat_args)
  in

  let nat_types = get_type_nat_locking_table tbl in

  let rec get_types_args = function
    | TPatVar _ -> []
    | TPatEq ty -> [ty]
    | TPatTuple args
    | TPatData(_,args) -> List.fold_right (fun t acc -> (get_types_args t)@acc) args []
  in

  let args_types = get_types_args tbl_pat in

  get_in_hashtbl (tbl.id_t,tbl_pat) (fun () ->
    {
      id_e = fresh_id_from_string (Printf.sprintf "Not_in_%s" tbl.id_t.id);
      arg_ty_e = nat_types @ args_types
    }
  ) event_not_in_table_record

let get_double typ =
  get_in_hashtbl typ (fun () ->
    {
      id_e = fresh_id_from_string (Printf.sprintf "DoubleInsert_%s" typ.id);
      arg_ty_e = [stamp_id; stamp_id; typ]
    }
  ) event_double_record

let get_special_double () = match !event_special_double_record with
  | None ->
      let e = { id_e = fresh_id_from_string "DoubleInsert"; arg_ty_e = [stamp_id;stamp_id] } in
      event_special_double_record := Some e;
      e
  | Some e -> e

let get_ucomm () = match !event_ucomm_record with
  | None ->
      let e = { id_e = fresh_id_from_string "UComm"; arg_ty_e = [stamp_id;stamp_id] } in
      event_ucomm_record := Some e;
      e
  | Some e -> e

let get_counter () = match !event_counter_record with
  | None ->
      let e = { id_e = fresh_id_from_string "Counter"; arg_ty_e = [channel_id;stamp_id;stamp_id;nat_id] } in
      event_counter_record := Some e;
      e
  | Some e -> e

let get_counter_identifier n =
  get_in_hashtbl n (fun () ->
    { id_n = fresh_id_from_string "id"; typ_n = stamp_id; options_n = [NDPrivate]; ext_n = dummy_ext; status_n = SCNone }
  ) identifier_record

let get_table_get_identifier =
  let acc = ref 0 in
  let f () =
    incr acc;
    get_in_hashtbl !acc (fun () ->
      { id_n = fresh_id_from_string "id_get"; typ_n = stamp_id; options_n = [NDPrivate]; ext_n = dummy_ext; status_n = SCNone }
    ) table_get_identifier_record
  in
  f

let get_blocking () = match !event_blocking_record with
  | None ->
      let e = { id_e = fresh_id_from_string "Blocking"; arg_ty_e = [channel_id;stamp_id] } in
      event_blocking_record := Some e;
      e
  | Some e -> e

let retrieve_from_output c done_nat in_term out_term_list =
  let status = match c with
    | VNVar v -> get_var_status v
    | VNName n -> get_status n
  in
  let done_nat_ref = ref done_nat in

  let rec explore in_t out_t_list n = function
    | SOpen _ | SBlackBox _ -> ([],n)
    | SNat _ ->
        if not (IntSet.mem n !done_nat_ref) && List.for_all (function Sum _ -> true | t when is_equal_term t in_t -> false | _ -> true ) out_t_list
        then
          let v_in = match in_t with
            | Var v -> v
            | _ -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case."
          in
          done_nat_ref := IntSet.add n !done_nat_ref;
          ([n,v_in],n+1)
        else ([],n+1)
    | STuple st_args ->
        let out_t_args_list = List.map (function Tuple args -> args | _ -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case 2.") out_t_list in


        let in_t_args = match in_t with
          | Tuple args -> args
          | _ -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case 3."
        in
        explore_list in_t_args out_t_args_list n st_args
    | SData(f,st_args) ->
        let out_t_args_list = List.map (function Fun(f',args) when f == f' -> args | _ -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case 4.") out_t_list in
        let in_t_args = match in_t with
          | Fun(f',args) when f == f' -> args
          | _ -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case 5."
        in
        explore_list in_t_args out_t_args_list n st_args

  and explore_list in_t_list out_t_list_list n st_list = match in_t_list,st_list with
    | [],[] -> ([],n)
    | [],_ | _,[] -> internal_error "[transform.ml >> retrieve_from_output] Unexpected case 6."
    | in_t::q_in, st::q_st ->
        let out_t, q_out =
          List.fold_right (fun args (acc_t,acc_q) ->
            if args = [] then internal_error "[transform.ml >> retrieve_from_output] Unexpected case 7.";

            ((List.hd args)::acc_t), ((List.tl args)::acc_q)
          ) out_t_list_list ([],[])
        in
        let (res,n') = explore in_t out_t n st in
        let (res',n'') = explore_list q_in q_out n' q_st in
        (res@res',n'')
  in

  let (res,_) = explore in_term out_term_list 1 status in
  (!done_nat_ref,res)

let retrieve_fixed_terms pat_args =
  let rec explore = function
    | PatVar _ -> []
    | PatEq t -> [t]
    | PatTuple args
    | PatData(_,args) -> List.fold_right (fun pat acc -> (explore pat) @ acc) args []
  in

  List.fold_right (fun pat acc -> (explore pat) @ acc) pat_args []

(* Go through process *)

(*let display_id id = Printf.sprintf "(%s,%d)" id.id id.counter

let rec display_modif_pattern = function
  | MModif -> "MModif"
  | MBlackBox -> "MBlackBox"
  | MFun args -> Printf.sprintf "MFun(%s)" (display_list display_modif_pattern "," args)

let rec display_status_pattern = function
  | SOpen typ -> Printf.sprintf "SOpen%s" (display_id typ)
  | SBlackBox typ -> Printf.sprintf "SBlackbox%s" (display_id typ)
  | SNat (_,None) -> Printf.sprintf "SNat(None)"
  | SNat (_,Some mpat) -> Printf.sprintf "SNat(%s)" (display_modif_pattern mpat)
  | STuple args -> Printf.sprintf "STuple(%s)" (display_list display_status_pattern "," args)
  | SData(f,args) -> Printf.sprintf "SData(%s,%s)" (display_id f.id_f) (display_list display_status_pattern "," args)*)

type cell_data =
  {
    in_term : eterm;
    vcell_added : bool;
    id_counter_added : IntSet.t;
    stamp_added : name option;
    apply_vcell : bool;
    apply_counter : bool
  }

let rec extract_scope_epattern scope = function
  | PatVar v -> IdSet.add v.id_v scope
  | PatEq _ -> scope
  | PatTuple args
  | PatData(_,args) -> List.fold_left extract_scope_epattern scope args

let rec is_in_scope_eterm scope = function
  | Var v
  | Sum(v,_) -> IdSet.mem v.id_v scope
  | Nat _ -> true
  | Name n -> IdSet.mem n.id_n scope
  | Fun(f,args) -> IdSet.mem f.id_f scope && List.for_all (is_in_scope_eterm scope) args
  | Tuple args -> List.for_all (is_in_scope_eterm scope) args
  | Restr(n,vars,t) ->
      let scope' = IdSet.add n.id_n scope in
      let b_op = match vars with
        | None -> true
        | Some vars -> List.for_all (fun v -> IdSet.mem v.id_v scope) vars
      in
      b_op && is_in_scope_eterm scope' t
  | Test(t1,t2,t_op) ->
      let b_op = match t_op with
        | None -> true
        | Some t -> is_in_scope_eterm scope t
      in
      is_in_scope_eterm scope t1 && is_in_scope_eterm scope t2 && b_op
  | Let(pat,t1,t2,t_op) ->
      let b_op = match t_op with
        | None -> true
        | Some t -> is_in_scope_eterm scope t
      in
      let scope' = extract_scope_epattern scope pat in
      is_in_scope_epattern scope pat && is_in_scope_eterm scope t1 && is_in_scope_eterm scope' t2 && b_op
  | LetFilter(vars,t1,t2,t_op) ->
      let b_op = match t_op with
        | None -> true
        | Some t -> is_in_scope_eterm scope t
      in
      let scope' = List.fold_left (fun acc v -> IdSet.add v.id_v acc) scope vars in
      is_in_scope_eterm scope' t1 && is_in_scope_eterm scope' t2 && b_op

and is_in_scope_epattern scope = function
  | PatVar _ -> true
  | PatEq t -> is_in_scope_eterm scope t
  | PatTuple args -> List.for_all (is_in_scope_epattern scope) args
  | PatData(f,args) -> IdSet.mem f.id_f scope && List.for_all (is_in_scope_epattern scope) args

let apply_vcell_counter scope cell_map out_terms next_p =
  NameVarMap.fold (fun c c_data (acc_p,acc_cell_map) ->
    if NameVarMap.mem c out_terms
    then
      let out_t_list = NameVarMap.find c out_terms in
      let p_vcell,vcell_added' =
        if c_data.apply_vcell
        then
          if c_data.vcell_added
          then acc_p,true
          else
            match out_t_list with
              | [t] ->
                  if is_equal_term t c_data.in_term || get_type t = nat_id
                  then acc_p, true
                  else if is_in_scope_eterm scope t
                  then
                    let ev = get_vcell c (get_type t) in
                    (fun c_map -> { proc = CEvent(ev,[term_of_name_var c; t], None, acc_p c_map); comment = None }), true
                  else acc_p, false
              | _ -> acc_p, false
        else acc_p,c_data.vcell_added
      in
      if c_data.apply_counter
      then
        let (id_counter_added',ev_to_add) = retrieve_from_output c c_data.id_counter_added c_data.in_term out_t_list in
        if ev_to_add <> []
        then
          let st, first_st = match c_data.stamp_added with
            | None -> fresh_name_stamp (), true
            | Some n -> n, false
          in

          let p_counter =
            List.fold_left (fun acc_p' (n,v) ->
              let id = get_counter_identifier n in
              let ev = get_counter () in

              (fun c_map -> { proc = CEvent(ev,[term_of_name_var c; Name id; Name st; Var v],None,acc_p' c_map); comment = None })
            ) p_vcell ev_to_add
          in
          let p_st =
            if first_st
            then (fun c_map -> { proc = CRestr(st,Some [], p_counter c_map); comment = None })
            else p_counter
          in
          let acc_cell_map' =
            let c_data' =
              { c_data with
                vcell_added = vcell_added';
                id_counter_added = id_counter_added';
                stamp_added = Some st
              }
            in
            NameVarMap.add c c_data' acc_cell_map
          in
          p_st,acc_cell_map'
        else
          let acc_cell_map' =
            let c_data' = { c_data with vcell_added = vcell_added'; id_counter_added = id_counter_added' } in
            NameVarMap.add c c_data' acc_cell_map
          in
          p_vcell, acc_cell_map'
      else
        let acc_cell_map' =
          let c_data' = { c_data with vcell_added = vcell_added' } in
          NameVarMap.add c c_data' acc_cell_map
        in
        p_vcell, acc_cell_map'
    else
      let p_block, vcell_added' =
        if c_data.apply_vcell
        then
          if c_data.vcell_added
          then acc_p, true
          else
            let ev = get_blocking () in
            let st = fresh_name_stamp () in
            (fun c_map ->
              let p_ev = { proc = CEvent(ev,[term_of_name_var c; Name st], None, acc_p c_map); comment = None } in
              { proc = CRestr(st,Some [],p_ev); comment = None }
            ),true
        else acc_p, c_data.vcell_added
      in
      let acc_cell_map' =
        let c_data' = { c_data with vcell_added = vcell_added' } in
        NameVarMap.add c c_data' acc_cell_map
      in

      p_block, acc_cell_map'
  ) cell_map (next_p,cell_map)

let retrieve_from_locked_table tbl incr c_map =

  let rec explore_status_nat = function
    | [] -> false
    | (TableRound(tbl',true))::_ when tbl == tbl' -> true
    | (TableInsert(tbl',true))::_ when tbl == tbl' -> true
    | _::q -> explore_status_nat q
  in

  let rec explore_status_pat acc status term = match status,term with
    | SNat (s_nat_l,_), Var v when explore_status_nat s_nat_l ->
        if incr
        then (Sum(v,1)) :: acc
        else (Var v)::acc
    | SNat _, _
    | SOpen _, _
    | SBlackBox _, _ -> acc
    | STuple(args), Tuple(args_t)
    | SData(_,args), Fun(_,args_t) -> List.fold_left2 explore_status_pat acc args args_t
    | _, _ -> internal_error "[transform.ml >> retrieve_from_locked_table] Wrong match of term and status."
  in

  match tbl.status_t with
    | STLocked n_l ->
        List.fold_left (fun acc n -> match n.status_n with
          | SCCell (Some st) ->
              let cell_data = NameVarMap.find (VNName n) c_map in
              explore_status_pat acc st cell_data.in_term
          | _ -> internal_error "[transform.ml >> retrieve_from_locked_table] Unexpected case."
        ) [] n_l
    | _ -> internal_error "[transform.ml >> retrieve_from_locked_table] Unexpected case 2."

type get_data =
  {
    id_get : name;
    st_get : name;
    pat_get : epattern
  }

let rec fresh_epattern = function
  | PatVar v -> PatVar (fresh_var v.typ_v)
  | PatEq t -> PatEq t
  | PatTuple args -> PatTuple (List.map fresh_epattern args)
  | PatData(f,args) -> PatData(f,List.map fresh_epattern args)

let generate_double_inserts tbl data_from_get args_inserted next_p =

  let rec apply_datas = function
    | [] -> next_p
    | get_data::q_get ->
        let t_args = match args_inserted with
          | [x] -> x
          | _ -> Tuple(args_inserted)
        in
        let fresh_pat = fresh_epattern get_data.pat_get in

        let ev_double,ev_args = match retrieve_fixed_terms [get_data.pat_get] with
          | [] -> get_special_double (), [Name get_data.id_get; Name get_data.st_get]
          | [x] ->
              let ev = get_double (get_type x) in
              ev, [Name get_data.id_get; Name get_data.st_get; x]
          | args ->
              let ev = get_double bitstring_id in
              ev, [Name get_data.id_get; Name get_data.st_get; Tuple args]
        in

        let c = fresh_name_channel () in

        (* Continuation process *)
        let input_proc = { proc = CInput(Name c,PatEq (Name get_data.st_get),apply_datas q_get) ; comment = None } in

        (* Let in process *)
        let out_proc = { proc = COutput(Name c,Name get_data.st_get, { proc = CNil; comment = None } ) ; comment = None } in
        let then_proc = { proc = CEvent(ev_double,ev_args,None,out_proc); comment = None } in
        let let_proc = { proc = CLet(fresh_pat, t_args, then_proc, out_proc); comment = None } in

        (* Parallel process *)
        let par_proc = { proc = CPar(let_proc,input_proc); comment = None } in
        { proc = CRestr(c,None,par_proc) ; comment = None }
  in

  if TableMap.mem tbl data_from_get
  then apply_datas (TableMap.find tbl data_from_get)
  else next_p

let rec add_events_eprocess_c scope data_from_get proc_c = match proc_c.proc with
  | CNil -> (fun _ -> proc_c), NameVarMap.empty
  | CPar(p1,p2) ->
      let gen_p1,out_map1 = add_events_eprocess_c scope data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map = { proc_c with proc = CPar(gen_p1 c_map,gen_p2 c_map) } in
        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map
  | CRepl p ->
      let gen_p,out_map = add_events_eprocess_c scope data_from_get p in

      let gen_p' cell_map =
        let next_p c_map = { proc_c with proc = CRepl(gen_p c_map) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | CRestr(n,vars,p) ->
      let scope' = IdSet.add n.id_n scope in
      let gen_p,out_map = add_events_eprocess_c scope' data_from_get p in

      let gen_p' cell_map =
        let next_p c_map = { proc_c with proc = CRestr(n,vars,gen_p c_map) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | CTest(t,p1,p2) ->
      let gen_p1,out_map1 = add_events_eprocess_c scope data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map = { proc_c with proc = CTest(t,gen_p1 c_map,gen_p2 c_map) } in
        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map
  | CInput(c,pat,p) when is_name c || is_var c ->
      let scope' = extract_scope_epattern scope pat in
      let gen_p,out_map = add_events_eprocess_c scope' data_from_get p in

      let c_var_name = if is_name c then VNName (name_of_term c) else VNVar (var_of_term c) in
      let options_var_name = if is_name c then (name_of_term c).options_n else (var_of_term c).options_v in
      let status_var_name = if is_name c then (name_of_term c).status_n else (var_of_term c).status_v in
      let is_cell_var_name = if is_name c then is_cell (name_of_term c) else is_var_cell (var_of_term c) in

      let gen_p' cell_map =
        let next_p c_map =
          let apply_vcell = is_cell_var_name && List.exists (fun decl -> decl = NDCell || decl = NDPrecise) options_var_name in
          let apply_counter = is_cell_var_name && List.exists (fun decl -> decl = NDCounter || decl = NDPrecise) options_var_name in
          let apply_uaction = List.mem NDUAction options_var_name || (status_var_name = SCPublic && List.mem NDPrecise options_var_name) in
          let apply_ucomm = List.mem NDUComm options_var_name || (status_var_name = SCPrivate && List.mem NDPrecise options_var_name) in

          let c_map' =
            if apply_counter || apply_vcell
            then
              let cell_data =
                {
                  in_term = term_of_pattern pat;
                  vcell_added = false;
                  id_counter_added = IntSet.empty;
                  stamp_added = None;
                  apply_vcell = apply_vcell;
                  apply_counter = apply_counter
                }
              in
              NameVarMap.add c_var_name cell_data c_map
            else c_map
          in

          if apply_ucomm
          then
            let st_in = fresh_name_stamp () in
            let st_out = fresh_var_stamp () in
            let pat' = PatTuple [PatVar st_out; pat] in

            let ev_uaction =
              if apply_uaction
              then
                let tpat = term_of_pattern pat in
                let typ = get_type tpat in
                { proc = CEvent(get_uaction typ, [Name st_in; tpat], None, gen_p c_map'); comment = None }
              else gen_p c_map'
            in
            let ev_ucomm = { proc = CEvent(get_ucomm (), [Name st_in; Var st_out], None, ev_uaction); comment = None } in
            let p_st = { proc = CRestr(st_in, Some [], ev_ucomm); comment = None } in
            { proc_c with proc = CInput(c,pat',p_st) }
          else if apply_vcell
          then
            let tpat = term_of_pattern pat in
            let typ = get_type tpat in

            let p_uaction =
              if apply_uaction
              then
                let st_in = fresh_name_stamp () in
                let ev_uaction = { proc = CEvent(get_uaction typ, [Name st_in; tpat], None, gen_p c_map'); comment = None } in
                { proc = CRestr(st_in, Some [], ev_uaction); comment = None }
              else gen_p c_map'
            in
            if get_type tpat = nat_id
            then { proc_c with proc = CInput(c,pat,p_uaction) }
            else
              let ev_vcell = { proc = CEvent(get_vcell c_var_name typ, [c; tpat], None, p_uaction); comment = None } in
              { proc_c with proc = CInput(c,pat,ev_vcell) }
          else if apply_uaction
          then
            let st_in = fresh_name_stamp () in
            let tpat = term_of_pattern pat in
            let typ = get_type tpat in
            let ev_uaction = { proc = CEvent(get_uaction typ, [Name st_in; tpat], None, gen_p c_map'); comment = None } in
            let p_st = { proc = CRestr(st_in, Some [], ev_uaction); comment = None } in
            { proc_c with proc = CInput(c,pat,p_st) }
          else { proc_c with proc = CInput(c,pat,gen_p c_map') }
        in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', NameVarMap.remove c_var_name out_map
  | CInput(c,pat,p) ->
      let scope' = extract_scope_epattern scope pat in
      let gen_p,out_map = add_events_eprocess_c scope' data_from_get p in

      let gen_p' cell_map =
        let next_p c_map = { proc_c with proc = CInput(c,pat,gen_p c_map) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | COutput(c,t,p) when is_name c || is_var c ->
      let gen_p,out_map = add_events_eprocess_c scope data_from_get p in

      let c_var_name = if is_name c then VNName (name_of_term c) else VNVar (var_of_term c) in
      let options_var_name = if is_name c then (name_of_term c).options_n else (var_of_term c).options_v in
      let status_var_name = if is_name c then (name_of_term c).status_n else (var_of_term c).status_v in

      let gen_p' cell_map =
        let next_p c_map =
          let apply_ucomm = status_var_name = SCPrivate && List.exists (fun decl -> decl = NDUComm || decl = NDPrecise) options_var_name in

          if apply_ucomm
          then
            let st_out = fresh_name_stamp () in
            let p_out = { proc_c with proc = COutput(c,Tuple [Name st_out; t], gen_p c_map) } in
            { proc = CRestr(st_out,Some [],p_out); comment = None }
          else { proc_c with proc = COutput(c,t,gen_p (NameVarMap.remove c_var_name c_map)) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map (NameVarMap.add c_var_name [t] out_map) next_p in
        gen_p'' cell_map'
      in
      gen_p', (NameVarMap.add c_var_name [t] out_map)
  | COutput(c,t,p) ->
      let gen_p,out_map = add_events_eprocess_c scope data_from_get p in

      let gen_p' cell_map =
        let next_p c_map = { proc_c with proc = COutput(c,t,gen_p c_map) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | CLet(pat,t,p1,p2) ->
      let scope' = extract_scope_epattern scope pat in
      let gen_p1,out_map1 = add_events_eprocess_c scope' data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map = { proc_c with proc = CLet(pat,t,gen_p1 c_map,gen_p2 c_map) } in
        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map
  | CLetFilter(vars,t,p1,p2) ->
      let scope' = List.fold_left (fun acc v -> IdSet.add v.id_v acc) scope vars in
      let gen_p1,out_map1 = add_events_eprocess_c scope' data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map = { proc_c with proc = CLetFilter(vars,t,gen_p1 c_map,gen_p2 c_map) } in
        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map
  | CEvent(ev,args,op,p) ->
      let gen_p,out_map = add_events_eprocess_c scope data_from_get p in

      let gen_p' cell_map =
        let next_p c_map = { proc_c with proc = CEvent(ev,args,op,gen_p c_map) } in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | CInsert(tbl,args,p) ->
      let gen_p,out_map = add_events_eprocess_c scope data_from_get p in

      let gen_p' cell_map =
        let next_p c_map =
          match tbl.options_t with
            | TblNone -> { proc_c with proc = CInsert(tbl,args,gen_p c_map) }
            | TblPrecise
            | TblLockedPerInsert
            | TblLockedPerRound ->
                let t_nats = retrieve_from_locked_table tbl true c_map in
                let ev_in_tbl = get_in_table tbl in
                let new_t = match args with
                  | [t] -> t
                  | _ -> Tuple args
                in
                let p_insert = { proc_c with proc = CInsert(tbl,args,gen_p c_map) } in
                { proc = CEvent(ev_in_tbl,t_nats@[new_t],None,p_insert); comment = None }
            | TblValue ->
                let p' = { proc_c with proc = CInsert(tbl,args,gen_p c_map) } in
                generate_double_inserts tbl data_from_get args p'
        in
        let (gen_p'',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p'' cell_map'
      in
      gen_p', out_map
  | CGet(tbl,pat_args,op,p1,p2) when tbl.options_t = TblValue ->
      let scope' = List.fold_left extract_scope_epattern scope pat_args in

      let new_st = fresh_name_stamp () in
      let new_id = get_table_get_identifier () in
      let new_pat = match pat_args with
        | [x] -> x
        | _ -> PatTuple(pat_args)
      in
      let data_from_get' =
        if TableMap.mem tbl data_from_get
        then
          let data = TableMap.find tbl data_from_get in
          TableMap.add tbl ({id_get = new_id; st_get = new_st; pat_get = new_pat} :: data) data_from_get
        else TableMap.add tbl [{id_get = new_id; st_get = new_st; pat_get = new_pat}] data_from_get
      in

      let gen_p1,out_map1 = add_events_eprocess_c scope' data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get' p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map =
          if tbl.uaction_t
          then
            let t_args = match pat_args with
              | [pat] -> term_of_pattern pat
              | _ -> term_of_pattern (PatTuple pat_args)
            in
            let typ_uaction = get_type t_args in
            let ev_uaction = get_uaction typ_uaction in
            let st = fresh_name_stamp () in
            let p_uaction = { proc = CEvent(ev_uaction,[Name st;t_args], None, gen_p1 c_map); comment = None } in
            let p_st = { proc = CRestr(st, Some [],p_uaction); comment = None } in
            let p_get = { proc_c with proc = CGet(tbl,pat_args,op,p_st,gen_p2 c_map) } in
            { proc = CRestr(new_st, None, p_get); comment = None }
          else
            let p_get = { proc_c with proc = CGet(tbl,pat_args,op,gen_p1 c_map,gen_p2 c_map) } in
            { proc = CRestr(new_st, None, p_get); comment = None }
        in

        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map
  | CGet(tbl,pat_args,op,p1,p2) ->
      let scope' = List.fold_left extract_scope_epattern scope pat_args in
      let gen_p1,out_map1 = add_events_eprocess_c scope' data_from_get p1 in
      let gen_p2,out_map2 = add_events_eprocess_c scope data_from_get p2 in
      let out_map = union_cellNameVarMap out_map1 out_map2 in

      let gen_p cell_map =
        let next_p c_map =
          if tbl.options_t <> TblNone
          then
            let t_nats = retrieve_from_locked_table tbl false c_map in
            let ev_not_in_tbl = get_not_in_table tbl pat_args in
            let ev_in_tbl = get_in_table tbl in
            let t_fixed = retrieve_fixed_terms pat_args in
            let t_args = match pat_args with
              | [pat] -> term_of_pattern pat
              | _ -> term_of_pattern (PatTuple pat_args)
            in

            let p_then =
              if tbl.uaction_t
              then
                let typ_uaction = get_type t_args in
                let ev_uaction = get_uaction typ_uaction in
                let st = fresh_name_stamp () in
                let p_in_tbl = { proc = CEvent(ev_in_tbl,t_nats @ [t_args], None, gen_p1 c_map); comment = None } in
                let p_uaction = { proc = CEvent(ev_uaction,[Name st;t_args], None, p_in_tbl); comment = None } in
                { proc = CRestr(st, Some [],p_uaction); comment = None }
              else { proc = CEvent(ev_in_tbl,t_nats @ [t_args], None, gen_p1 c_map); comment = None }
            in
            let p_else = { proc = CEvent(ev_not_in_tbl,t_nats @ t_fixed, None, gen_p2 c_map); comment = None } in
            { proc_c with proc = CGet(tbl,pat_args,op,p_then,p_else) }
          else
            if tbl.uaction_t
            then
              let t_args = match pat_args with
                | [pat] -> term_of_pattern pat
                | _ -> term_of_pattern (PatTuple pat_args)
              in
              let typ_uaction = get_type t_args in
              let ev_uaction = get_uaction typ_uaction in
              let st = fresh_name_stamp () in
              let p_uaction = { proc = CEvent(ev_uaction,[Name st;t_args], None, gen_p1 c_map); comment = None } in
              let p_st = { proc = CRestr(st, Some [],p_uaction); comment = None } in
              { proc_c with proc = CGet(tbl,pat_args,op,p_st,gen_p2 c_map) }
            else { proc_c with proc = CGet(tbl,pat_args,op,gen_p1 c_map,gen_p2 c_map) }
        in

        let (gen_p',cell_map') = apply_vcell_counter scope cell_map out_map next_p in
        gen_p' cell_map'
      in
      gen_p, out_map

let add_events_query env query_list =
  let memory = Hashtbl.create 5 in

  let fresh_var typ =
    if Hashtbl.mem memory typ
    then
      let (int_to_var,curr) = Hashtbl.find memory typ in
      if !curr < Hashtbl.length int_to_var
      then
        begin
          incr curr;
          Hashtbl.find int_to_var !curr
        end
      else
        begin
          incr curr;
          let v = fresh_var typ in
          Hashtbl.add int_to_var !curr v;
          v
        end
    else
      begin
        let int_to_var = Hashtbl.create 5 in
        let curr = ref 1 in
        let v = fresh_var typ in
        Hashtbl.add int_to_var 1 v;
        Hashtbl.add memory typ (int_to_var,curr);
        v
      end
  in

  let reset () = Hashtbl.iter (fun _ (_,curr) -> curr := 0) memory in

  let generate_new_query query =
    let q_uaction =
      Hashtbl.fold (fun typ ev acc_q ->
        reset ();
        let st = fresh_var stamp_id in
        let x = fresh_var typ in
        let y = fresh_var typ in

        let ev1 = QFact(FEvent(ev,[Var st; Var x], false)) in
        let ev2 = QFact(FEvent(ev,[Var st; Var y], false)) in
        let ev3 = QFact(FPred(neq_symb,[Var x; Var y])) in
        match acc_q with
          | None -> Some (QAnd (ev1,QAnd(ev2,ev3)))
          | Some q -> Some (QOr(q,QAnd (ev1,QAnd(ev2,ev3))))
      ) event_uaction_record query
    in

    let q_vcell =
      let rec generate_term_from_status = function
        | SOpen typ
        | SBlackBox typ -> Var (fresh_var typ)
        | SNat _ -> Var (fresh_var nat_id)
        | STuple args -> Tuple(List.map generate_term_from_status args)
        | SData(f,args) -> Fun(f,List.map generate_term_from_status args)
      in

      let rec generate_term_from_modif mpat term = match mpat,term with
        | MModif, _ -> [term]
        | MBlackBox, _ -> []
        | MFun margs, Tuple args
        | MFun margs, Fun(_,args) -> List.fold_right2 (fun m t acc -> (generate_term_from_modif m t)@acc) margs args []
        | _, _ -> internal_error "[transform.ml >> generate_term_from_modif] Matching issue."
      in

      let rec generate_eq_from_status query full_t1 full_t2 status t1 t2 = match status,t1,t2 with
        | SOpen _, _,_
        | SBlackBox _, _, _ -> query
        | SNat(_,Some mpat),_, _ ->
            let t_l1 = generate_term_from_modif mpat full_t1 in
            let t_l2 = generate_term_from_modif mpat full_t2 in

            if List.length t_l1 = 1
            then query
            else
              let t1' = Tuple t_l1 in
              let t2' = Tuple t_l2 in
              let q1 = QFact (FPred(eq_symb, [t1;t2])) in
              let q2 = QFact (FPred(neq_symb, [t1';t2'])) in
              begin match query with
                | None -> Some (QAnd(q1,q2))
                | Some q -> Some (QOr(QAnd(q1,q2),q))
              end
        | SNat _,_,_ -> internal_error "[transform.ml >> generate_eq_from_status] Unexpected case."
        | STuple args, Tuple args1, Tuple args2
        | SData(_,args), Fun(_,args1), Fun(_,args2) -> generate_eq_from_status_list query full_t1 full_t2 args args1 args2
        | _, _, _ -> internal_error "[transform.ml >> generate_eq_from_status] Matching issue 2."

      and generate_eq_from_status_list query full_t1 full_t2 status_l t1_l t2_l = match status_l, t1_l, t2_l with
        | [],[],[] -> query
        | [],_,_ | _,[],_ | _,_,[] -> internal_error "[transform.ml >> generate_eq_from_status_list] Unexpected case."
        | st::q_st,t1::q1,t2::q2 ->
            let query1 = generate_eq_from_status query full_t1 full_t2 st t1 t2 in
            generate_eq_from_status_list query1 full_t1 full_t2 q_st q1 q2
      in

      Hashtbl.fold (fun (status,_) ev acc_q ->
        reset ();
        let v = fresh_var channel_id in
        let t1 = generate_term_from_status status in
        let t2 = generate_term_from_status status in

        let query_op = generate_eq_from_status None t1 t2 status t1 t2 in
        match query_op with
          | None -> internal_error "[transform.ml >> add_events_query] Unexpected case."
          | Some q ->
              let q1 = QFact (FEvent(ev,[Var v;t1],false)) in
              let q2 = QFact (FEvent(ev,[Var v;t2],false)) in
              begin match acc_q with
                | None -> Some (QAnd(q1,QAnd(q2,q)))
                | Some q' -> Some (QOr(q',(QAnd(q1,QAnd(q2,q)))))
              end
      ) event_vcell_record q_uaction
    in

    let q_table =
      let rec term_of_table_pattern = function
        | TPatVar typ -> Var (fresh_var typ), []
        | TPatEq typ -> let v = fresh_var typ in
            Var v, [Var v]
        | TPatTuple args ->
            let new_args, vars =
              List.fold_left (fun (acc_t,acc_v) pat ->
                let (t,v) = term_of_table_pattern pat in
                (acc_t @ [t], acc_v @ v)
              ) ([],[]) args
            in
            Tuple new_args, vars
        | TPatData(f,args) ->
            let new_args, vars =
              List.fold_left (fun (acc_t,acc_v) pat ->
                let (t,v) = term_of_table_pattern pat in
                (acc_t @ [t], acc_v @ v)
              ) ([],[]) args
            in
            Fun(f,new_args), vars
      in

      let complete_vars typ_args vars =
        let rev_typ = List.rev typ_args in
        let rev_vars = List.rev vars in
        let rec explore t_l v_l = match t_l, v_l with
          | [],[] -> []
          | [],_ -> internal_error "[add_events_query] Unexpected case."
          | typ::q, [] when typ == nat_id -> (Var (fresh_var nat_id))::(explore q [])
          | _::_, [] -> internal_error "[add_events_query] Unexpected case2."
          | _::q, _::q_v -> explore q q_v
        in
        explore rev_typ rev_vars
      in

      let rec vars_in_tbl = function
        | [] -> internal_error "[transform.ml >> generate_new_query] Unexpected case3."
        | [typ] -> [], Var (fresh_var typ)
        | typ::q ->
            let v = fresh_var typ in
            let (vars,last) = vars_in_tbl q in
            ((Var v)::vars,last)
      in

      let rec generate_disjunc vars_i vars_j = match vars_i, vars_j with
        | [],_
        | _,[] -> internal_error "[transform.ml >> generate_disjunc] Unexpected case."
        | [i],[j] -> QFact (FPred(infeq_symb,[i;j]))
        | i::q_i, j::q_j -> QOr (QFact (FPred(infeq_symb,[i;j])), generate_disjunc q_i q_j)
      in

      Hashtbl.fold (fun (tbl_id,tbl_pat) ev_not_in acc_q ->
        reset ();
        let ev_in = Hashtbl.find event_in_table_record tbl_id in

        let t_pat, vars_y = term_of_table_pattern tbl_pat in
        let vars_j = complete_vars ev_not_in.arg_ty_e vars_y in
        let vars_i,x = vars_in_tbl ev_in.arg_ty_e in

        let q_in = QFact (FEvent(ev_in,vars_i@[x],false)) in
        let q_not_in = QFact (FEvent(ev_not_in,vars_j@vars_y,false)) in
        let q_eq = QFact (FPred(eq_symb,[x;t_pat])) in
        match acc_q with
          | None -> Some (QAnd(q_in,QAnd(q_not_in,QAnd(q_eq,generate_disjunc vars_i vars_j))))
          | Some q -> Some (QOr(q,(QAnd(q_in,QAnd(q_not_in,QAnd(q_eq,generate_disjunc vars_i vars_j))))))
      ) event_not_in_table_record q_vcell
    in

    let q_ucomm = match !event_ucomm_record with
      | None -> q_table
      | Some ev ->
          reset ();
          let x = fresh_var stamp_id in
          let y = fresh_var stamp_id in
          let z = fresh_var stamp_id in

          let q_ev1 = QFact (FEvent(ev,[Var x; Var y],false)) in
          let q_ev2 = QFact (FEvent(ev,[Var x; Var z],false)) in
          let q_ev3 = QFact (FEvent(ev,[Var y; Var x],false)) in
          let q_ev4 = QFact (FEvent(ev,[Var z; Var x],false)) in
          let q_neq = QFact (FPred(neq_symb,[Var y; Var z])) in

          begin match q_table with
            | None -> Some (QOr(QAnd(q_ev1,QAnd(q_ev2,q_neq)),QAnd(q_ev3,QAnd(q_ev4,q_neq))))
            | Some q -> Some (QOr(q,QOr(QAnd(q_ev1,QAnd(q_ev2,q_neq)),QAnd(q_ev3,QAnd(q_ev4,q_neq)))))
          end
    in

    let q_counter = match !event_counter_record with
      | None -> q_ucomm
      | Some ev ->
          reset ();
          let c = fresh_var channel_id in
          let id = fresh_var stamp_id in
          let st = fresh_var stamp_id in
          let st' = fresh_var stamp_id in
          let i = fresh_var nat_id in
          let j = fresh_var nat_id in

          let q_ev1 = QFact (FEvent(ev,[Var c; Var id; Var st;Var i],false)) in
          let q_ev2 = QFact (FEvent(ev,[Var c; Var id; Var st;Var j],false)) in
          let q_ev3 = QFact (FEvent(ev,[Var c; Var id; Var st';Var i], false)) in
          let q_neq1 = QFact (FPred(neq_symb,[Var i; Var j])) in
          let q_neq2 = QFact (FPred(neq_symb,[Var st; Var st'])) in

          begin match q_ucomm with
            | None -> Some (QOr(QAnd(q_ev1,QAnd(q_ev2,q_neq1)),QAnd(q_ev1,QAnd(q_ev3,q_neq2))))
            | Some q -> Some (QOr(q,QOr(QAnd(q_ev1,QAnd(q_ev2,q_neq1)),QAnd(q_ev1,QAnd(q_ev3,q_neq2)))))
          end
    in

    let q_blocking = match !event_blocking_record with
      | None -> q_counter
      | Some ev ->
          reset ();
          let c = fresh_var channel_id in
          let st = fresh_var stamp_id in
          let st' = fresh_var stamp_id in
          let q_ev1 = QFact (FEvent(ev,[Var c; Var st],false)) in
          let q_ev2 = QFact (FEvent(ev,[Var c; Var st'],false)) in
          let q_neq = QFact (FPred(neq_symb,[Var st; Var st'])) in
          begin match q_counter with
            | None -> Some (QAnd(q_ev1,QAnd(q_ev2,q_neq)))
            | Some q -> Some (QOr(q,QAnd(q_ev1,QAnd(q_ev2,q_neq))))
          end
    in

    let q_special_double_insert = match !event_special_double_record with
      | None -> q_blocking
      | Some ev ->
          reset ();
          let id = fresh_var stamp_id in
          let st = fresh_var stamp_id in
          let st' = fresh_var stamp_id in
          let q_ev1 = QFact (FEvent(ev,[Var id; Var st],false)) in
          let q_ev2 = QFact (FEvent(ev,[Var id; Var st'],false)) in
          let q_neq = QFact (FPred(neq_symb,[Var st; Var st'])) in
          begin match q_blocking with
            | None -> Some (QAnd(q_ev1,QAnd(q_ev2,q_neq)))
            | Some q -> Some (QOr(q,QAnd(q_ev1,QAnd(q_ev2,q_neq))))
          end
    in

    let q_double_insert =
      Hashtbl.fold (fun typ ev acc_q ->
        reset ();
        let id = fresh_var stamp_id in
        let st = fresh_var stamp_id in
        let st' = fresh_var stamp_id in
        let x = fresh_var typ in

        let q_ev1 = QFact(FEvent(ev,[Var id; Var st; Var x], false)) in
        let q_ev2 = QFact(FEvent(ev,[Var id; Var st'; Var x], false)) in
        let q_neq = QFact(FPred(neq_symb,[Var st; Var st'])) in
        match acc_q with
          | None -> Some (QAnd(q_ev1,QAnd(q_ev2,q_neq)))
          | Some q -> Some (QOr(q,QAnd(q_ev1,QAnd(q_ev2,q_neq))))
      ) event_double_record q_special_double_insert
    in

    q_double_insert
  in

  let rec generate_full_query = function
    | FLetNew(v,n,q) -> FLetNew(v,n,generate_full_query q)
    | FQuery q ->
        begin match q with
          | QFact f ->
              begin match generate_new_query None with
                | None -> FQuery (QFact f)
                | Some q -> FQuery(QQuery(f, q))
              end
          | QQuery(f,q) ->
              begin match generate_new_query (Some q) with
                | None -> internal_error "[transform.ml >> add_events_decl] The query cannot be empty."
                | Some q' -> FQuery(QQuery(f, q'))
              end
          | _ -> internal_error "[transform.ml >> add_events_decl] Wrong query format."
        end
  in

  let new_query_list =
    List.map generate_full_query query_list in

  let new_env =
    Hashtbl.fold (fun _ (int_to_var,_) env ->
      Hashtbl.fold (fun _ v env' -> env'@[v]) int_to_var env
    ) memory env
  in
  new_env, new_query_list

let rec add_events_decl scope queries = function
  | [] -> internal_error "[transform.ml >> add_events_decl] Unexpected case."
  | DFun f :: q -> DFun f :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | DConst f :: q -> DConst f :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | DReduc(f,reduc) :: q -> DReduc(f,reduc) :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | DReducFail(f,reduc) :: q -> DReducFail(f,reduc) :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | DPred(f,ids)::q -> DPred(f,ids) :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | DFree n :: q -> DFree n :: (add_events_decl (IdSet.add n.id_n scope) queries q)
  | DLetFun(f,vars,t) :: q -> DLetFun(f,vars,t) :: (add_events_decl (IdSet.add f.id_f scope) queries q)
  | [DProcess p] ->
      let gen_p, _ = add_events_eprocess_c scope TableMap.empty p in
      let p' = gen_p NameVarMap.empty in

      let queries' =
        List.map (fun (env,query_list) ->
          let env',query_list' = add_events_query env query_list in
          DQuery(env',query_list')
        ) queries
      in

      let decl_uaction = Hashtbl.fold (fun _ ev acc_decl -> (DEvent ev)::acc_decl) event_uaction_record (queries' @ [DProcess p']) in
      let decl_vcell = Hashtbl.fold (fun _ ev acc_decl -> (DEvent ev)::acc_decl) event_vcell_record decl_uaction in
      let decl_in_table = Hashtbl.fold (fun _ ev acc_decl -> (DEvent ev)::acc_decl) event_in_table_record decl_vcell in
      let decl_not_in_table = Hashtbl.fold (fun _ ev acc_decl -> (DEvent ev)::acc_decl) event_not_in_table_record decl_in_table in
      let decl_ucomm = match !event_ucomm_record with
        | None -> decl_not_in_table
        | Some ev -> (DEvent ev)::decl_not_in_table
      in
      let decl_ucounter = match !event_counter_record with
        | None -> decl_ucomm
        | Some ev -> (DEvent ev)::decl_ucomm
      in
      let decl_identifier = Hashtbl.fold (fun _ n acc_decl -> (DFree n)::acc_decl) identifier_record decl_ucounter in
      let decl_blocking = match !event_blocking_record with
        | None -> decl_identifier
        | Some ev -> (DEvent ev)::decl_identifier
      in
      let decl_special_double_insert = match !event_special_double_record with
        | None -> decl_blocking
        | Some ev -> (DEvent ev)::decl_blocking
      in
      let decl_double_insert = Hashtbl.fold (fun _ ev acc_decl -> (DEvent ev)::acc_decl) event_double_record decl_special_double_insert in
      let decl_id_get = Hashtbl.fold (fun _ n acc_decl -> (DFree n)::acc_decl) table_get_identifier_record decl_double_insert in
      let decl_st =
        if
          !event_ucomm_record <> None ||
          Hashtbl.length event_uaction_record <> 0 ||
          !event_counter_record <> None ||
          !event_blocking_record <> None ||
          !event_special_double_record <> None ||
          Hashtbl.length event_double_record <> 0
        then (DType stamp_id)::decl_id_get
        else decl_id_get
      in

      if !ignore_type && (
          Hashtbl.length event_vcell_record <> 0 ||
          Hashtbl.length event_in_table_record <> 0 ||
          Hashtbl.length event_not_in_table_record <> 0 ||
          !event_counter_record <> None
        )
      then
        begin
          replace_types_by_bitstring := true;
          (DSet(("ignoreTypes",dummy_ext), S ("false",dummy_ext))) :: decl_st
        end
      else decl_st
  | DQuery(env,query_list)::q -> add_events_decl scope (queries @ [env,query_list]) q
  | decl::q -> decl::(add_events_decl scope queries q)

(*******************************
***         Parsing          ***
********************************)

let parse filename =
  try
    let ic = open_in filename in
    let lexbuf = Lexing.from_channel ic in
    lexbuf.Lexing.lex_curr_p <- { lexbuf.Lexing.lex_curr_p with
                                  Lexing.pos_fname = filename };
    let ptree =
      try
        Pitparser.all Pitlexer.token lexbuf
      with Parsing.Parse_error ->
        input_error "Syntax error" (extent lexbuf)
    in
    close_in ic;
    ptree
  with Sys_error s ->
    user_error ("File error: " ^ s ^ "\n")

let parse_file s =
  Printf.printf "Parsing the file...\n";
  let (decl_l, proc, second_proc) = parse s in

  if second_proc <> None
  then user_error "Equivalence properties are not handled yet by the front-end.";

  let decl1 = expand_tdecl initial_environment proc decl_l in

  ignore_type := true;

  set_ignore_type decl1;

  detect_table_arguments_decl NameSet.empty decl1;

  assign_linked_variables ();

  analyse_channels_decl NameMap.empty decl1;

  let decl2 = expand_wrt_status_decl decl1 in

  analyse_tables_decl decl2;

  let decl3 = transform_tables_decl [] decl2 in

  analyse_cells_decl decl3;
  let cell_rq =
    {
      vars_to_add = NameSet.empty;
      last_input = NameMap.empty;
      last_var_input = NameMap.empty;
      last_output = NameMap.empty;
      last_var_output = NameMap.empty
    }
  in

  let decl4 = transform_cells_decl cell_rq decl3 in

  reset_decl decl4;

  analyse_channels_decl NameMap.empty decl4;

  analyse_tables_decl decl4;
  analyse_cells_decl decl4;

  add_events_decl IdSet.empty [] decl4
