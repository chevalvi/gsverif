(*

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details (in file LICENSE).

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)

let user_message =
  let str = ref "GSVerif : A front end for Proverif.\n" in
  let new_line s = str := Printf.sprintf "%s\n%s" !str s in
  new_line "The input file have almost the same syntax as input files for ProVerif.";
  new_line "Some options available in ProVerif are not accepted by the front-end but";
  new_line "and an error message will be displayed in such cases.";
  new_line "To help the front-end generating more precise ProVerif file, users can";
  new_line "indicate additional options when declaring names and tables.\n";
  new_line "Channels (declared with new or free) can take the options 'precise', 'cell',";
  new_line "'counter', 'uniqueAction' and 'uniqueComm'.";
  new_line "  Ex: free d:channel [precise].";
  new_line "  Ex: process ! new d:channel [precise]; Q\n";
  new_line "Tables can take the options 'precise', 'lockedInsert', 'lockedRound' and";
  new_line "'value'.";
  new_line "  Ex: table tbl(bitstring,bitstring) [value].\n";
  new_line "Options:";
  !str


let out_file = ref "output.pv"
let first_file = ref true

let anal_file s =
  if not (!first_file) then
    Parsing_helper.user_error "Error: You can analyze a single ProVerif file for each run of the front end.\nPlease rerun the front end with your second file.\n";

  first_file := false;

  Printf.printf "Starting the transformation...\n";
  let decl = Transform.parse_file s in

  Printf.printf "Writing in the output file...\n";
  let channel_out = open_out !out_file in
  Printf.fprintf channel_out "%s" (Display.display_all decl);
  close_out channel_out;
  Printf.printf "Translation complete !\n"

let _ =
  Arg.parse
    [ "-o", Arg.String(fun s -> out_file := s),
        "<filename> \tchoose the output file name"
    ]
    anal_file user_message
