open Pitptree

val parse_file : string -> decl list

val replace_types_by_bitstring : bool ref


val and_symb : symbol
val or_symb : symbol
val eq_symb : symbol
val neq_symb : symbol
val inf_symb : symbol
val infeq_symb : symbol
val not_symb : symbol
val true_symb : symbol
val false_symb : symbol

val bitstring_id : id
val channel_id : id
val bool_id : id
val nat_id : id
val stamp_id : id
val true_id : id
val false_id : id
val and_id : id
val or_id : id
val eq_id : id
val neq_id : id
val inf_id : id
val infeq_id : id
val not_id : id
