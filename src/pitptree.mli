(*

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details (in file LICENSE).

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*)
(* Typed front-end *)

(* Terms *)

type ident = Ptree.ident

type term =
		| PIdent of ident
		| PNat of int
		| PSum of ident * int
	  | PFail
    | PFunApp of ident * term_e list
    | PTuple of term_e list

and term_e = term * Parsing_helper.extent

(* Equational theory *)

type equation = term_e * term_e

(* Functions defined by reduction rules *)

type fundef =  (term_e * term_e) list

(* Nounif *)

type gformat =
    PFGIdent of ident
  | PFGFunApp of ident * gformat_e list
  | PFGTuple of gformat_e list
  | PFGName of ident * (ident * gformat_e) list
  | PFGAny of ident
  | PFGLet of ident * gformat_e * gformat_e

and gformat_e = gformat * Parsing_helper.extent

type nounif_t =
    BFLet of ident * gformat_e * nounif_t
  | BFNoUnif of (ident * gformat_e list * int) * int

(* Queries *)

type gterm =
    PGIdent of ident
	| PGNat of int
	| PGSum of ident * int
  | PGFunApp of ident * gterm_e list
  | PGPhase of ident * gterm_e list * int
  | PGTuple of gterm_e list
  | PGName of ident * (ident * gterm_e) list
  | PGLet of ident * gterm_e * gterm_e

and gterm_e = gterm * Parsing_helper.extent

type tquery =
    PPutBegin of bool * ident list
	(* bool value: false -> non-inj event, true -> inj event *)
  | PRealQuery of gterm_e

(* Clauses *)

type tclause =
    PClause of term_e * term_e
  | PFact of term_e
  | PEquiv of term_e * term_e * bool

(* Processes *)

type pterm =
    PPIdent of ident
	| PPNat of int
	| PPSum of ident * int
  | PPFunApp of ident * pterm_e list
  | PPTuple of pterm_e list
  | PPRestr of ident * ident list option(*variables to include as arguments*) * ident(*type*) * pterm_e
  | PPTest of pterm_e * pterm_e * pterm_e option
  | PPLet of tpattern * pterm_e * pterm_e * pterm_e option
  | PPLetFilter of (ident * ident(*type*)) list * pterm_e * pterm_e * pterm_e option

and pterm_e = pterm * Parsing_helper.extent

and tpattern =
    PPatVar of ident * ident option(*type*)
  | PPatTuple of tpattern list
  | PPatFunApp of ident * tpattern list
  | PPatEqual of pterm_e

type tprocess =
    PNil
  | PPar of tprocess * tprocess
  | PRepl of tprocess
  | PRestr of ident * ident list option(*variables to include as arguments*) * ident(*type*) * ident list (*options*) * tprocess
  | PLetDef of ident * pterm_e list
  | PTest of pterm_e * tprocess * tprocess
  | PInput of pterm_e * tpattern * tprocess
  | POutput of pterm_e * pterm_e * tprocess
  | PLet of tpattern * pterm_e * tprocess * tprocess
  | PLetFilter of (ident * ident(*type*)) list * pterm_e * tprocess * tprocess
  | PEvent of ident * pterm_e list * ident list option(*variables to include in environment for injective events*) * tprocess
  | PPhase of int * tprocess
  | PBarrier of int * ident option * tprocess
  | PInsert of ident * pterm_e list * tprocess
  | PGet of ident * tpattern list * pterm_e option * tprocess * tprocess

(* Declarations *)

type envdecl = (ident(*variable*) * ident(*type*)) list
type may_fail_env_decl = (ident(*variable*) * ident(*type*) * bool (* may-fail*)) list

type tdecl =
    TTypeDecl of ident (* type declaration *)
  | TFunDecl of ident * ident list(*argument types*) * ident(*result type*) * ident list(*options*)
  | TEventDecl of ident * ident list(*argument types*)
  | TConstDecl of ident * ident(*type*) * ident list(*options*)
  | TReduc of (envdecl * term_e * term_e) list * ident list(*options*)
  | TReducFail of ident * ident list * ident * (may_fail_env_decl * term_e * term_e) list * ident list(*options*)
  | TEquation of (envdecl * term_e * term_e) list * ident list(*options*)
  | TPredDecl of ident * ident list(*argument types*) * ident list(*options*)
  | TTableDecl of ident * ident list(*argument types*) * ident list (*options*)
  | TSet of ident * Ptree.pval
  | TPDef of ident * may_fail_env_decl * tprocess
  | TQuery of envdecl * tquery list
  | TNoninterf of envdecl * (ident * term_e list option) list
  | TWeaksecret of ident
  | TNoUnif of envdecl * nounif_t
  | TNot of envdecl * gterm_e
  | TElimtrue of may_fail_env_decl * term_e
  | TFree of ident * ident(*type*) * ident list(*options*)
  | TClauses of (may_fail_env_decl * tclause) list
  | TDefine of ident * ident list * tdecl list
  | TExpand of ident * ident list
  | TLetFun of ident * may_fail_env_decl * pterm_e

(* Transformation *)

type id =
  {
    id : string;
    counter : int
  }

type symbol =
  {
    id_f : id;
    arg_ty : id list;
    return_ty : id;

		mutable pure_priv : bool;
    priv : bool;
    data : bool;
    type_conv : bool;

  }

type name_decl =
  | NDPrivate
  | NDPrecise
  | NDUAction
  | NDUComm
  | NDCell
  | NDCounter

type table_decl =
  | TblNone
  | TblPrecise
	| TblLockedPerInsert
  | TblLockedPerRound
  | TblValue


type modif_pattern =
  | MModif
  | MBlackBox
  | MFun of modif_pattern list

type status_nat =
  | TableRound of table * bool
  | TableInsert of table * bool

and status_pattern =
  | SOpen of id (* Type *)
  | SBlackBox of id (* Type *)
  | SNat of status_nat list * modif_pattern option
  | STuple of status_pattern list
  | SData of symbol * status_pattern list

and status_channel =
  | SCNone
  | SCPublic
  | SCPrivate
  | SCCell of status_pattern option

and status_table =
  | STNone
  | STUnknown
  | STUnlocked
  | STLocked of name list

and var =
  {
    id_v : id;
    typ_v : id;
		mutable status_v : status_channel;
		mutable options_v : name_decl list
  }

and name =
  {
    id_n : id;
    typ_n : id;
    options_n : name_decl list;
    ext_n : Parsing_helper.extent;
    mutable status_n : status_channel
  }

and table =
  {
    id_t : id;
    arg_ty_t : id list;
    options_t : table_decl;
		uaction_t : bool;
    mutable status_t : status_table
  }

and event =
  {
    id_e : id;
    arg_ty_e : id list
  }

type eterm =
  | Var of var
  | Name of name
  | Sum of var * int
  | Nat of int
  | Fun of symbol * eterm list
  | Tuple of eterm list
  | Restr of name * var list option * eterm
  | Test of eterm * eterm * eterm option
  | Let of epattern * eterm * eterm * eterm option
  | LetFilter of var list * eterm * eterm * eterm option

and epattern =
  | PatVar of var
  | PatEq of eterm
  | PatTuple of epattern list
  | PatData of symbol * epattern list

type eprocess =
    CNil
  | CPar of eprocess_c * eprocess_c
  | CRepl of eprocess_c
  | CRestr of name * var list option (*variables to include as arguments*)  * eprocess_c
  | CTest of eterm * eprocess_c * eprocess_c
  | CInput of eterm * epattern * eprocess_c
  | COutput of eterm * eterm * eprocess_c
  | CLet of epattern * eterm * eprocess_c * eprocess_c
  | CLetFilter of var list * eterm * eprocess_c * eprocess_c
  | CEvent of event * eterm list * var list option(*variables to include in environment for injective events*) * eprocess_c
  | CInsert of table * eterm list * eprocess_c
  | CGet of table * epattern list * eterm option * eprocess_c * eprocess_c

and eprocess_c =
  {
    proc : eprocess;
    comment : string option
  }

type e_envdecl = var list
type e_may_fail_env_decl = (var * bool) list

type fact =
  | FPred of symbol * eterm list
  | FEvent of event * eterm list * bool
  | FTable of table * eterm list
  | FMessage of eterm * eterm
  | FAttacker of eterm

and query =
  | QFact of fact
  | QOr of query * query
  | QAnd of query * query
  | QNot of query
  | QQuery of fact * query

type query_full =
	| FLetNew of var * string * query_full
	| FQuery of query

type cclause =
    CClause of eterm * eterm
  | CFact of eterm
  | CEquiv of eterm * eterm * bool

type decl =
  | DType of id
  | DFun of symbol
  | DEvent of event
  | DConst of symbol
  | DReduc of symbol * (e_envdecl * eterm * eterm) list
  | DReducFail of symbol * (e_may_fail_env_decl * eterm * eterm) list
  | DEquation of (e_envdecl * eterm * eterm) list * ident list
  | DPred of symbol * ident list
  | DTable of table
  | DSet of ident * Ptree.pval
	| DNot of e_envdecl * fact
  | DQuery of e_envdecl * query_full list
  | DFree of name
  | DLetFun of symbol * e_may_fail_env_decl * eterm
  | DProcess of eprocess_c
	| DClauses of (e_may_fail_env_decl * cclause) list
	| DElimtrue of e_may_fail_env_decl * eterm

type envElt =
  | EProcess of id (* id *) * id list (* arg types *) * (eterm list -> eprocess_c)
  | EType of id (* id *)
  | ETable of table
  | EEvent of event
  | EFun of symbol
  | EVar of var
  | EName of name
  | EArg of eterm * id (* type *)
