open Ptree
open Pitptree
open Parsing_helper
open Transform

module CompString =
  struct
    type t = string
    let compare  = Pervasives.compare
  end

module StringSet = Set.Make(CompString)

module StringMap = Map.Make(CompString)

module CompId =
  struct
    type t = id
    let compare  = Pervasives.compare
  end

module IdMap = Map.Make(CompId)

type envDisplay =
  {
      used_str : StringSet.t;
      map : string IdMap.t;
      map_str : int StringMap.t;
  }

(* Generic display function *)

let display_list f_elt sep = function
  | [] -> ""
  | [t] -> f_elt t
  | t::q -> List.fold_left (fun acc t' -> Printf.sprintf "%s%s%s" acc sep (f_elt t')) (f_elt t) q

let restricted_name = ref []

(* Options, environments, ... *)

let rec find_n env str n =
  let str' =
    if n = 0
    then str
    else (Printf.sprintf "%s%d" str n)
  in
  if StringSet.mem str' env.used_str
  then find_n env str (n+1)
  else (str',n)

let add_id env id =
  if IdMap.mem id env.map
  then internal_error "[display.ml >> add_id] The id should not be already registered."
  else
    let n =
      if StringMap.mem id.id env.map_str
      then StringMap.find id.id env.map_str
      else 0
    in
    let (str,n') = find_n env id.id n in
    { used_str = StringSet.add str env.used_str; map = IdMap.add id str env.map; map_str = StringMap.add id.id n' env.map_str }

let add_type env id =
  if IdMap.mem id env.map
  then internal_error "[display.ml >> add_type] The type should not be already registered."
  else
    if !replace_types_by_bitstring && id <> stamp_id
    then { env with map = IdMap.add id "bitstring" env.map }
    else
      let n =
        if StringMap.mem id.id env.map_str
        then StringMap.find id.id env.map_str
        else 0
      in
      let (str,n') = find_n env id.id n in
      { used_str = StringSet.add str env.used_str; map = IdMap.add id str env.map; map_str = StringMap.add id.id n' env.map_str }

let display_id env id =
  if not (IdMap.mem id env.map)
  then internal_error (Printf.sprintf "[display.ml >> display_id] The id (%s,%d) should already be registered." id.id id.counter);

  IdMap.find id env.map

let display_vars_list env = display_list (fun v -> display_id env v.id_v) ", "

let display_vars_list_options env = function
  | None -> ""
  | Some l -> Printf.sprintf "[%s]" (display_vars_list env l)

let rec display_decl_vars_list env = function
  | [] -> internal_error "[display.ml >> display_decl_vars_list] The list should not be empty."
  | [v] ->
      let env' = add_id env v.id_v in
      (Printf.sprintf "%s:%s" (display_id env' v.id_v) (display_id env v.typ_v)), env'
  | v::q ->
      let env' = add_id env v.id_v in
      let str = Printf.sprintf "%s:%s" (display_id env' v.id_v) (display_id env v.typ_v) in
      let str_q, env'' = display_decl_vars_list env' q in
      (str^","^str_q), env''

let rec display_decl_fail_vars_list env = function
  | [] -> internal_error "[display.ml >> display_decl_fail_vars_list] The list should not be empty."
  | [v,b] ->
      let env' = add_id env v.id_v in
      let str_b = if b then " or fail" else "" in
      (Printf.sprintf "%s:%s%s" (display_id env' v.id_v) (display_id env v.typ_v) str_b), env'
  | (v,b)::q ->
      let env' = add_id env v.id_v in
      let str_b = if b then " or fail" else "" in
      let str = Printf.sprintf "%s:%s%s" (display_id env' v.id_v) (display_id env v.typ_v) str_b in
      let str_q, env'' = display_decl_fail_vars_list env' q in
      (str^","^str_q), env''

(*let display_newarg = function
  | None -> ""
  | Some l -> Printf.sprintf "[%s]" (display_neidentseq l)

let display_nevartype = function
  | [] -> failwith "[syntax.ml >> display_nevartype] The list should not be empty"
  | l -> display_list (fun (id,typ) -> Printf.sprintf "%s:%s" (display_ident id) (display_ident typ)) ", " l

let display_forallvartype = function
  | [] -> ""
  | l ->
      Printf.sprintf "forall %s; " (
        display_list (fun (id,typ) ->
          Printf.sprintf "%s:%s" (display_ident id) (display_ident typ)
        ) ", " l
      )

let display_typeid = display_ident

let display_typeidseq = display_list display_typeid ","
*)

(* Terms *)

let rec display_eterm env = function
  | Var v -> display_id env v.id_v
  | Name n -> display_id env n.id_n
  | Sum(v,n) -> Printf.sprintf "%s + %d" (display_id env v.id_v) n
  | Nat n -> string_of_int n
  | Fun(f,[t1;t2]) when List.exists (fun f' -> f == f') [eq_symb;neq_symb;inf_symb;infeq_symb] ->
      Printf.sprintf "%s %s %s" (display_eterm env t1) (display_id env f.id_f) (display_eterm env t2)
  | Fun(f,[t1;t2]) when List.exists (fun f' -> f == f') [and_symb;or_symb] ->
      Printf.sprintf "(%s %s %s)" (display_eterm env t1) (display_id env f.id_f) (display_eterm env t2)
  | Fun(f,_) when List.exists (fun f' -> f == f') [eq_symb;neq_symb;and_symb;or_symb;inf_symb;infeq_symb] ->
      internal_error "[display.ml >> display_eterm] Should have exactly two terms"
  | Fun(f,[t]) when f == not_symb -> Printf.sprintf "%s (%s)" (display_id env f.id_f) (display_eterm env t)
  | Fun(f,_) when f == not_symb -> internal_error "[display.ml >> display_term] Should have exactly one term"
  | Fun(f,[]) -> Printf.sprintf "%s" (display_id env f.id_f)
  | Fun(f,args) -> Printf.sprintf "%s(%s)" (display_id env f.id_f) (display_eterm_list env args)
  | Tuple(args) -> Printf.sprintf "(%s)" (display_eterm_list env args)
  | Restr(n,op,t) ->
      let env' = add_id env n.id_n in
      Printf.sprintf "new %s%s:%s; %s"
        (display_id env' n.id_n)
        (display_vars_list_options env op)
        (display_id env n.typ_n)
        (display_eterm env' t)
  | Test(cond,t_then,t_else_op) ->
      let else_str = match t_else_op with
        | None -> ""
        | Some t_else -> Printf.sprintf " else %s" (display_eterm env t_else)
      in
      Printf.sprintf "if %s then %s%s" (display_eterm env cond) (display_eterm env t_then) else_str
  | Let(pat,cond,t_then,t_else_op) ->
      let else_str = match t_else_op with
        | None -> ""
        | Some t_else -> Printf.sprintf " else %s" (display_eterm env t_else)
      in
      let str_pat, env' = display_epattern env pat in
      Printf.sprintf "let %s = %s in %s%s" str_pat (display_eterm env cond) (display_eterm env' t_then) else_str
  | LetFilter(var_list,cond,t_then,t_else_op) ->
      let else_str = match t_else_op with
        | None -> ""
        | Some t_else -> Printf.sprintf " else %s" (display_eterm env t_else)
      in
      let str_var_list,env' = display_decl_vars_list env var_list in
      Printf.sprintf "let %s suchthat %s in %s%s" str_var_list (display_eterm env' cond) (display_eterm env' t_then) else_str

and display_eterm_list env l = display_list (display_eterm env) "," l

and display_epattern env = function
  | PatVar v ->
      let env' = add_id env v.id_v in
      (Printf.sprintf "%s:%s" (display_id env' v.id_v) (display_id env v.typ_v)), env'
  | PatEq t -> (Printf.sprintf "= %s" (display_eterm env t)), env
  | PatTuple args ->
      let str,env' = display_epattern_list env args in
      (Printf.sprintf "(%s)" str), env'
  | PatData(f,args) ->
      let str,env' = display_epattern_list env args in
      (Printf.sprintf "%s(%s)" (display_id env f.id_f) str), env'

and display_epattern_list env = function
  | [] -> internal_error "[display.ml >> display_epattern_list] List should not be empty."
  | [pat] -> display_epattern env pat
  | pat::q ->
      let str, env' = display_epattern env pat in
      let str_q, env'' = display_epattern_list env' q in
      (str^","^str_q), env''

(* Display function for reductions and equations *)

let display_forall env display_elt l =
  if l = []
  then "", env
  else
    let str, env' = display_elt env l in
    (Printf.sprintf "forall %s; " str),env'

let display_reduc_eq_list env = function
  | [] -> internal_error "[display.ml >> display_reduc_list] The list should not be empty"
  | l ->
      display_list (fun (forall,t1,t2) ->
        let str_forall, env' = display_forall env display_decl_vars_list forall in
        Printf.sprintf "%s%s = %s" str_forall (display_eterm env' t1) (display_eterm env' t2)
      ) ";\n   " l

let display_reduc_otherwise_list env = function
  | [] -> internal_error "[display.ml >> display_reduc_otherwise_list] The list should not be empty"
  | l ->
      display_list (fun (forall,t1,t2) ->
        let str_forall, env' = display_forall env display_decl_fail_vars_list forall in
        Printf.sprintf "%s%s = %s" str_forall (display_eterm env' t1) (display_eterm env' t2)
      ) "\n   otherwise " l

let display_clause env = function
  | CClause(t1,t2) -> Printf.sprintf "%s -> %s" (display_eterm env t1)(display_eterm env t2)
  | CFact(t)-> display_eterm env t
  | CEquiv(t1,t2,true) -> Printf.sprintf "%s <-> %s" (display_eterm env t1)(display_eterm env t2)
  | CEquiv(t1,t2,false) -> Printf.sprintf "%s <=> %s" (display_eterm env t1)(display_eterm env t2)

(* Queries *)

let display_fact env = function
  | FPred(f,[t1;t2]) when List.exists (fun f' -> f == f') [eq_symb;neq_symb;inf_symb;infeq_symb] ->
      Printf.sprintf "%s %s %s" (display_eterm env t1) (display_id env f.id_f) (display_eterm env t2)
  | FPred(f,_) when List.exists (fun f' -> f == f') [eq_symb;neq_symb;and_symb;or_symb;inf_symb;infeq_symb] ->
      internal_error "[display.ml >> display_fact] Should have exactly two terms"
  | FPred(f,args) ->
      let str_args =
        if args = []
        then ""
        else Printf.sprintf "(%s)" (display_eterm_list env args)
      in
      Printf.sprintf "%s%s" (display_id env f.id_f) str_args
  | FEvent(ev,args,b) ->
      let str_inj = if b then "inj-event" else "event" in
      let str_args =
        if args = []
        then ""
        else Printf.sprintf "(%s)" (display_eterm_list env args)
      in

      Printf.sprintf "%s(%s%s)" str_inj (display_id env ev.id_e) str_args
  | FTable(tbl,args) -> Printf.sprintf "table(%s(%s))" (display_id env tbl.id_t) (display_eterm_list env args)
  | FMessage(t1,t2) -> Printf.sprintf "mess(%s,%s)" (display_eterm env t1) (display_eterm env t2)
  | FAttacker t -> Printf.sprintf "attacker(%s)" (display_eterm env t)

type prev_display =
  | PrevNone
  | PrevOr
  | PrevAnd

let display_query env = function
  | QFact f -> Printf.sprintf "  %s" (display_fact env f)
  | QQuery(f,q) ->
      let rec sub_display_or search_first prev = function
        | QOr(q1,q2) ->
            if search_first
            then Printf.sprintf "%s\n%s" (sub_display_or true PrevOr q1) (sub_display_or false PrevOr q2)
            else Printf.sprintf "%s\n%s" (sub_display_or false PrevOr q1) (sub_display_or false PrevOr q2)
        | q ->
            if search_first
            then Printf.sprintf "        %s" (sub_display prev q)
            else Printf.sprintf "    ||  %s" (sub_display prev q)

      and sub_display prev = function
        | QFact f -> display_fact env f
        | QOr(q1,q2) ->
            if prev = PrevOr || prev = PrevNone
            then Printf.sprintf "%s || %s" (sub_display PrevOr q1) (sub_display PrevOr q2)
            else Printf.sprintf "(%s || %s)" (sub_display PrevOr q1) (sub_display PrevOr q2)
        | QAnd(q1,q2) -> Printf.sprintf "%s && %s" (sub_display PrevAnd q1) (sub_display PrevAnd q2)
        | QNot q -> Printf.sprintf "not (%s)" (sub_display PrevNone q)
        | QQuery(f,q) ->
            if prev = PrevNone
            then Printf.sprintf "%s ==> %s" (display_fact env f) (sub_display PrevNone q)
            else Printf.sprintf "(%s ==> %s)" (display_fact env f) (sub_display PrevNone q)
      in

      Printf.sprintf "  %s ==>\n%s" (display_fact env f) (sub_display_or true PrevNone q)
  | _ -> user_error "Wrong format of query."

let rec display_full_query env = function
  | FLetNew(v,n,q) ->
      let env' = add_id env v.id_v in
      Printf.sprintf "let %s = new %s in %s" (display_id env' v.id_v) n (display_full_query env' q)
  | FQuery q -> display_query env q
(* Process *)

let display_comment p = match p.comment with
  | None -> ""
  | Some str -> Printf.sprintf " (* %s *)" str

let rec display_eprocess_c tab prev_par env proc_c =
  let tab = match proc_c.proc with
    | CPar _ -> tab
    | _ -> if prev_par then tab^"  " else tab
  in

  let pat_let pat str_pat = match pat with
    | PatEq _ -> Printf.sprintf "(%s)" str_pat
    | _ -> str_pat
  in

  match proc_c.proc with
    | CNil -> Printf.sprintf "%s0%s\n" tab (display_comment proc_c)
    | CPar(p1,p2) ->
        if prev_par
        then
          match proc_c.comment with
            | None ->
                Printf.sprintf "%s%s) | (\n%s"
                  (display_eprocess_c tab true env p1) tab
                  (display_eprocess_c tab true env p2)
            | Some str_comment ->
                Printf.sprintf "%s(* %s *)\n%s%s) | (\n%s"
                  tab str_comment
                  (display_eprocess_c tab true env p1) tab
                  (display_eprocess_c tab true env p2)
        else
          Printf.sprintf "%s(%s\n%s%s) | (\n%s%s)\n"
            tab (display_comment proc_c)
            (display_eprocess_c tab true env p1) tab
            (display_eprocess_c tab true env p2) tab
    | CRepl p ->
        Printf.sprintf "%s!( %s\n%s%s)" tab (display_comment proc_c) (display_eprocess_c tab false env p) tab
    | CRestr(n,vars_op,p) ->
        let env' = add_id env n.id_n in
        restricted_name := (n,display_id env' n.id_n) :: !restricted_name;
        Printf.sprintf "%snew %s%s:%s;%s\n%s"
          tab (display_id env' n.id_n)
          (display_vars_list_options env vars_op) (display_id env n.typ_n)
          (display_comment proc_c) (display_eprocess_c tab false env' p)
    | CTest(cond,p_then,{ proc = CNil; _ }) ->
        Printf.sprintf "%sif %s then%s\n%s" tab (display_eterm env cond) (display_comment proc_c) (display_eprocess_c tab false env p_then)
    | CTest(cond,p_then,p_else) ->
        Printf.sprintf "%sif %s%s\n%sthen\n%s%selse\n%s"
          tab (display_eterm env cond)
          (display_comment proc_c)
          tab (display_eprocess_c (tab^"  ") false env p_then)
          tab (display_eprocess_c (tab^"  ") false env p_else)
    | CInput(t,pat,p) ->
        let str_pat, env' = display_epattern env pat in
        Printf.sprintf "%sin(%s,%s);%s\n%s" tab (display_eterm env t) str_pat (display_comment proc_c) (display_eprocess_c tab false env' p)
    | COutput(t1,t2,p) ->
        Printf.sprintf "%sout(%s,%s);%s\n%s" tab (display_eterm env t1) (display_eterm env t2) (display_comment proc_c) (display_eprocess_c tab false env p)
    | CLet(pat,t,p_then, { proc = CNil; _ }) ->
        let str_pat, env' = display_epattern env pat in
        let str_pat = pat_let pat str_pat in
        Printf.sprintf "%slet %s = %s in%s\n%s" tab str_pat (display_eterm env t) (display_comment proc_c) (display_eprocess_c tab false env' p_then)
    | CLet(pat,t,p_then,p_else) ->
        let str_pat, env' = display_epattern env pat in
        let str_pat = pat_let pat str_pat in
        Printf.sprintf "%slet %s = %s in%s\n%s%selse\n%s"
          tab str_pat
          (display_eterm env t) (display_comment proc_c)
          (display_eprocess_c (tab^"  ") false env' p_then)
          tab (display_eprocess_c (tab^"  ") false env p_else)
    | CLetFilter(vars_l,cond,p_then, { proc = CNil; _ }) ->
        let str_pat, env' = display_decl_vars_list env vars_l in
        Printf.sprintf "%slet %s suchthat %s in%s\n%s" tab str_pat (display_eterm env' cond) (display_comment proc_c) (display_eprocess_c tab false env' p_then)
    | CLetFilter(vars_l,cond,p_then,p_else) ->
        let str_pat, env' = display_decl_vars_list env vars_l in
        Printf.sprintf "%slet %s = %s in%s\n%s%selse\n%s"
          tab str_pat
          (display_eterm env' cond) (display_comment proc_c)
          (display_eprocess_c (tab^"  ") false env' p_then)
          tab (display_eprocess_c (tab^"  ") false env p_else)
    | CEvent(ev,args,var_op,p) ->
        Printf.sprintf "%sevent %s(%s)%s;%s\n%s"
          tab (display_id env ev.id_e) (display_eterm_list env args) (display_vars_list_options env var_op) (display_comment proc_c) (display_eprocess_c tab false env p)
    | CInsert(tbl,args,p) ->
        Printf.sprintf "%sinsert %s(%s);%s\n%s"
          tab (display_id env tbl.id_t) (display_eterm_list env args) (display_comment proc_c) (display_eprocess_c tab false env p)
    | CGet(tbl,pat_l,t_op,p_then, { proc = CNil; _ }) ->
        let str_pat, env' = display_epattern_list env pat_l in
        let str_such = match t_op with
          | None -> ""
          | Some t -> Printf.sprintf " suchthat %s" (display_eterm env' t)
        in
        Printf.sprintf "%sget %s(%s)%s in%s\n%s"
          tab (display_id env tbl.id_t) str_pat str_such (display_comment proc_c) (display_eprocess_c tab false env' p_then)
    | CGet(tbl,pat_l,t_op,p_then,p_else) ->
        let str_pat, env' = display_epattern_list env pat_l in
        let str_such = match t_op with
          | None -> ""
          | Some t -> Printf.sprintf " suchthat %s" (display_eterm env' t)
        in
        Printf.sprintf "%sget %s(%s)%s in%s\n%s%selse\n%s"
          tab (display_id env tbl.id_t) str_pat str_such (display_comment proc_c) (display_eprocess_c (tab^"  ") false env' p_then) tab (display_eprocess_c (tab^"  ") false env' p_else)

(* Display function for lib *)

let display_options f =
  let one_op = ref false in
  let str_priv = if f.priv then (one_op := true; "private") else "" in
  let str_data =
    let comma = if !one_op then "," else "" in
    if f.data
    then (one_op := true; Printf.sprintf "%s%sdata" str_priv comma)
    else str_priv
  in
  let str_type =
    let comma = if !one_op then "," else "" in
    if f.type_conv
    then (one_op := true; Printf.sprintf "%s%stypeConverter" str_data comma)
    else str_data
  in
  if !one_op then Printf.sprintf " [%s]" str_type else ""

let display_decl env = function
  | DType id ->
      let env' = add_type env id in
      if !replace_types_by_bitstring && id <> stamp_id
      then
        let str = Printf.sprintf "(* The type %s was replaced by bitstring since types are ignored. *)" id.id in
        str,env'
      else
        let str = Printf.sprintf "type %s." (display_id env' id) in
        str, env'
  | DFun f ->
      let str_option = display_options f in
      let env' = add_id env f.id_f in
      let str = Printf.sprintf "fun %s(%s):%s%s."
        (display_id env' f.id_f)
        (display_list (display_id env) "," f.arg_ty)
        (display_id env f.return_ty)
        str_option
      in
      str,env'
  | DEvent ev ->
      let env' = add_id env ev.id_e in
      let str =
        if ev.arg_ty_e = []
        then Printf.sprintf "event %s." (display_id env' ev.id_e)
        else Printf.sprintf "event %s(%s)." (display_id env' ev.id_e) (display_list (display_id env) "," ev.arg_ty_e)
      in
      str,env'
  | DConst f ->
      let str_option = display_options f in
      let env' = add_id env f.id_f in
      let str = Printf.sprintf "const %s:%s%s." (display_id env' f.id_f) (display_id env f.return_ty) str_option in
      str,env'
  | DReduc(f,reduc_list) ->
      let str_option = display_options f in
      let env' = add_id env f.id_f in
      let str = Printf.sprintf "reduc %s%s." (display_reduc_eq_list env' reduc_list) str_option in
      str, env'
  | DReducFail(f,reduc_list) ->
      let str_option = display_options f in
      let env' = add_id env f.id_f in
      let str =
        Printf.sprintf "fun %s(%s):%s\nreduc %s%s."
          (display_id env' f.id_f)
          (display_list (display_id env) "," f.arg_ty)
          (display_id env f.return_ty)
          (display_reduc_otherwise_list env' reduc_list)
          str_option
      in
      str,env'
  | DEquation(eq_list,options) ->
      let str_option = match options with
        | [] -> ""
        | _ -> Printf.sprintf " [%s]" (display_list (fun (str,_) -> str) "," options)
      in
      let str = Printf.sprintf "equation %s%s." (display_reduc_eq_list env eq_list) str_option in
      str,env
  | DPred(f,options) ->
      let str_option = match options with
        | [] -> ""
        | _ -> Printf.sprintf " [%s]" (display_list (fun (str,_) -> str) "," options)
      in
      let env' = add_id env f.id_f in
      let str_type =
        if f.arg_ty = []
        then ""
        else Printf.sprintf "(%s)" (display_list (display_id env) "," f.arg_ty)
      in
      let str = Printf.sprintf "pred %s%s%s." (display_id env' f.id_f) str_type str_option in
      str,env'
  | DTable tbl ->
      let env' = add_id env tbl.id_t in
      let str = Printf.sprintf "table %s(%s)." (display_id env' tbl.id_t) (display_list (display_id env) "," tbl.arg_ty_t) in
      str, env'
  | DSet((str,_),value) ->
      let str_value = match value with
        | S (str,_) -> str
        | I n -> string_of_int n
      in
      let str = Printf.sprintf "set %s = %s." str str_value in
      str,env
  | DNot(vars_list,fact) ->
      let str_vars, env' =
        if vars_list = []
        then "", env
        else
          let str,env'' = display_decl_vars_list env vars_list in
          (Printf.sprintf "%s; " str),env''
      in
      let str = Printf.sprintf "not %s%s." str_vars (display_fact env' fact) in
      str, env
  | DQuery(vars_list,q_list) ->
      let str_var, env' =
        if vars_list = []
        then "", env
        else
          let str, env' = display_decl_vars_list env vars_list in
          str^";", env'
      in
      let str = Printf.sprintf "query %s\n%s." str_var (display_list (display_full_query env') ";\n" q_list) in
      str,env
  | DFree n ->
      let str_option =
        if List.mem NDPrivate n.options_n
        then " [private]"
        else ""
      in
      let env' = add_id env n.id_n in
      let str = Printf.sprintf "free %s:%s%s." (display_id env' n.id_n) (display_id env n.typ_n) str_option in
      str,env'
  | DLetFun(f,vars_list,t) ->
      let env' = add_id env f.id_f in
      let str_vars,env'' =
        if vars_list = []
        then "", env'
        else
          let str_sub_vars, env'' = display_decl_fail_vars_list env' vars_list in
          (Printf.sprintf "(%s)" str_sub_vars),env''
      in

      let str = Printf.sprintf "letfun %s%s = %s." (display_id env' f.id_f) str_vars (display_eterm env'' t) in
      str,env'
  | DProcess p ->
      let str = Printf.sprintf "process\n%s" (display_eprocess_c "  " false env p) in
      str, env
  | DClauses clause_list ->
      let display_single (forall,clause) =
        let str_forall, env' = display_decl_fail_vars_list env forall in
        let str_comma = if forall = [] then "" else "; " in
        Printf.sprintf "  %s%s%s" str_forall str_comma (display_clause env' clause)
      in

      let str = Printf.sprintf "clauses\n%s." (display_list display_single ";\n" clause_list) in
      str,env
  | DElimtrue(forall,t) ->
      let str_forall, env' = display_decl_fail_vars_list env forall in
      let str_comma = if forall = [] then "" else "; " in
      let str = Printf.sprintf "elimtrue %s%s%s." str_forall str_comma (display_eterm env' t) in

      str,env

let is_same = function
  | DType _, DType _
  | DFun _, DFun _
  | DConst _, DConst _
  | DEquation _, DEquation _
  | DEvent _, DEvent _
  | DPred _, DPred _
  | DTable _, DTable _
  | DNot _, DNot _
  | DFree _, DFree _ -> true
  | _, _ -> false

let initial_environment =
  let env0 =
    {
        used_str = StringSet.empty;
        map = IdMap.empty;
        map_str = StringMap.empty;
    }
  in
  let env1 = add_id env0 bitstring_id in
  let env2 = add_id env1 channel_id in
  let env3 = add_id env2 bool_id in
  let env4 = add_id env3 nat_id in
  let env5 = add_id env4 true_id in
  let env6 = add_id env5 false_id in
  let env7 = add_id env6 and_id in
  let env8 = add_id env7 or_id in
  let env9 = add_id env8 eq_id in
  let env10 = add_id env9 neq_id in
  let env11 = add_id env10 inf_id in
  let env12 = add_id env11 infeq_id in
  let env13 = add_id env12 not_id in
  env13

let display_all = function
  | [] -> ""
  | [lib] -> let str,_ = display_decl initial_environment lib in str
  | lib::q ->
      let rec go_through acc prev env = function
        | [] -> acc
        | l::q' ->
            let str,env' = display_decl env l in
            let acc' =
              if is_same (prev,l)
              then Printf.sprintf "%s\n%s" acc str
              else Printf.sprintf "%s\n\n%s" acc str
            in
            go_through acc' l env' q'
      in
      let str,env = display_decl initial_environment lib in
      go_through str lib env q
