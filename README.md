# Global States in ProVerif: GSVerif

Formal methods have been successfully applied to security protocols, offering very good tool support to detect flaws or prove security. One exception is the case of protocols with global states such as counters, tables, or more generally, memory cells. Even the highly popular tool **ProVerif** fails to analyse such protocols, due to its internal abstraction.

Instead of designing a new ad-hoc procedure, our key idea is to devise a generic transformation of the security properties queried to **ProVerif**. We prove the soundness of our transformation and implement it into a front-end **GSVerif**.
